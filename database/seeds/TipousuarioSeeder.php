<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipousuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $ahora = new DateTime;
        DB::table('tipousuario')->insert([
            'nombre' => 'Super usuario',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);

        DB::table('tipousuario')->insert([
            'nombre' => 'Representante',
            'created_at' => $ahora,
            'updated_at' => $ahora,
            'empresa_id' => 2
         ]);

        DB::table('tipousuario')->insert([
            'nombre' => 'Gerente general',
            'created_at' => $ahora,
            'updated_at' => $ahora,
            'empresa_id' => 3
        ]);

        DB::table('tipousuario')->insert([
            'nombre' => 'Administrador general',
            'created_at' => $ahora,
            'updated_at' => $ahora,
            'empresa_id' => 3
        ]);
    }
}
