<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FlujotrabajoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $ahora = new DateTime;
        DB::table('flujotrabajo')->insert([
            'descripcion' => 'FLUJO DE TRABAJO 001',
            'codigo' => 'FT001',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('flujotrabajo')->insert([
            'descripcion' => 'FLUJO DE TRABAJO 002',
            'codigo' => 'FT002',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('flujotrabajo')->insert([
            'descripcion' => 'FLUJO DE TRABAJO 003',
            'codigo' => 'FT003',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
    }
}
