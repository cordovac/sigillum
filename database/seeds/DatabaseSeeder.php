<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EmpresaSeeder::class);
        $this->call(RepresentanteSeeder::class);
        $this->call(CategoriamenuSeeder::class);
        $this->call(OpcionmenuSeeder::class);
        $this->call(PermisoSeeder::class);
        $this->call(UsuarioSeeder::class);
        $this->call(TipousuarioSeeder::class);
        $this->call(TipousuarioUsuarioSeeder::class);
        $this->call(PermisoTipousuarioSeeder::class);
        $this->call(TareaSeeder::class);
        $this->call(TipodocumentoSeeder::class);
        $this->call(FlujotrabajoSeeder::class);
        $this->call(ReglaSeeder::class);
        $this->call(RespuestaSeeder::class);
        $this->call(DetalleflujotrabajoSeeder::class);
        $this->call(MetadataSeeder::class);
    }
}
