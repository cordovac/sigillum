<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MetadataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $ahora = new DateTime;
        DB::table('metadata')->insert([
            'nombre' => 'Apellido paterno',
            'codigo' => 'apellidopaterno',
            'tipocontrol' => 'text',
            'tipodocumento_id' => 1,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);

        DB::table('metadata')->insert([
            'nombre' => 'Apellido materno',
            'codigo' => 'apellidomaterno',
            'tipocontrol' => 'text',
            'tipodocumento_id' => 1,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);

        DB::table('metadata')->insert([
            'nombre' => 'Nombres',
            'codigo' => 'nombres',
            'tipocontrol' => 'text',
            'tipodocumento_id' => 1,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
    }
}
