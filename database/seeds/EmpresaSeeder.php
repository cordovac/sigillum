<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class EmpresaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $ahora = new DateTime;
        $fecha = date('Y-m-d');
        DB::table('empresa')->insert([
            'ruc' => '20602635172',
            'razonsocial' => 'IMANTRA CONSULTORA EMPRESARIAL SAC',
            'direccionfiscal' => 'AV. SANTA CATALINA NRO. 1713 CENTRO CHONGOYAPE LAMBAYEQUE - CHICLAYO - CHONGOYAPE',
            'fechaalta' => $fecha,
            'created_at' => $ahora,
            'updated_at' => $ahora
        ]);

        DB::table('empresa')->insert([
            'ruc' => '20600154754',
            'razonsocial' => 'UBICACION SATELITAL GPS E.I.R.L',
            'direccionfiscal' => 'AV. JUAN VELASCO ALVARADO MZA. B LOTE. 01 URB. FELIX M. DI (A 4 CDRAS GRIFO PRIMAX) LAMBAYEQUE - CHICLAYO - JOSE LEONARDO ORTIZ',
            'fechaalta' => $fecha,
            'created_at' => $ahora,
            'updated_at' => $ahora
        ]);

        DB::table('empresa')->insert([
            'ruc' => '12345678901',
            'razonsocial' => 'EMPRESA ABC SAC',
            'direccionfiscal' => 'DIRECCIÓN',
            'fechaalta' => $fecha,
            'created_at' => $ahora,
            'updated_at' => $ahora
        ]);
    }
}
