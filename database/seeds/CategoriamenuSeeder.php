<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriamenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $ahora = new DateTime;
        DB::table('categoriamenu')->insert([
            'nombre' => 'Usuarios',
            'orden' => '1',
            'icono' => 'icon-users',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);

        DB::table('categoriamenu')->insert([
            'nombre' => 'Administración',
            'orden' => '2',
            'icono' => 'icon-cogs',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);

        DB::table('categoriamenu')->insert([
            'nombre' => 'Documentos',
            'orden' => '1',
            'icono' => 'icon-stack',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);

        DB::table('categoriamenu')->insert([
            'nombre' => 'Configuración',
            'orden' => '1',
            'icono' => 'icon-wrench',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);

        DB::table('categoriamenu')->insert([
            'nombre' => 'Flujo de trabajo',
            'orden' => '3',
            'icono' => 'icon-equalizer2',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
    }
}
