<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OpcionmenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $ahora = new DateTime;
        DB::table('opcionmenu')->insert([
            'nombre' => 'Categoria menu',
            'orden' => '1',
            'link' => 'categoriamenu.index',
            'icono' => 'icon-medal2',
            'categoriamenu_id' => 1,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);

        DB::table('opcionmenu')->insert([
            'nombre' => 'Opción Menu',
            'orden' => '2',
            'link' => 'opcionmenu.index',
            'icono' => 'icon-hyperlink',
            'categoriamenu_id' => 1,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);

        DB::table('opcionmenu')->insert([
            'nombre' => 'Tipos de usuario',
            'orden' => '4',
            'link' => 'tipousuario.index',
            'icono' => 'icon-user-tie',
            'categoriamenu_id' => 1,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);

        DB::table('opcionmenu')->insert([
            'nombre' => 'Usuario',
            'orden' => '5',
            'link' => 'usuario.index',
            'icono' => 'icon-user',
            'categoriamenu_id' => 1,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('opcionmenu')->insert([
            'nombre' => 'Bitacora',
            'orden' => '6',
            'link' => 'bitacora.index',
            'icono' => 'icon-wall',
            'categoriamenu_id' => 1,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);

        DB::table('opcionmenu')->insert([
            'nombre' => 'Empresa',
            'orden' => '1',
            'link' => 'empresa.index',
            'icono' => 'icon-office',
            'categoriamenu_id' => 2,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);

        DB::table('opcionmenu')->insert([
            'nombre' => 'Representante',
            'orden' => '2',
            'link' => 'representante.index',
            'icono' => 'icon-certificate',
            'categoriamenu_id' => 2,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);

        // documento
        DB::table('opcionmenu')->insert([
            'nombre' => 'Tipo documento',
            'orden' => '1',
            'link' => 'tipodocumento.index',
            'icono' => 'icon-folder',
            'categoriamenu_id' => 3,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('opcionmenu')->insert([
            'nombre' => 'Reglas',
            'orden' => '2',
            'link' => 'regla.index',
            'icono' => 'icon-hammer2',
            'categoriamenu_id' => 3,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('opcionmenu')->insert([
            'nombre' => 'Flujo trabajo',
            'orden' => '3',
            'link' => 'flujotrabajo.index',
            'icono' => 'icon-tab',
            'categoriamenu_id' => 5,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('opcionmenu')->insert([
            'nombre' => 'Tarea',
            'orden' => '5',
            'link' => 'tarea.index',
            'icono' => 'icon-calendar',
            'categoriamenu_id' => 5,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('opcionmenu')->insert([
            'nombre' => 'Respuesta',
            'orden' => '6',
            'link' => 'respuesta.index',
            'icono' => 'icon-bubbles2',
            'categoriamenu_id' => 3,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('opcionmenu')->insert([
            'nombre' => 'Documentos',
            'orden' => '7',
            'link' => 'documento.index',
            'icono' => 'icon-files-empty',
            'categoriamenu_id' => 3,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('opcionmenu')->insert([
            'nombre' => 'Datos personales',
            'orden' => '1',
            'link' => 'datos.index',
            'icono' => 'icon-vcard',
            'categoriamenu_id' => 4,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
    }
}
