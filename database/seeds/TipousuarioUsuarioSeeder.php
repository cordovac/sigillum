<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipousuarioUsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipousuario_usuario')->insert([
            'tipousuario_id' => 1,
            'usuario_id' => 1,
        ]);
        DB::table('tipousuario_usuario')->insert([
            'tipousuario_id' => 2,
            'usuario_id' => 2,
        ]);
        DB::table('tipousuario_usuario')->insert([
            'tipousuario_id' => 2,
            'usuario_id' => 3,
        ]);
        DB::table('tipousuario_usuario')->insert([
            'tipousuario_id' => 2,
            'usuario_id' => 4,
        ]);
    }
}
