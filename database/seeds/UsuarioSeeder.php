<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Laravolt\Avatar\Facade as Avatar;


class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $usuario = new User();
        $usuario->name = 'Rubén Dario Chanamé Sánchez';
        $usuario->password = Hash::make('123456');
        $usuario->estado = 'H';
        $usuario->email = 'ruben@gmail.com';
        $usuario->representante_id = 1;
        $usuario->superusuario = true;
        $usuario->save();
        $avatar = Avatar::create($usuario->name)->getImageObject()->encode('png');
        Storage::put('public/avatars/' . $usuario->id . '/avatar.png', (string)$avatar);

        $usuario = new User();
        $usuario->name = 'David Chanamé Vallejos';
        $usuario->password = Hash::make('123456');
        $usuario->estado = 'H';
        $usuario->email = 'david@gmail.com';
        $usuario->representante_id = 2;
        $usuario->superusuario = false;
        $usuario->save();
        $avatar = Avatar::create($usuario->name)->getImageObject()->encode('png');
        Storage::put('public/avatars/' . $usuario->id . '/avatar.png', (string)$avatar);

        $usuario = new User();
        $usuario->name = 'Ricardo Córdova Castro';
        $usuario->password = Hash::make('123456');
        $usuario->estado = 'H';
        $usuario->email = 'ricardo@gmail.com';
        $usuario->representante_id = 3;
        $usuario->superusuario = false;
        $usuario->save();
        $avatar = Avatar::create($usuario->name)->getImageObject()->encode('png');
        Storage::put('public/avatars/' . $usuario->id . '/avatar.png', (string)$avatar);

        $usuario = new User();
        $usuario->name = 'Guillermo Perez Gonzales';
        $usuario->password = Hash::make('123456');
        $usuario->estado = 'H';
        $usuario->email = 'guillermo@gmail.com';
        $usuario->representante_id = 4;
        $usuario->superusuario = false;
        $usuario->save();
        $avatar = Avatar::create($usuario->name)->getImageObject()->encode('png');
        Storage::put('public/avatars/' . $usuario->id . '/avatar.png', (string)$avatar);
    }
}
