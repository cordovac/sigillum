<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DetalleflujotrabajoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $ahora = new DateTime;
        // Inicio
        DB::table('detalle_flujotrabajo')->insert([
            'secuencia' => 'I',
            'tarea_id' => 1,
            'flujotrabajo_id' => 1,
            'usuario_id' => 1,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);

        // Fin
        DB::table('detalle_flujotrabajo')->insert([
            'secuencia' => 'F',
            'tarea_id' => 2,
            'flujotrabajo_id' => 1,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);


        DB::table('detalle_flujotrabajo')->insert([
            'secuencia' => 1,
            'tarea_id' => 3,
            'editararchivos' => true,
            'flujotrabajo_id' => 1,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);

        DB::table('detalle_flujotrabajo')->insert([
            'secuencia' => 2,
            'tarea_id' => 4,
            'flujotrabajo_id' => 1,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);

        // Creamos respuestas

        // Siguiente
        DB::table('respuesta_detalleflujo')->insert([
            'descripcion' => 'Ir al siguiente paso',
            'detalleflujotrabajo_id' => 3,
            'respuesta_id' => 1,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        // Anterior
        DB::table('respuesta_detalleflujo')->insert([
            'descripcion' => 'Ir al anterior paso',
            'detalleflujotrabajo_id' => 3,
            'respuesta_id' => 2,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        // Inicio
        DB::table('respuesta_detalleflujo')->insert([
            'descripcion' => 'Ir al inicio',
            'detalleflujotrabajo_id' => 3,
            'respuesta_id' => 3,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        // Fin
        DB::table('respuesta_detalleflujo')->insert([
            'descripcion' => 'Ir al fin',
            'detalleflujotrabajo_id' => 3,
            'respuesta_id' => 4,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        // Nodo
        DB::table('respuesta_detalleflujo')->insert([
            'descripcion' => 'Ir al paso 4',
            'detalleflujotrabajo_id' => 3,
            'respuesta_id' => 5,
            'secuencia' => 4,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);

        // Siguiente
        DB::table('respuesta_detalleflujo')->insert([
            'descripcion' => 'Ir al siguiente paso',
            'detalleflujotrabajo_id' => 4,
            'respuesta_id' => 1,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        // Anterior
        DB::table('respuesta_detalleflujo')->insert([
            'descripcion' => 'Ir al anterior paso',
            'detalleflujotrabajo_id' => 4,
            'respuesta_id' => 2,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        // Inicio
        DB::table('respuesta_detalleflujo')->insert([
            'descripcion' => 'Ir al inicio',
            'detalleflujotrabajo_id' => 4,
            'respuesta_id' => 3,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        // Fin
        DB::table('respuesta_detalleflujo')->insert([
            'descripcion' => 'Ir al fin',
            'detalleflujotrabajo_id' => 4,
            'respuesta_id' => 4,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        // Nodo
        DB::table('respuesta_detalleflujo')->insert([
            'descripcion' => 'Ir al paso 4',
            'detalleflujotrabajo_id' => 4,
            'respuesta_id' => 5,
            'secuencia' => 4,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);


        DB::table('detalleflujotrabajo_tipousuario')->insert([
            'tipousuario_id' => 2,
            'detalleflujotrabajo_id' => 3,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);



        DB::table('detalleflujotrabajo_tipousuario')->insert([
            'tipousuario_id' => 2,
            'detalleflujotrabajo_id' => 4,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);

        DB::table('detalle_flujotrabajo')->insert([
            'secuencia' => 3,
            'tarea_id' => 5,
            'flujotrabajo_id' => 1,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('detalleflujotrabajo_tipousuario')->insert([
            'tipousuario_id' => 3,
            'detalleflujotrabajo_id' => 5,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);

        DB::table('detalle_flujotrabajo')->insert([
            'secuencia' => 4,
            'editararchivos' => true,
            'tarea_id' => 6,
            'flujotrabajo_id' => 1,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('detalleflujotrabajo_tipousuario')->insert([
            'tipousuario_id' => 4,
            'detalleflujotrabajo_id' => 6,
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
    }
}
