<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReglaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $ahora = new DateTime;
        DB::table('regla')->insert([
            'codigo' => 'required',
            'descripcion' => 'Obligatorio',
            'observacion' => 'El campo bajo validación es obligatorio',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('regla')->insert([
            'codigo' => 'nullable',
            'descripcion' => 'No obligatorio',
            'observacion' => 'El campo bajo validación no es obligatorio',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('regla')->insert([
            'codigo' => 'in',
            'descripcion' => 'Dentro de una lista',
            'observacion' => 'El campo bajo validación debe estar dentro de una lista de valores dada',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('regla')->insert([
            'codigo' => 'string',
            'descripcion' => 'Texto',
            'observacion' => 'El campo bajo validación debe ser una cadena',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('regla')->insert([
            'codigo' => 'integer',
            'descripcion' => 'Número entero',
            'observacion' => 'El campo bajo validación debe ser un entero.',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('regla')->insert([
            'codigo' => 'numeric',
            'descripcion' => 'Número decimal',
            'observacion' => 'El campo bajo validación debe ser decimal.',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('regla')->insert([
            'codigo' => 'date_format',
            'descripcion' => 'Fecha',
            'observacion' => 'El campo bajo validación debe tener el formato fecha que se le indique, ejemplo: d/m/Y, donde d->Día, m->Mes, Y->Año',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
    }
}
