<?php

use App\Librerias\Libreria;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TareaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $ahora = new DateTime;
        DB::table('tarea')->insert([
            'codigo' => 'I',
            'descripcion' => 'Inicio',
            'editable' => 0,
            'created_at' => $ahora,
            'updated_at' => $ahora
        ]);
        DB::table('tarea')->insert([
            'codigo' => 'F',
            'descripcion' => 'Fin',
            'editable' => 0,
            'created_at' => $ahora,
            'updated_at' => $ahora
        ]);
        for ($i = 1; $i < 21; $i++) {
            DB::table('tarea')->insert([
                'codigo' => 'TR' . Libreria::completarCeros($i, 3),
                'descripcion' => 'TAREA ' . Libreria::completarCeros($i, 3),
                'created_at' => $ahora,
                'updated_at' => $ahora
            ]);
        }
    }
}
