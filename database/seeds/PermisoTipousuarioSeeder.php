<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermisoTipousuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permisos = DB::table('permiso')->get();
        foreach ($permisos as $key => $value) {
            DB::table('permiso_tipousuario')->insert(array(
                'tipousuario_id' => 1,
                'permiso_id' => $value->id,
            ));
        }

        DB::table('permiso_tipousuario')->insert(array(
            'tipousuario_id' => 2,
            'permiso_id' => 46,
        ));

        DB::table('permiso_tipousuario')->insert(array(
            'tipousuario_id' => 2,
            'permiso_id' => 47,
        ));

        DB::table('permiso_tipousuario')->insert(array(
            'tipousuario_id' => 2,
            'permiso_id' => 48
        ));

        DB::table('permiso_tipousuario')->insert(array(
            'tipousuario_id' => 2,
            'permiso_id' => 49,
        ));
    }
}
