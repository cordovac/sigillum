<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RespuestaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $ahora = new DateTime;
        DB::table('respuesta')->insert([
            'descripcion' => 'Siguiente',
            'accion' => 'siguiente',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('respuesta')->insert([
            'descripcion' => 'Anterior',
            'accion' => 'anterior',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('respuesta')->insert([
            'descripcion' => 'Ir al inicio del flujo',
            'accion' => 'inicio',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('respuesta')->insert([
            'descripcion' => 'Ir al fin del flujo',
            'accion' => 'fin',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('respuesta')->insert([
            'descripcion' => 'Ir a a la tarea...',
            'accion' => 'nodo',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
//        DB::table('respuesta')->insert([
//            'descripcion' => 'Ir al flujo de trabajo...',
//            'accion' => 'flujo',
//            'created_at' => $ahora,
//            'updated_at' => $ahora,
//        ]);
    }
}
