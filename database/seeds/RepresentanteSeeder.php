<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RepresentanteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $ahora = new DateTime;
        DB::table('representante')->insert([
            'nombres' => 'Rubén Dario',
            'apellidopaterno' => 'Chanamé',
            'apellidomaterno' => 'Sánchez',
            'dni' => '46780102',
            'rol' => 'Jefe de Almacén',
            'created_at' => $ahora,
            'updated_at' => $ahora,
            'empresa_id' => 1,
        ]);

        DB::table('representante')->insert([
            'nombres' => 'David Sixto',
            'apellidopaterno' => 'Chanamé',
            'apellidomaterno' => 'Vallejos',
            'dni' => '17403019',
            'rol' => 'Jefe de RRHH',
            'created_at' => $ahora,
            'updated_at' => $ahora,
            'empresa_id' => 1,
        ]);

        DB::table('representante')->insert([
            'nombres' => 'Ricardo',
            'apellidopaterno' => 'Cordova',
            'apellidomaterno' => 'Castro',
            'dni' => '87654321',
            'rol' => 'Jefe de Contabilidad',
            'created_at' => $ahora,
            'updated_at' => $ahora,
            'empresa_id' => 2,
        ]);

        DB::table('representante')->insert([
            'nombres' => 'Guillermo',
            'apellidopaterno' => 'Perez',
            'apellidomaterno' => 'Gonzales',
            'dni' => '12345678',
            'rol' => 'Jefe de Almacén',
            'created_at' => $ahora,
            'updated_at' => $ahora,
            'empresa_id' => 3,
        ]);
    }
}
