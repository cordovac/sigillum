<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipodocumentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $ahora = new DateTime;
        DB::table('tipodocumento')->insert([
            'descripcion' => 'PARTIDA DE NACIMIENTO',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('tipodocumento')->insert([
            'descripcion' => 'TIPO DE DOCUMENTO 2',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
        DB::table('tipodocumento')->insert([
            'descripcion' => 'TIPO DE DOCUMENTO 3',
            'created_at' => $ahora,
            'updated_at' => $ahora,
        ]);
    }
}
