<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermisoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $ahora = new DateTime;
        $opciones = DB::table('opcionmenu')->get();
        foreach ($opciones as $key => $value) {
            $codigo = explode('.', $value->link);
            $nuevocodigo = $codigo[0];
            DB::table('permiso')->insert(array(
                'nombre' => 'can list ' . $nuevocodigo,
                'codigo' => 'list_' . $nuevocodigo,
                'opcionmenu_id' => $value->id,
                'created_at' => $ahora,
                'updated_at' => $ahora
            ));

            DB::table('permiso')->insert(array(
                'nombre' => 'can read ' . $nuevocodigo,
                'codigo' => 'read_' . $nuevocodigo,
                'opcionmenu_id' => $value->id,
                'created_at' => $ahora,
                'updated_at' => $ahora
            ));

            DB::table('permiso')->insert(array(
                'nombre' => 'can add ' . $nuevocodigo,
                'codigo' => 'add_' . $nuevocodigo,
                'opcionmenu_id' => $value->id,
                'created_at' => $ahora,
                'updated_at' => $ahora
            ));

            DB::table('permiso')->insert(array(
                'nombre' => 'can change ' . $nuevocodigo,
                'codigo' => 'change_' . $nuevocodigo,
                'opcionmenu_id' => $value->id,
                'created_at' => $ahora,
                'updated_at' => $ahora
            ));

            DB::table('permiso')->insert(array(
                'nombre' => 'can delete ' . $nuevocodigo,
                'codigo' => 'delete_' . $nuevocodigo,
                'opcionmenu_id' => $value->id,
                'created_at' => $ahora,
                'updated_at' => $ahora
            ));
        }

        // PERMISOS MANUALES

        // permitir ver listado de detalleflujotrabajo
        $nuevocodigo = 'detalleflujotrabajo';
        $codigopadre = 'flujotrabajo';
        $opcionmenuid = 10;
        DB::table('permiso')->insert(array(
            'nombre' => 'can list ' . $nuevocodigo,
            'codigo' => $codigopadre.'_list_' . $nuevocodigo,
            'opcionmenu_id' => $opcionmenuid,
            'created_at' => $ahora,
            'updated_at' => $ahora
        ));

        DB::table('permiso')->insert(array(
            'nombre' => 'can read ' . $nuevocodigo,
            'codigo' => $codigopadre.'_read_' . $nuevocodigo,
            'opcionmenu_id' => $opcionmenuid,
            'created_at' => $ahora,
            'updated_at' => $ahora
        ));

        DB::table('permiso')->insert(array(
            'nombre' => 'can add ' . $nuevocodigo,
            'codigo' => $codigopadre.'_add_' . $nuevocodigo,
            'opcionmenu_id' => $opcionmenuid,
            'created_at' => $ahora,
            'updated_at' => $ahora
        ));

        DB::table('permiso')->insert(array(
            'nombre' => 'can change ' . $nuevocodigo,
            'codigo' => $codigopadre.'_change_' . $nuevocodigo,
            'opcionmenu_id' => $opcionmenuid,
            'created_at' => $ahora,
            'updated_at' => $ahora
        ));

        DB::table('permiso')->insert(array(
            'nombre' => 'can delete ' . $nuevocodigo,
            'codigo' => $codigopadre.'_delete_' . $nuevocodigo,
            'opcionmenu_id' => $opcionmenuid,
            'created_at' => $ahora,
            'updated_at' => $ahora
        ));
    }
}
