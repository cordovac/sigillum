<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDocumento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documento', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero', 30);
            $table->boolean('activo')->default(1);
            $table->enum('estado', ['E', 'F', 'R'])
                ->comment('E->En proceso, F->Finalizado, R->Rechazado')
                ->default('E');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('flujotrabajo_id');
            $table->unsignedBigInteger('tipodocumento_id');
            $table->foreign('flujotrabajo_id')->references('id')->on('flujotrabajo')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('tipodocumento_id')->references('id')->on('tipodocumento')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documento');
    }
}
