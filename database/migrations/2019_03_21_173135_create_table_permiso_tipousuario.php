<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePermisoTipousuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permiso_tipousuario', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('tipousuario_id');
            $table->unsignedBigInteger('permiso_id');
            $table->foreign('tipousuario_id')->references('id')->on('tipousuario')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('permiso_id')->references('id')->on('permiso')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permiso_tipousuario');
    }
}
