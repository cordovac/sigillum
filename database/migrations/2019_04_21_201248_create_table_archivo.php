<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableArchivo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archivo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 200);
            $table->string('ruta', 200);
            $table->boolean('activo')->default(1);
            $table->enum('accion', ['C', 'U', 'D', 'M'])->default('C'); // C->creado, U->actualizado, D->eliminado, M->Mantenido del paso anterior
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('historial_id');
            $table->unsignedBigInteger('archivo_id')->nullable();
            $table->foreign('historial_id')->references('id')->on('historial')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('archivo_id')->references('id')->on('archivo')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archivo');
    }
}
