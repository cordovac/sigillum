<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBitacora extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitacora', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('accion', ['C', 'R', 'U', 'D'])->comment('C->Create, R->Read, U->Update, D->Delete');
            $table->timestamp('fecha');
            $table->string('ip', 100);
            $table->unsignedBigInteger('registroid');
            $table->string('tabla', 45);
            $table->text('detalle');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('usuario_id');
            $table->foreign('usuario_id')->references('id')->on('usuario')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitacora');
    }
}
