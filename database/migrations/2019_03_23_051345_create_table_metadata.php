<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMetadata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metadata', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 50);
            $table->string('codigo', 50);
            $table->enum('tipocontrol', ['text', 'select', 'textarea', 'checkbox', 'radio'])->default('text')->comment('text -> Para caja de texto, textarea -> Caja de texto tipo textarea, select -> Para combos');
            $table->boolean('activo')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('tipodocumento_id');
            $table->foreign('tipodocumento_id')->references('id')->on('tipodocumento')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metadata');
    }
}
