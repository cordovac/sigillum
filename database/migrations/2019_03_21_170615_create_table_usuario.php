<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 150);
            $table->string('password');
            $table->enum('estado', ['H', 'I'])->comment('H -> Habilitado, I -> Inhabilitado');
            $table->string('email')->unique();
            $table->boolean('superusuario')->default(false);
            $table->string('avatar')->default('avatar.png');
            $table->rememberToken();
            $table->timestamp('email_verified_at')->nullable();
            $table->unsignedBigInteger('representante_id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('representante_id')->references('id')->on('representante')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario');
    }
}
