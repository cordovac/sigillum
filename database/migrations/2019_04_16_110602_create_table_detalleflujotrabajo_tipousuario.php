<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetalleflujotrabajoTipousuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalleflujotrabajo_tipousuario', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('detalleflujotrabajo_id');
            $table->unsignedBigInteger('tipousuario_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('detalleflujotrabajo_id')->references('id')->on('detalle_flujotrabajo')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('tipousuario_id')->references('id')->on('tipousuario')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalleflujotrabajo_tipousuario');
    }
}
