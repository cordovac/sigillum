<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCategoriamenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categoriamenu', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 50);
            $table->integer('orden')->unsigned();
            $table->string('icono', 50);
            $table->boolean('activo')->default(1);
            $table->unsignedBigInteger('categoriamenu_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('categoriamenu_id')->references('id')->on('categoriamenu')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categoriamenu');
    }
}
