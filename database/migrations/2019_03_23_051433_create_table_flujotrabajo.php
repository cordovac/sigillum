<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFlujotrabajo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flujotrabajo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo', 10);
            $table->string('descripcion', 50);
            $table->boolean('activo')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('reporte_id')->nullable();
            $table->foreign('reporte_id')->references('id')->on('reporte')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flujotrabajo');
    }
}
