<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHistorial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('fechaoperacion')->nullable();
            $table->text('observacion')->nullable();
            $table->boolean('pendiente')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('usuario_id')->nullable();
            $table->unsignedBigInteger('usuarioemisor_id')->nullable();
            $table->unsignedBigInteger('respuestadetalleflujo_id')->nullable();
            $table->unsignedBigInteger('documento_id');
            $table->unsignedBigInteger('detalleflujotrabajo_id');
            $table->unsignedBigInteger('historial_id')->nullable();
            $table->foreign('usuario_id')->references('id')->on('usuario')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('usuarioemisor_id')->references('id')->on('usuario')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('respuestadetalleflujo_id')->references('id')->on('respuesta_detalleflujo')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('documento_id')->references('id')->on('documento')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('detalleflujotrabajo_id')->references('id')->on('detalle_flujotrabajo')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('historial_id')->references('id')->on('historial')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial');
    }
}
