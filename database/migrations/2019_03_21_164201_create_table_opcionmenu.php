<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOpcionmenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opcionmenu', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 50);
            $table->string('link', 50);
            $table->integer('orden')->unsigned();
            $table->string('icono', 50);
            $table->boolean('activo')->default(1);
            $table->unsignedBigInteger('categoriamenu_id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('categoriamenu_id')->references('id')->on('categoriamenu')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opcionmenu');
    }
}
