<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePermiso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permiso', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 45);
            $table->string('codigo', 45);
            $table->boolean('superusuario')->default(false);
            $table->boolean('activo')->default(true);
            $table->unsignedBigInteger('opcionmenu_id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('opcionmenu_id')->references('id')->on('opcionmenu')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permiso');
    }
}
