<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetalleFlujotrabajo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_flujotrabajo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('secuencia', 2);
            $table->boolean('activo')->default(1);
            $table->boolean('editararchivos')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('tarea_id');
            $table->unsignedBigInteger('flujotrabajo_id');
            $table->unsignedBigInteger('usuario_id')->nullable();
            $table->foreign('tarea_id')->references('id')->on('tarea')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('flujotrabajo_id')->references('id')->on('flujotrabajo')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('usuario_id')->references('id')->on('usuario')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_flujotrabajo');
    }
}
