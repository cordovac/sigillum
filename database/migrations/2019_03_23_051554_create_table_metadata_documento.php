<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMetadataDocumento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metadata_documento', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('valor')->nullable();
            $table->unsignedBigInteger('metadata_id');
            $table->unsignedBigInteger('documento_id');
            $table->foreign('metadata_id')->references('id')->on('metadata')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('documento_id')->references('id')->on('documento')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metadata_documento');
    }
}
