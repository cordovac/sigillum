<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRespuestaDetalleflujo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuesta_detalleflujo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion', 100);
            $table->unsignedInteger('secuencia')->nullable();
            $table->unsignedBigInteger('detalleflujotrabajo_id');
            $table->unsignedBigInteger('respuesta_id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('detalleflujotrabajo_id')->references('id')->on('detalle_flujotrabajo')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('respuesta_id')->references('id')->on('respuesta')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respuesta_detalleflujo');
    }
}
