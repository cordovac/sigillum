<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMetadataRegla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metadata_regla', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('valor', 120)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('metadata_id');
            $table->unsignedBigInteger('regla_id');
            $table->foreign('metadata_id')->references('id')->on('metadata')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('regla_id')->references('id')->on('regla')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metadata_regla');
    }
}
