<?php

use App\Representante;
use Illuminate\Support\Facades\Auth;

$user = Auth::user();
$person = Representante::find($user->representante_id);
?>
<div class="sidebar sidebar-light sidebar-main sidebar-fixed sidebar-expand-md">

    {{-- Sidebar mobile toggler --}}
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    {{-- /sidebar mobile toggler --}}

    {{-- Sidebar content --}}
    <div class="sidebar-content">

        {{-- User menu --}}
        <div class="sidebar-user-material">
            <div class="sidebar-user-material-body">
                <div class="card-body text-center">
                    <a href="#">
                        <img src="{{ $user->avatar_url }}"
                             class="img-fluid rounded-circle shadow-1 mb-3" width="80" height="80" alt="">
                    </a>
                    <h6 class="mb-0 text-white text-shadow-dark">{{ $person->apellidopaterno.' '.$person->apellidomaterno.', '.$person->nombres }}</h6>
                    <span class="font-size-sm text-white text-shadow-dark">{{ $user->unicotipousuario->tipousuario->nombre }}</span><br>
                    <span class="font-size-sm text-white text-shadow-dark">{{ $person->empresa->razonsocial }}</span>
                </div>
                <div class="sidebar-user-material-footer">
                    <a href="#user-nav"
                       class="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle"
                       data-toggle="collapse"><span>Mi cuenta</span></a>
                </div>
            </div>
            <div class="collapse" id="user-nav">
                <ul class="nav nav-sidebar">
                    <li class="nav-item">
                        <a href="#" onclick="modal('usuario/editdatos/{{ $user->id }}', 'Modificar datos')"
                           class="nav-link">
                            <i class="icon-user-plus"></i>
                            <span>Mis datos</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" onclick="modal('usuario/editclave/{{ $user->id }}', 'Modificar contraseña')"
                           class="nav-link">
                            <i class="icon-key"></i>
                            <span>Cambiar Contraseña</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="logout" class="nav-link">
                            <i class="icon-switch2"></i>
                            <span>Cerrar Sesión</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        {{-- /user menu --}}

        {{-- Main navigation --}}
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                {{-- Main --}}
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Principal</div>
                    <i class="icon-menu" title="Main"></i>
                </li>
                <li class="nav-item">
                    <a onclick="cargarRuta('{{ URL::route('home.main') }}', 'content');" class="nav-link">
                        <i class="icon-home4"></i>
                        <span>Inicio</span>
                    </a>
                </li>

                {!! $menu !!}

                {{-- /main --}}

            </ul>
        </div>
        {{-- /main navigation --}}

    </div>
    {{-- /sidebar content --}}

</div>