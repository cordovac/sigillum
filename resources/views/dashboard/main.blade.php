{{--Page header--}}
@section('pageheader')
    <div id="cabecerahome" class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <div class="d-flex justify-content-center">
                    <a onclick="cargarRuta('{{ URL::route('home.receptor') }}', 'contenido');" class="btn btn-link btn-float font-weight-semibold text-default">
                        <i id="i-receptor" class="icon-hour-glass2 text-default-300"></i>
                        <span>Pendientes</span>
                    </a>
                    <a onclick="cargarRuta('{{ URL::route('home.emisor') }}', 'contenido');" class="btn btn-link btn-float font-weight-semibold text-default">
                        <i id="i-emisor" class="icon-folder-upload text-default-300"></i>
                        <span>Emitidos</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
@show
{{--/page header--}}

{{--Content area--}}
@section('contenido')
    <div class="content" id="contenido">
        @yield('contenido')
    </div>
@show
{{--/content area--}}

{{--Footer--}}
@include('dashboard.footer')
{{--/footer--}}
<script>
    $(document).ready(function () {
        cargarRuta('{{ URL::route('home.receptor') }}', 'contenido');

    });
</script>