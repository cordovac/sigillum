<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="shortcut icon" type="image/png" href="login_assets/assets/images/isotipo.png"/>

    {{--Global stylesheets--}}
    {!! Html::style('https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900') !!}
    {!! Html::style('global_assets/css/icons/icomoon/styles.css') !!}
    {!! Html::style('assets/css/bootstrap.min.css') !!}
    {!! Html::style('assets/css/bootstrap_limitless.min.css') !!}
    {!! Html::style('assets/css/layout.min.css') !!}
    {!! Html::style('assets/css/components.min.css') !!}
    {!! Html::style('assets/css/colors.min.css') !!}
    {{--/global stylesheets--}}

    {{--Core JS files --}}
    {!! Html::script('global_assets/js/main/jquery.min.js') !!}
    {!! Html::script('global_assets/js/main/bootstrap.bundle.min.js') !!}
    {!! Html::script('global_assets/js/plugins/loaders/blockui.min.js') !!}
    {!! Html::script('global_assets/js/plugins/ui/ripple.min.js') !!}
    {{--/core JS files--}}

    {{--Calendar--}}
    {!! Html::script('global_assets/js/plugins/extensions/jquery_ui/interactions.min.js') !!}
    {!! Html::script('global_assets/js/plugins/forms/styling/switchery.min.js') !!}
    {!! Html::script('global_assets/js/plugins/ui/moment/moment.min.js') !!}
    {!! Html::script('global_assets/js/plugins/ui/fullcalendar/fullcalendar.min.js') !!}
    {!! Html::script('global_assets/js/plugins/ui/fullcalendar/lang/es.js') !!}
    {{--/calendar--}}

    {{--Acordeon--}}
    {!! Html::script('global_assets/js/plugins/extensions/jquery_ui/touch.min.js') !!}
    {{--/Acordeon--}}

    {{--Notifications--}}
    {!! Html::script('global_assets/js/plugins/notifications/pnotify.min.js') !!}
    {{--/Notifications--}}

    {{--Date picker--}}
    {!! Html::script('global_assets/js/plugins/pickers/daterangepicker.js') !!}
    {!! Html::script('global_assets/js/plugins/pickers/anytime.min.js') !!}
    {!! Html::script('global_assets/js/plugins/pickers/pickadate/picker.js') !!}
    {!! Html::script('global_assets/js/plugins/pickers/pickadate/picker.date.js') !!}
    {!! Html::script('global_assets/js/plugins/pickers/pickadate/picker.time.js') !!}
    {!! Html::script('global_assets/js/plugins/pickers/pickadate/legacy.js') !!}
    {{--/Date Pciker--}}

    {{--Databables--}}
    {!! Html::script('global_assets/js/plugins/tables/datatables/datatables.min.js') !!}
    {{--/Databables--}}

    {{-- Wizard --}}
    {!! Html::script('global_assets/js/plugins/forms/wizards/steps.min.js') !!}
    {!! Html::script('global_assets/js/plugins/forms/validation/validate.min.js') !!}
    {{-- /Wizard --}}

    {{--Modales--}}
    {!! Html::script('global_assets/js/plugins/notifications/bootbox.min.js') !!}
    {!! Html::script('global_assets/js/plugins/forms/selects/select2.min.js') !!}
    {!! Html::script('global_assets/js/plugins/forms/styling/uniform.min.js') !!}
    {{--/modales--}}

    {{--Upload files--}}
    {!! Html::script('global_assets/js/plugins/uploaders/fileinput/plugins/purify.min.js') !!}
    {!! Html::script('global_assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js') !!}
    {!! Html::script('global_assets/js/plugins/uploaders/fileinput/fileinput.min.js') !!}
    {{--/Upload files--}}

    {{--Site--}}
    {!! Html::script('assets/js/app.js') !!}
    {!! Html::script('js/funciones.js') !!}
    {!! Html::script('global_assets/js/demo_pages/fullcalendar_advanced.js') !!}
    {{--/site--}}


    {{--Buttons--}}
    {!! Html::script('global_assets/js/plugins/buttons/spin.min.js') !!}
    {!! Html::script('global_assets/js/plugins/buttons/ladda.min.js') !!}
    {!! Html::script('global_assets/js/demo_pages/components_buttons.js') !!}
    {{--/buttons--}}

    {{-- jquery.inputmask: para mascaras en cajas de texto --}}
    {!! Html::script('plugins/input-mask/jquery.inputmask.js') !!}
    {!! Html::script('plugins/input-mask/jquery.inputmask.extensions.js') !!}
    {!! Html::script('plugins/input-mask/jquery.inputmask.date.extensions.js') !!}
    {!! Html::script('plugins/input-mask/jquery.inputmask.numeric.extensions.js') !!}
    {!! Html::script('plugins/input-mask/jquery.inputmask.phone.extensions.js') !!}
    {!! Html::script('plugins/input-mask/jquery.inputmask.regex.extensions.js') !!}
    {{-- /jquery.inputmask: para mascaras en cajas de texto --}}

    {{--Databables--}}
    {!! Html::style('plugins/chosen/chosen.css') !!}
    {!! Html::style('css/bootstrap-nav-wizard.css') !!}
    {!! Html::style('plugins/bloodhound/typeahead.css') !!}
    {{--/Databables--}}

    <style>
        .bootbox.modal {
            overflow: auto !important;
        }
    </style>

</head>

<body class="navbar-top">

{{--Main navbar--}}
@include('dashboard.navbar')
{{--/main navbar--}}

{{--Page content--}}
<div class="page-content">

    {{--Main sidebar--}}
    @include('dashboard.sidebar')
    {{--/main sidebar--}}

    {{--Main content--}}
    <div class="content-wrapper" id="content">

        {{--Page header--}}
        @section('pageheader')
            <div id="cabecerahome" class="page-header page-header-light">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        <div class="d-flex justify-content-center">
                            <a onclick="cargarRuta('{{ URL::route('home.receptor') }}', 'contenido');" class="btn btn-link btn-float font-weight-semibold text-default">
                                <i id="i-receptor" class="icon-hour-glass2 text-default-300"></i>
                                <span>Pendientes</span>
                            </a>
                            <a onclick="cargarRuta('{{ URL::route('home.emisor') }}', 'contenido');" class="btn btn-link btn-float font-weight-semibold text-default">
                                <i id="i-emisor" class="icon-folder-upload text-default-300"></i>
                                <span>Emitidos</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @show
        {{--/page header--}}

        {{--Content area--}}
        @section('contenido')
            <div class="content" id="contenido">
                @yield('contenido')
            </div>
        @show
        {{--/content area--}}

        {{--Footer--}}
        @include('dashboard.footer')
        {{--/footer--}}
    </div>
    {{--/main content--}}
</div>
{{--/page content--}}
<script>
    $(document).ready(function () {
        cargarRuta('{{ URL::route('home.receptor') }}', 'contenido');
    });
</script>
</body>
</html>
