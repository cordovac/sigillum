<div class="navbar navbar-expand-lg navbar-light">
    <div class="text-center d-lg-none w-100">
        <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                data-target="#navbar-footer">
            <i class="icon-unfold mr-2"></i>
            Footer
        </button>
    </div>
    <div class="navbar-collapse collapse" id="navbar-footer">
        <span class="navbar-text">
            &copy; 2019. <a href="#">Sigillum</a>
        </span>
    </div>
</div>
{!! Html::script('plugins/chosen/chosen.jquery.js') !!}
{!! Html::script('plugins/bloodhound/bloodhound.min.js') !!}
{!! Html::script('plugins/bloodhound/typeahead.bundle.min.js') !!}