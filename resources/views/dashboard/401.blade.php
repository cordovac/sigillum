<div id="cabecerahome" class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <div class="d-flex justify-content-center">
                <h1 class="text-danger">Error 401: No tiene permisos suficientes</h1>
            </div>
        </div>
    </div>
</div>
<div class="content" id="contenido">
    <div class="row">
        <div class="col-xl-12">
            <!-- Traffic sources -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h3 class="card-title text-info"><i class="icon-user-lock"></i> Mensaje del sistema</h3>
                </div>
                <div class="card-body">
                    <h5 class="text-monospace">Contacte con el administrador del sistema para que le conceda los permisos necesarios</h5>
                </div>
            </div>
        </div>
    </div>
</div>