<div class="row">
    <div class="col-xl-12">
        <!-- Traffic sources -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h6 class="card-title text-uppercase">Seleccionar flujo de trabajo</h6>
            </div>
            <div class="card-body">
                {!! Form::open(array('route' => 'seguimiento.buscar', 'method' => 'POST' ,'onsubmit' => 'return false;', 'role' => 'form', 'autocomplete' => 'off', 'id' => 'formBusqueda'.$entidad))  !!}
                {!! Form::hidden('page', 1, array('id' => 'page')) !!}
                {!! Form::hidden('filas', 40, array('id' => 'filas')) !!}
                {!! Form::hidden('accion', 'listar', array('id' => 'accion')) !!}
                <div class="form-group row">
                    <label for="flujotrabajo_id" class="col-lg-3 col-form-label">Flujo de trabajo:</label>
                    <div class="col-lg-9">
                        {!! Form::select('flujotrabajo_id', $cboFlujotrabajo, NULL, array('class' => 'form-control form-control-select2', 'id' => 'flujotrabajo_id')) !!}
                    </div>
                </div>
                <div class="text-right">
                    {!! Form::button('Buscar <i class="icon-search4"></i>', array('class' => 'btn btn-success btn-sm', 'id' => 'btnBuscar', 'onclick' => 'actualizar();')) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xl-12">
        <!-- Traffic sources -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h6 class="card-title text-uppercase">Detalle de flujo de trabajo</h6>
            </div>
            <div id="listado{{ $entidad }}">
            </div>
        </div>
    </div>
</div>
<div class="card-group-control card-group-control-left" id="accordion-control">

</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(IDFORMBUSQUEDA + '{!! $entidad !!} :input[id="flujotrabajo_id"]').select2({});
        let ireceptor = $("#i-receptor");
        let iemisor = $("#i-emisor");
        ireceptor.removeClass('text-default-300');
        ireceptor.addClass('text-purple-300');
        iemisor.removeClass('text-purple-300');
        iemisor.addClass('text-default-300');
    });

    function actualizar() {
        let flujotrabajo_id = $(IDFORMBUSQUEDA + '{!! $entidad !!} :input[id="flujotrabajo_id"]').val();
        if (flujotrabajo_id !== "") {
            buscar('{{ $entidad }}');
            let url = "{{ URL::route('seguimiento.buscartareas', array("")) }}/" + flujotrabajo_id;
            cargarRuta(url, 'accordion-control');
        } else {
            alert("Seleccionar un flujo de trabajo")
        }

    }
</script>
