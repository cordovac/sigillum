<?php

use App\Representante;
use Illuminate\Support\Facades\Auth;

$user = Auth::user();
session(['usertype_id' => $user->usertype_id]);
$tipousuario_id = session('usertype_id');
$person = Representante::find($user->representante_id);
?>
<div class="navbar navbar-expand-md navbar-dark bg-indigo navbar-static fixed-top">
    <div class="navbar-brand">
        <a href="{{ URL::route('home') }}" class="d-inline-block">
            <img src="global_assets/images/logo_light_2.png" alt="">
        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ $user->avatar_url }}" class="rounded-circle" alt="">
                    <span>{{ $person->nombres }}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="logout" class="dropdown-item"><i class="icon-switch2"></i> Cerrar sesión</a>
                </div>
            </li>
        </ul>
    </div>
</div>