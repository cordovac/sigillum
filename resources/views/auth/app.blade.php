<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="shortcut icon" type="image/png" href="login_assets/assets/images/isotipo.png"/>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="login_assets/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="login_assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="login_assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="login_assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="login_assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="login_assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="login_assets/assets/js/main/jquery.min.js"></script>
    <script src="login_assets/assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="login_assets/assets/js/plugins/loaders/blockui.min.js"></script>
    <script src="login_assets/assets/js/plugins/ui/ripple.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="login_assets/assets/js/plugins/forms/styling/uniform.min.js"></script>

    <script src="login_assets/js/app.js"></script>
    <script src="login_assets/assets/js/demo_pages/login.js"></script>
    <!-- /theme JS files -->
</head>
<body>
<!-- Page content -->
<div class="page-content login-cover">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
        <div class="content d-flex justify-content-center align-items-center">

            <!-- Login form -->
        @yield('formulario')
        <!-- /login form -->

        </div>
        <!-- /content area -->

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

</body>
</html>
