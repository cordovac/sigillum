@extends('auth.app')

@section('formulario')
    <form class="login-form" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="card mb-0">
            <div class="card-body">
                <div class="text-center mb-3">

                    <img src="login_assets/assets/images/logo2x2.png" alt="" width="150px">
                    <h5 class="mb-0">Inicia sesión con tu cuenta</h5>
                    <span class="d-block text-muted">Tus credenciales</span>
                </div>

                <div class="form-group form-group-feedback form-group-feedback-left">
                    <input id="email" type="text" name="email" class="form-control" placeholder="Correo Electrónico"
                           value="{{ old('email') }}" required autofocus>
                    <div class="form-control-feedback">
                        <i class="icon-user text-muted"></i>
                    </div>
                    @if ($errors->has('email'))
                        <label id="username-error" class="validation-invalid-label"
                               for="username">{{ $errors->first('email') }}</label>
                    @endif

                </div>
                <div class="form-group form-group-feedback form-group-feedback-left{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="form-control" name="password" required>
                    <div class="form-control-feedback">
                        <i class="icon-lock2 text-muted"></i>
                    </div>
                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">Ingresar <i
                                class="icon-circle-right2"></i></button>
                </div>
            </div>
        </div>
    </form>
@endsection