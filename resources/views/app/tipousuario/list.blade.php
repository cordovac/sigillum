@if(count($lista) == 0)
    <h3 class="text-warning">No se encontraron resultados.</h3>
@else
    <table id="example1" class="table datatable-basic">
        <thead>
        <tr>
            @foreach($cabecera as $key => $value)
                <th @if((int)$value['numero'] > 1) colspan="{{ $value['numero'] }}" @endif>{!! $value['valor'] !!}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        <?php
        $contador = $inicio + 1;
        ?>
        @foreach ($lista as $key => $value)
            <tr>
                <td>{{ $contador }}</td>
                <td>{{ $value->nombre }}</td>
                @if($superusuario)
                    @if(!is_null($value->empresa_id))
                        <td>{{ $value->empresa->razonsocial }}</td>
                    @else
                        <td>{{ '' }}</td>
                    @endif
                @endif
                <td>{!! Form::button('<i class="icon-user-lock"></i> Permisos', array('onclick' => 'modal (\''.URL::route($ruta["permisos"], array($value->id, 'listar'=>'SI')).'\', \'Permisos\', this);', 'class' => 'btn btn-sm btn-default')) !!}</td>
                <td>{!! Form::button('<i class="icon-pencil4"></i> Editar', array('onclick' => 'modal (\''.URL::route($ruta["edit"], array($value->id, 'listar'=>'SI')).'\', \''.$titulo_modificar.'\', this);', 'class' => 'btn btn-warning btn-sm')) !!}</td>
                <td>{!! Form::button('<i class="icon-cross2"></i> Eliminar', array('onclick' => 'modal (\''.URL::route($ruta["delete"], array($value->id, 'SI')).'\', \''.$titulo_eliminar.'\', this);', 'class' => 'btn btn-danger btn-sm')) !!}</td>
            </tr>
            <?php
            $contador = $contador + 1;
            ?>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            @foreach($cabecera as $key => $value)
                <th @if((int)$value['numero'] > 1) colspan="{{ $value['numero'] }}" @endif>{!! $value['valor'] !!}</th>
            @endforeach
        </tr>
        </tfoot>
    </table>
    <script>
        $(document).ready(function () {
            // Basic datatable
            $('.datatable-basic').DataTable({
                pageLength: 100,
            });
        });
    </script>
@endif