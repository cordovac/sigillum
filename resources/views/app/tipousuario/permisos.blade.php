{!! Form::open(array('route' => array('tipousuario.guardarpermisos', $tipousuario->id,'id','false'), 'id' => 'formMantenimiento'.$entidad)) !!}
<div class="form-group border">
    {!! $vista !!}
</div>
<div class="form-group text-center">
    {{--    {!! Form::button('Guardar', array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardar(\''.$entidad.'\', this)')) !!}--}}
    {!! Form::button('Aceptar', array('class' => 'btn btn-success btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal((contadorModal - 1));')) !!}
</div>
{!! Form::close() !!}

<script type="text/javascript">
    $(document).ready(function () {
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}');
    });

    function submitFormpermiso(idformulario, permiso_id, action) {
        let parametros = $(idformulario).serialize();
        let accion = $(idformulario).attr('action').toLowerCase();
        accion = accion.split('id')[0] + permiso_id + '/' + action;
        let metodo = $(idformulario).attr('method').toLowerCase();
        return $.ajax({
            url: accion,
            type: metodo,
            data: parametros
        });
    }

    function guardarpermiso(entidad, permiso_id) {
        let idformulario = IDFORMMANTENIMIENTO + entidad;
        let respuesta = '';
        let action = '';
        if ($('#' + permiso_id).is(':checked')) {
            action = true;
        } else {
            action = false;
        }
        let data = submitFormpermiso(idformulario, permiso_id, action);
        data.done(function (msg) {
            respuesta = msg;
        }).fail(function (xhr, textStatus, errorThrown) {
            respuesta = 'ERROR';
        }).always(function () {
            if (respuesta === 'ERROR') {
            } else {
                if (respuesta !== 'OK') {
                    mostrarErrores(respuesta, idformulario, entidad);

                }
            }
        });
    }
</script>