@if(count($lista) == 0)
    <h3 class="text-warning">No se encontraron resultados.</h3>
@else
    <table id="example1" class="table datatable-basic">
        <thead>
        <tr>
            @foreach($cabecera as $key => $value)
                <th @if((int)$value['numero'] > 1) colspan="{{ $value['numero'] }}" @endif>{!! $value['valor'] !!}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        <?php
        $contador = $inicio + 1;
        ?>
        @foreach ($lista as $key => $value)
            <tr>
                <td>{{ $contador }}</td>
                <td>{{ $value->tabla }}</td>
                <td>{{ $value->usuario->name }}</td>
                <td>{{ $value->accion }}</td>
                <td>{{ $value->fecha }}</td>
                <td>{{ $value->ip }}</td>
                <td>{{ $value->registroid }}</td>
                <td>{{ $value->detalle }}</td>
            </tr>
            <?php
            $contador = $contador + 1;
            ?>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            @foreach($cabecera as $key => $value)
                <th @if((int)$value['numero'] > 1) colspan="{{ $value['numero'] }}" @endif>{!! $value['valor'] !!}</th>
            @endforeach
        </tr>
        </tfoot>
    </table>
    <script>
        $(document).ready(function () {
            // Basic datatable
            $('.datatable-basic').DataTable({
                pageLength: 100,
            });
        });
    </script>
@endif