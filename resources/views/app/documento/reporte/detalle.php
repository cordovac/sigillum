<?php
$pdf = new TCPDF('P', 'pt');
$pdf->AddPage();

$pdf->Ln(30);
$pdf->AddFont('helvetica');
$pdf->Cell(0, 10, 'DATOS DEL DOCUMENTO', 0, 0, 'C');
$pdf->Ln(40);

$pdf->Cell(200, 10, 'TIPO DE DOCUMENTO:');
$pdf->Cell(100, 10, $documento->tipodocumento->descripcion);
$pdf->Ln(25);

$pdf->Cell(200, 10, 'NÚMERO:');
$pdf->Cell(100, 10, $documento->numero);
$pdf->Ln(25);

$pdf->Cell(200, 10, 'EMPRESA EMISOR:');
$pdf->Cell(100, 10, $documento->empresaemisor->razonsocial);
$pdf->Ln(25);

$pdf->Cell(200, 10, 'REPRESENTANTE EMISOR:');
$pdf->Cell(100, 10, $documento->representanteemisor->apellidopaterno . ' ' . $documento->representanteemisor->apellidomaterno . ' ' . $documento->representanteemisor->nombres);
$pdf->Ln(25);

$pdf->Ln(30);

$pdf->Cell(0, 10, '(METADATA)', 0, 0, 'C');
$pdf->Ln(40);
foreach ($metadata as $key => $value) {
    $valorguardado = null;
    if (!is_null($documento_id)) {
        $valormetadata = \App\Metadatadocumento::where('metadata_id', $value->id)->where('documento_id', $documento_id)->first();
        if (!is_null($valormetadata) && !is_null($valormetadata->id)) {
            $valorguardado = $valormetadata->valor;
        }
    }

    if ($value->tipocontrol === 'text') {
        //
    } elseif ($value->tipocontrol === 'textarea') {
        //
    } elseif ($value->tipocontrol === 'radio') {
        //
    } elseif ($value->tipocontrol === 'checkbox') {
        //
    } elseif ($value->tipocontrol === 'select') {
        //
    }
    $pdf->Cell(200, 10, $value->nombre . ':');
    $pdf->Cell(100, 10, $valorguardado);
    $pdf->Ln(20);
}
$pdf->Output('documento.pdf');
