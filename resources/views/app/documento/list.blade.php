@if(count($lista) == 0)
    <h3 class="text-warning">No se encontraron resultados.</h3>
@else
    <table id="example1" class="table datatable-basic">
        <thead>
        <tr>
            @foreach($cabecera as $key => $value)
                <th @if((int)$value['numero'] > 1) colspan="{{ $value['numero'] }}" @endif>{!! $value['valor'] !!}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        <?php
        $contador = $inicio + 1;
        ?>
        @foreach ($lista as $key => $value)
            <tr>
                <td>{{ $contador }}</td>
                <td>{{ $value->tipodocumento->descripcion }}</td>
                <td>{{ $value->flujotrabajo->descripcion }}</td>
                <td>{{ $value->numero }}</td>
                <td>{!! Form::button('<i class="icon-file-eye"></i> Ver', array('onclick' => 'modal (\''.URL::route('historial.historialdocumento', array($value->id)).'\', \'Seguimiento documento: '.$value->numero.'\', this);', 'class' => 'btn btn-default btn-sm')) !!}</td>
                <td>{!! Form::button('<i class="icon-file-eye"></i> Ver', array('onclick' => 'modal (\''.URL::route('documento.show', array($value->id, 'flujotrabajo_id' => $value->flujotrabajo_id)).'\', \'Seguimiento documento: '.$value->numero.'\', this);', 'class' => 'btn btn-success btn-sm')) !!}</td>
                <td>{!! Form::button('<i class="icon-cross2"></i> Eliminar', array('onclick' => 'modal (\''.URL::route($ruta["delete"], array($value->id, 'SI')).'\', \''.$titulo_eliminar.'\', this);', 'class' => 'btn btn-danger btn-sm')) !!}</td>
            </tr>
            <?php
            $contador = $contador + 1;
            ?>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            @foreach($cabecera as $key => $value)
                <th @if((int)$value['numero'] > 1) colspan="{{ $value['numero'] }}" @endif>{!! $value['valor'] !!}</th>
            @endforeach
        </tr>
        </tfoot>
    </table>
    <script>
        $(document).ready(function () {
            // Basic datatable
            $('.datatable-basic').DataTable({
                pageLength: 100,
            });
        });
    </script>
@endif