@foreach($metadata as $key => $value)
    @php
        $entidad = 'Documento';
        $valorguardado = null;
        if (!is_null($documento_id)){
            $valormetadata = \App\Metadatadocumento::where('metadata_id', $value->id)->where('documento_id', $documento_id)->first();
            if (!is_null($valormetadata) && !is_null($valormetadata->id)) {
                $valorguardado = $valormetadata->valor;
            }
        }
    @endphp
    <div class="form-group row">
        {!! Form::label($value->codigo, $value->nombre.':', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label font-weight-semibold')) !!}
        @if($value->tipocontrol === 'text')
            @php
                $metadatareglas = $value->metadatareglas;
                $isfecha = false;
                $fecha = null;
                if (!is_null($valorguardado)){
                    $fecha = date('d/m/Y',strtotime($valorguardado));
                }
                foreach ($metadatareglas as $keydetalle => $valuedetalle){
                    if ($valuedetalle->regla->codigo === 'date_format'){
                        $isfecha = true;
                    }
                }
            @endphp
            @if($isfecha)
                <div class="col-lg-5 col-md-5 col-sm-5">
                    <div class='input-group' id='div{{ $value->codigo }}'>
                        {!! Form::text($value->codigo, $fecha, array('class' => 'form-control', 'id' => $value->codigo, 'placeholder' => 'Ingrese fecha')) !!}
                        <span class="input-group-prepend">
                            <button type="button" class="btn btn-light btn-icon" id="btn{{ $value->codigo }}"><i
                                        class="icon-calendar3"></i></button>
                        </span>
                    </div>
                </div>
                <script>
                    $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="{{ $value->codigo }}"]').pickadate({
                        format: 'dd/mm/yyyy',
                        selectMonths: true,
                        selectYears: true,
                        monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                        weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
                    });
                    $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="btn{{ $value->codigo }}"]').on('click', function (e) {
                        $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="{{ $value->codigo }}"]').focus();
                        e.stopPropagation()
                    });
                </script>
            @else
                <div class="col-lg-9 col-md-9 col-sm-9">
                    {!! Form::text($value->codigo, $valorguardado, array('class' => 'form-control', 'id' => $value->codigo, 'placeholder' => 'Ingrese '.$value->nombre)) !!}
                </div>
            @endif
        @elseif($value->tipocontrol === 'textarea')
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::textarea($value->codigo, $valorguardado, array('rows' => '3', 'cols'=> '3', 'class' => 'form-control', 'id' => $value->codigo, 'placeholder' => 'Ingrese '.$value->nombre)) !!}
            </div>
        @elseif($value->tipocontrol === 'radio')
            <div class="col-lg-9 col-md-9 col-sm-9">
                @php
                    $reglas = $value->reglas;
                    $metadatareglas = $value->metadatareglas;
                    $valores = array();
                    foreach($metadatareglas as $keymetadataregla => $valuemetadataregla){
                        if ($valuemetadataregla->regla->codigo === 'in'){
                            if (!is_null($valuemetadataregla->valor) and $valuemetadataregla->valor !== ''){
                                $valor = explode(',', $valuemetadataregla->valor);
                                for($i=0;$i<count($valor);$i++){
                                    $valores = $valores  + array($valor[$i] => $valor[$i]);
                                }
                            }
                        }
                    }
                @endphp
                @foreach($valores as $keyvalor => $valuevalor)
                    @if($keyvalor === $valorguardado)
                        {{ Form::radio($value->codigo, $keyvalor, true) }} {{ $valuevalor }}
                    @else
                        {{ Form::radio($value->codigo, $keyvalor, false) }} {{ $valuevalor }}
                    @endif
                @endforeach
            </div>
        @elseif($value->tipocontrol === 'checkbox')
            <div class="col-lg-9 col-md-9 col-sm-9">
                @php
                    $reglas = $value->reglas;
                    $metadatareglas = $value->metadatareglas;
                    $valores = array();
                    foreach($metadatareglas as $keymetadataregla => $valuemetadataregla){
                        if ($valuemetadataregla->regla->codigo === 'in'){
                            if (!is_null($valuemetadataregla->valor) and $valuemetadataregla->valor !== ''){
                                $valor = explode(',', $valuemetadataregla->valor);
                                for($i=0;$i<count($valor);$i++){
                                    $valores = $valores  + array($valor[$i] => $valor[$i]);
                                }
                            }
                        }
                    }
                @endphp
                @foreach($valores as $keyvalor => $valuevalor)
                    @php
                        $seleccionados = explode(",", $valorguardado);
                    @endphp
                    @if(in_array($keyvalor, $seleccionados))
                        {{ Form::checkbox($value->codigo.'[]', $keyvalor, true) }} {{ $valuevalor }}
                    @else
                        {{ Form::checkbox($value->codigo.'[]', $keyvalor, false) }} {{ $valuevalor }}
                    @endif
                @endforeach
            </div>
        @elseif($value->tipocontrol === 'select')
            <div class="col-lg-9 col-md-9 col-sm-9">
                @php
                    $reglas = $value->reglas;
                    $metadatareglas = $value->metadatareglas;
                    $valores = array('' => 'Seleccione');
                    foreach($metadatareglas as $keymetadataregla => $valuemetadataregla){
                        if ($valuemetadataregla->regla->codigo === 'in'){
                            if (!is_null($valuemetadataregla->valor) and $valuemetadataregla->valor !== ''){
                                $valor = explode(',', $valuemetadataregla->valor);
                                for($i=0;$i<count($valor);$i++){
                                    $valores = $valores  + array($valor[$i] => $valor[$i]);
                                }
                            }
                        }
                    }
                @endphp
                {!! Form::select($value->codigo, $valores, $valorguardado, array('class' => 'form-control form-control-select2', 'id' => $value->codigo)) !!}
            </div>
        @endif
    </div>
@endforeach