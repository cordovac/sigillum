<div id="divMensajeError{!! $entidad !!}"></div>
{!! Form::model($documento, $formData) !!}
{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}
{!! Form::hidden('documento_id', $documento_id, array('id' => 'documento_id')) !!}
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="form-group row">
            {!! Form::label('tipodocumento_id', 'Tipo:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label font-weight-bold')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::select('tipodocumento_id', $cboTipodocumento, NULL, array('class' => 'form-control form-control-select2', 'id' => 'tipodocumento_id', 'onchange' => 'generarMetadata(this)')) !!}
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="form-group row">
            {!! Form::label('numero', 'Número:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label font-weight-bold')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::text('numero', null, array('class' => 'form-control', 'id' => 'numero', 'placeholder' => 'Ingrese número')) !!}
            </div>
        </div>
    </div>
</div>
@if(!is_null($flujotrabajo_id))
    {!! Form::hidden('flujotrabajo_id', $flujotrabajo_id, array('id' => 'flujotrabajo_id')) !!}
@else
    <div class="form-group row">
        {!! Form::label('flujotrabajo_id', 'Flujo de trabajo:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label font-weight-bold')) !!}
        <div class="col-lg-9 col-md-9 col-sm-9">
            {!! Form::select('flujotrabajo_id', $cboFlujotrabajo, NULL, array('class' => 'form-control form-control-select2', 'id' => 'flujotrabajo_id')) !!}
        </div>
    </div>
@endif
<div class="form-group row">
    <label class="col-lg-3 col-md-3 col-sm-3 col-form-label font-weight-semibold">Archivos:</label>
    <div class="col-lg-9 col-md-9 col-sm-9">
        <input type="file" id="archivo" name="archivo[]" multiple>
    </div>
</div>
<h5>Datos</h5>
<div id="datosdocumento">

</div>
<div class="form-group row">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardararchivo(\''.$entidad.'\', this, null, actualizar)')) !!}
        {!! Form::button('<i class="fa fa-exclamation fa-lg"></i> Cancelar', array('class' => 'btn btn-warning btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
    $(document).ready(function () {
        configurarAnchoModal('900');
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M');
        $(IDFORMMANTENIMIENTO + '{!! $entidad !!}' + " :input[id='tipodocumento_id']").trigger("onchange");
    });

    function generarMetadata(idboton) {
        let cadena = IDFORMMANTENIMIENTO + '{!! $entidad !!}' + " :input[id='documento_id']";
        let documento = $(cadena);
        let ruta = 'documento/generarmetadata/' + idboton.value + '/' + documento.val();
        let contenedor = '#datosdocumento';
        $(contenedor).html(imgCargando());
        let respuesta = '';
        if (idboton.value === '') {
            $(contenedor).html(respuesta);
            return;
        }
        let data = sendRuta(ruta);
        data.done(function (msg) {
            respuesta = msg;
        }).fail(function (xhr, textStatus, errorThrown) {
            respuesta = 'ERROR';
        }).always(function () {
            if (respuesta === 'ERROR') {
                $(contenedor).html('<button type="button" onclick="cerrarModal(' + (contadorModal - 1) + ');" class="btn btn-warning btn-sm"><i class="fa fa-exclamation fa-lg"></i> Cancelar</button>');
            } else {
                $(contenedor).html(respuesta);
            }
        });
    }
</script>