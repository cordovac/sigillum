<h3>Revisados</h3>
@if(count($historial) == 0)
    <h3 class="text-warning">No se encontraron resultados.</h3>
@else
    <table id="tabla-historial" class="table datatable-basic" style="width:100%">
        <thead>
        <tr>
            <th>Archivos</th>
            <th>Revisado por</th>
            <th>Tarea</th>
            <th>Fecha ingreso</th>
            <th>Fecha de revisión</th>
            <th>Comentario</th>
            <th>Tiempo de respuesta</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $contador = 1;
        ?>
        @foreach ($historial as $key => $value)
            <tr>
                <td>
                    <ul>
                        @foreach($value->archivos as $keyarchivo => $valuearchivo)
                            @if($valuearchivo->accion === 'C')
                                <li>
                                    Nuevo archivo: <a class="btn btn-sm btn-default" target="_blank"
                                                      href="{{ URL::route('archivo.mostrararchivo', array($valuearchivo->id)) }}"><i
                                                class="icon-download7"></i> {{ $valuearchivo->nombre }}</a>
                                </li>
                            @elseif($valuearchivo->accion === 'U')
                                <li>
                                    Se actualizo el archivo: <a class="btn btn-sm btn-default" target="_blank"
                                                                href="{{ URL::route('archivo.mostrararchivo', array($valuearchivo->padre->id)) }}"><i
                                                class="icon-download7"></i> {{ $valuearchivo->padre->nombre }}</a>
                                    por el archivo <a class="btn btn-sm btn-default" target="_blank"
                                                      href="{{ URL::route('archivo.mostrararchivo', array($valuearchivo->id)) }}"><i
                                                class="icon-download7"></i> {{ $valuearchivo->nombre }}</a>
                                </li>
                            @elseif($valuearchivo->accion === 'M')
                                <li>
                                    Se converva el archivo: <a class="btn btn-sm btn-default" target="_blank"
                                                               href="{{ URL::route('archivo.mostrararchivo', array($valuearchivo->id)) }}"><i
                                                class="icon-download7"></i> {{ $valuearchivo->nombre }}</a>
                                </li>
                            @elseif($valuearchivo->accion === 'D')
                                <li>
                                    Se eliminó el archivo: <a class="btn btn-sm btn-default" target="_blank"
                                                              href="{{ URL::route('archivo.mostrararchivo', array($valuearchivo->id)) }}"><i
                                                class="icon-download7"></i> {{ $valuearchivo->nombre }}</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </td>
                <td>
                    <span class="badge badge-success badge-pill">{{ $value->usuario->representante->apellidopaterno.' '.$value->usuario->representante->apellidomaterno.' '.$value->usuario->representante->nombres.' - '.$value->usuario->representante->empresa->razonsocial }}</span>
                </td>
                <td>{{ $value->detalleflujotrabajo->tarea->descripcion }}</td>
                <td>{{ $value->created_at }}</td>
                <td>{{ $value->fechaoperacion }}</td>
                <td>{{ $value->observacion }}</td>
                <td>
                    @if($value->pendiente)
                        {{ '' }}
                    @else
                        @if($value->detalleflujotrabajo->secuencia === 'I' || $value->detalleflujotrabajo->secuencia === 'F')
                            {{ '' }}
                        @else
                            {{ $value->fechaoperacion->diffForHumans($value->created_at) }}
                        @endif
                    @endif
                </td>
            </tr>
            <?php
            $contador = $contador + 1;
            ?>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th>Archivos</th>
            <th>Revisado por</th>
            <th>Tarea</th>
            <th>Fecha ingreso</th>
            <th>Fecha de revisión</th>
            <th>Comentario</th>
            <th>Tiempo de respuesta</th>
        </tr>
        </tfoot>
    </table>
@endif
<h3>Pendientes</h3>
@if(count($historialpendiente) == 0)
    <h3 class="text-warning">No se encontraron resultados.</h3>
@else
    <table id="tabla-historial-pendiente" class="table datatable-basic" style="width:100%">
        <thead>
        <tr>
            <th>Archivos</th>
            <th>A revisar por (rol)</th>
            <th>Enviado por</th>
            <th>Tarea</th>
            <th>Fecha ingreso</th>
            <th>Fecha de revisión</th>
            <th>Comentario</th>
            <th>Tiempo de respuesta</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $contador = 1;
        ?>
        @foreach ($historialpendiente as $key => $value)
            @php
                $usuarioemisor = $value->usuarioemisor->representante;
                $empresaemisor = $usuarioemisor->empresa;
                $detalletipousuarios = $value->detalleflujotrabajo->detalletipousuarios;
                $contadortipousuarios = 0;
            @endphp
            <tr>
                <td>
                    <ul>
                        @foreach($value->archivos as $keyarchivo => $valuearchivo)
                            <li>{{ $valuearchivo->nombre }}</li>
                        @endforeach
                    </ul>
                </td>
                <td>
                    @foreach($detalletipousuarios as $keydetalle => $valuedetalle)
                        @if($contadortipousuarios === 0)
                            <span class="badge badge-danger badge-pill">{{ $valuedetalle->tipousuario->nombre }} - {{ $valuedetalle->tipousuario->empresa->razonsocial }}</span>
                        @else
                            <br><span
                                    class="badge badge-danger badge-pill">{{ $valuedetalle->tipousuario->nombre }} - {{ $valuedetalle->tipousuario->empresa->razonsocial }}</span>
                        @endif
                        @php
                            $contadortipousuarios++;
                        @endphp
                    @endforeach
                </td>
                <td>
                    <span class="badge badge-success badge-pill">{{ $usuarioemisor->apellidopaterno . ' ' .$usuarioemisor->apellidomaterno . ', ' . $usuarioemisor->nombres . ' - '. $empresaemisor->razonsocial }}</span>
                </td>
                <td>{{ $value->detalleflujotrabajo->tarea->descripcion }}</td>
                <td>{{ $value->created_at }}</td>
                <td>{{ $value->fechaoperacion }}</td>
                <td>{{ $value->observacion }}</td>
                <td>
                    @if($value->pendiente)
                        {{ '' }}
                    @else
                        {{ $value->fechaoperacion->diffForHumans($value->created_at) }}
                    @endif
                </td>
            </tr>
            <?php
            $contador = $contador + 1;
            ?>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th>Archivos</th>
            <th>Revisado por</th>
            <th>Tarea</th>
            <th>Fecha ingreso</th>
            <th>Fecha de revisión</th>
            <th>Comentario</th>
            <th>Tiempo de respuesta</th>
        </tr>
        </tfoot>
    </table>
@endif
<div class="form-group">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-exclamation fa-lg"></i> Cerrar', array('class' => 'btn btn-warning btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal((contadorModal - 1));')) !!}
    </div>
</div>
<script>
    $(document).ready(function () {
        // Basic datatable
        configurarAnchoModal('900');
        $('#tabla-historial').DataTable({
            pageLength: 100,
            responsive: true,
            "scrollX": true,
            "ordering": false
        });
        $('#tabla-historial-pendiente').DataTable({
            pageLength: 100,
            responsive: true,
            "scrollX": true,
            "ordering": false
        });
    });
</script>