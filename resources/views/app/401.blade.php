<div class="form-group row">
    <div class="col-lg-12 col-md-12 col-sm-12 text-center">
        <h1 class="headline text-yellow"> 404</h1>
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12 col-md-12 col-sm-12 text-danger">
        <h3><i class="fa fa-warning text-yellow"></i> Oops! Usted no tiene permisos para realizar esta acción.</h3>
        <p> Este usuario no tiene permiso para realizar este acción.Comuniquese con el administrador del sistema. </p>
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-exclamation fa-lg"></i>Cerrar', array('class' => 'btn btn-warning btn-sm', 'id' => 'btnCancelar', 'onclick' => 'cerrarModal();')) !!}
    </div>
</div>