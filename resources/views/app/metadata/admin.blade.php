<?php

use Illuminate\Support\Facades\URL;

$base = URL::to('/') . '/';
?>

<div class="card-header header-elements-inline">
    <h5 class="card-title">Búsqueda de {{ $title }}</h5>
    <div class="header-elements">
        <div class="list-icons">

        </div>
    </div>
</div>

<div class="card-body">
    {!! Form::open(array('route' => 'metadata.buscar', 'method' => 'POST' ,'onsubmit' => 'return false;', 'role' => 'form', 'autocomplete' => 'off', 'id' => 'formBusqueda'.$entidad))  !!}
    {!! Form::hidden('page', 1, array('id' => 'page')) !!}
    {!! Form::hidden('tipodocumento_id', $tipodocumento_id, array('id' => 'tipodocumento_id')) !!}
    {!! Form::hidden('filas', 40, array('id' => 'filas')) !!}
    {!! Form::hidden('accion', 'listar', array('id' => 'accion')) !!}
    <div class="text-right">
        {!! Form::button('Buscar <i class="icon-search4"></i>', array('class' => 'btn btn-success btn-sm', 'id' => 'btnBuscar', 'onclick' => 'buscar(\''.$entidad.'\')')) !!}
        {!! Form::button('Registrar <i class="icon-add"></i>', array('class' => 'btn btn-info btn-sm', 'id' => 'btnNuevo', 'onclick' => 'modal (\''.URL::route($ruta["create"], array('listar'=>'SI', 'tipodocumento_id'=> $tipodocumento_id)).'\', \''.$titulo_registrar.'\', this);')) !!}
        {!! Form::button('Cerrar <i class="icon-paperplane"></i>', array('class' => 'btn btn-danger btn-sm', 'id' => 'btnBuscar', 'onclick' => 'cerrarModal();')) !!}
    </div>
    {!! Form::close() !!}
</div>
<div id="listado{{ $entidad }}">

</div>
<script>
    $(document).ready(function () {
        init(IDFORMBUSQUEDA + '{{ $entidad }}', 'B');
        buscar('{{ $entidad }}');
        configurarAnchoModal('800');
    });
</script>
