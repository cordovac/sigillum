@if(count($lista) == 0)
    <h3 class="text-warning">No se encontraron resultados.</h3>
@else
    <table id="example1" class="table datatable-basic">
        <thead>
        <tr>
            @foreach($cabecera as $key => $value)
                <th @if((int)$value['numero'] > 1) colspan="{{ $value['numero'] }}" @endif>{!! $value['valor'] !!}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        <?php
        $contador = $inicio + 1;
        $contadorempresas = 0;
        ?>
        @foreach ($lista as $key => $value)
            <tr>
                <td>{{ $contador }}</td>
                <td>{{ $value->codigo }}</td>
                <td>{{ $value->descripcion }}</td>
                @if($superusuario)
                    <td>
                        @foreach($value->detalleempresas as $keyempresa => $valueempresa)
                            @if($contadorempresas === 0)
                                <span class="badge badge-dark badge-pill">{{ $valueempresa->empresa->razonsocial }}</span>
                            @else
                                <br><span
                                        class="badge badge-dark badge-pill">{{ $valueempresa->empresa->razonsocial }}</span>
                            @endif
                            @php
                                $contadorempresas++;
                            @endphp
                        @endforeach
                    </td>
                @endif
                @if($verdetalle)
                    <td>{!! Form::button('<i class="icon-list-numbered"></i> Detalle', array('onclick' => 'modal (\''.URL::route('detalleflujotrabajo.index', array('flujotrabajo_id'=>$value->id)).'\', \''.$value->descripcion.'\', this);', 'class' => 'btn btn-sm btn-default')) !!}</td>
                @endif
                @if($editar)
                    <td>{!! Form::button('<i class="icon-pencil4"></i> Editar', array('onclick' => 'modal (\''.URL::route($ruta["edit"], array($value->id, 'listar'=>'SI')).'\', \''.$titulo_modificar.'\', this);', 'class' => 'btn btn-warning btn-sm')) !!}</td>
                @endif
                @if($eliminar)
                    <td>{!! Form::button('<i class="icon-cross2"></i> Eliminar', array('onclick' => 'modal (\''.URL::route($ruta["delete"], array($value->id, 'SI')).'\', \''.$titulo_eliminar.'\', this);', 'class' => 'btn btn-danger btn-sm')) !!}</td>
                @endif
            </tr>
            <?php
            $contador = $contador + 1;
            ?>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            @foreach($cabecera as $key => $value)
                <th @if((int)$value['numero'] > 1) colspan="{{ $value['numero'] }}" @endif>{!! $value['valor'] !!}</th>
            @endforeach
        </tr>
        </tfoot>
    </table>
    <script>
        $(document).ready(function () {
            // Basic datatable
            $('.datatable-basic').DataTable({
                pageLength: 100,
            });

        });
    </script>
@endif