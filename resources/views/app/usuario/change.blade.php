<div id="divMensajeError{!! $entidad !!}"></div>
{!! Form::model($usuario, $formData) !!}
{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}
<div class="form-group row">
    {!! Form::label('password', 'Contraseña:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::password('password',  array('class' => 'form-control input-xs', 'id' => 'password', 'placeholder' => 'Contraseña' )) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('password', 'Repita la contraseña:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::password('password_confirmation',  array('class' => 'form-control input-xs', 'id' => 'password-confirm', 'placeholder' => 'Vuelva escribir la contraseña' )) !!}
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardar(\''.$entidad.'\', this)')) !!}
        {!! Form::button('<i class="fa fa-exclamation fa-lg"></i> Cancelar', array('class' => 'btn btn-warning btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
    $(document).ready(function () {
        let idmodal = '#modal' + (contadorModal - 1);
        $('.form-control-select2').select2({
            dropdownParent: $(idmodal)
        });
        configurarAnchoModal('620');
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M');
    });
</script>