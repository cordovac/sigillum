<div id="divMensajeError{!! $entidad !!}"></div>
{!! Form::model($usuario, $formData) !!}
{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}
@if(is_null($usuario))
    <div class="form-group row">
        {!! Form::label('representante_id', 'Persona:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
        <div class="col-lg-9 col-md-9 col-sm-9">
            {!! Form::text('representante', isset($usuario->representante_id) ? $usuario->representante->apellidopaterno." ".$usuario->representante->apellidomaterno." ".$usuario->representante->nombres :null, array('class' => 'form-control input-xs', 'id' => 'representante', 'placeholder' => 'Apellidos o Nombre' , 'maxlength' => '11')) !!}
            {!! Form::hidden('representante_id', null, array('id' => 'representante_id')) !!}
        </div>
    </div>
@endif
<div class="form-group row">
    {!! Form::label('email', 'Email:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::email('email', null, array('class' => 'form-control input-xs', 'id' => 'email', 'placeholder' => 'Ingrese Email' )) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('tipousuario_id', 'Tipo de usuario:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::select('tipousuario_id', $cboTipousuario, $tipousuario_id, array('class' => 'form-control form-control-select2', 'id' => 'tipousuario_id')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('password', 'Contraseña:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::password('password',  array('class' => 'form-control input-xs', 'id' => 'password', 'placeholder' => 'Contraseña' , 'maxlength' => '11')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('password', 'Repita la contraseña:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::password('password_confirmation',  array('class' => 'form-control input-xs', 'id' => 'password-confirm', 'placeholder' => 'Vuelva escribir la contraseña' , 'maxlength' => '11')) !!}
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardar(\''.$entidad.'\', this)')) !!}
        {!! Form::button('<i class="glyphicon glyphicon-remove"></i> Cancelar', array('class' => 'btn btn-danger btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
    $(document).ready(function () {
        $(".chosen-select").chosen({width: "100%"});
        configurarAnchoModal('650');
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M');

        /* Autocompletar Cliente -- Inicio */
        $('#representante').change(function (e) {
            if ($(this).val() === '') {
                $('#representante_id').val('');
            }
        });
        let representante = new Bloodhound({
            datumTokenizer: function (d) {
                return Bloodhound.tokenizers.whitespace(d.value);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: 'usuario/autocompletarrepresentante/%QUERY',
                filter: function (representante) {
                    return $.map(representante, function (movie) {
                        return {
                            value: movie.value,
                            id: movie.id
                        };
                    });
                }
            }
        });
        representante.initialize();
        $('#representante').typeahead(null, {
            displayKey: 'value',
            source: representante.ttAdapter()
        }).on('typeahead:selected', function (object, datum) {
            $('#representante_id').val(datum.id);
        });

        /* Autocompletar Cliente -- Fin */
    });

</script>

