<div id="divMensajeError{!! $entidad !!}"></div>
{!! Form::model($modelo, $formData) !!}
{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}
@if(!is_null($mensaje))
    {!! $mensaje !!}
@else
    {!! '<blockquote><p class="text-danger">¿Esta seguro de eliminar el registro?</p></blockquote>' !!}
@endif
<div class="form-group">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'confirmarEliminar(\''.$entidad.'\', this)')) !!}
        {!! Form::button('<i class="fa fa-exclamation fa-lg"></i> Cancelar', array('class' => 'btn btn-warning btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal((contadorModal - 1));')) !!}
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
    $(document).ready(function () {
        init(IDFORMMANTENIMIENTO + '{{ $entidad }}', 'M');
        configurarAnchoModal('420');
    });
</script>