<div id="divMensajeError{!! $entidad !!}"></div>
{!! Form::model($respuestadetalleflujo, $formData) !!}
{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}
{!! Form::hidden('detalleflujotrabajo_id', $detalleflujotrabajo_id, array('id' => 'detalleflujotrabajo_id')) !!}
<div class="form-group row">
    {!! Form::label('respuesta_id', 'Respuesta:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::select('respuesta_id', $cboRespuesta, NULL, array('class' => 'form-control select', 'id' => 'respuesta_id')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('descripcion', 'Descripción:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('descripcion', null, array('class' => 'form-control', 'id' => 'descripcion', 'placeholder' => 'Ingrese descripcion')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('secuencia', 'Ir a tarea:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::select('secuencia', $cboDetalleflujotrabajo, NULL, array('class' => 'form-control select', 'id' => 'secuencia')) !!}
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardar(\''.$entidad.'\', this)')) !!}
        {!! Form::button('<i class="fa fa-exclamation fa-lg"></i> Cancelar', array('class' => 'btn btn-warning btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
    $(document).ready(function () {
        let idmodal = '#modal' + (contadorModal - 1);
        $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="respuesta_id"]').select2({
            dropdownParent: $(idmodal)
        });
        configurarAnchoModal('620');
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M');
    });
</script>