<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Administración</span> -
                Seguimiento de flujo de trabajo</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>

<?php
use Illuminate\Support\Facades\URL;
$base = URL::to('/') . '/';
?>

<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{ $title }}</h5>
            <div class="header-elements">
                <div class="list-icons">

                </div>
            </div>
        </div>

        <div class="card-body">
            {!! Form::open(array('route' => 'seguimiento.buscar', 'method' => 'POST' ,'onsubmit' => 'return false;', 'role' => 'form', 'autocomplete' => 'off', 'id' => 'formBusqueda'.$entidad))  !!}
            {!! Form::hidden('flujotrabajo_id', $flujotrabajo_id, array('id' => 'flujotrabajo_id')) !!}
            {!! Form::hidden('page', 1, array('id' => 'page')) !!}
            <div class="text-left">
                {!! Form::button('Actualizar <i class="icon-sync"></i>', array('class' => 'btn btn-success btn-sm', 'id' => 'btnBuscar', 'onclick' => 'actualizar()')) !!}
                {!! Form::button('Emitir <i class="icon-add"></i>', array('class' => 'btn btn-info btn-sm', 'id' => 'btnNuevo', 'onclick' => 'modal (\''.URL::route('documento.create', array('flujotrabajo_id' => $flujotrabajo_id)).'\', \''.$titulo_registrar.'\', this);')) !!}
            </div>
            {!! Form::close() !!}
        </div>
        <div id="listado{{ $entidad }}">

        </div>
    </div>
    <div class="card-group-control card-group-control-left" id="accordion-control">

    </div>
</div>

<script>
    $(document).ready(function () {
        init(IDFORMBUSQUEDA + '{{ $entidad }}', 'B');
        actualizar();
    });
    function actualizar() {
        buscar('{{ $entidad }}');
        cargarRuta('{{ URL::route('seguimiento.buscartareas', array($flujotrabajo_id)) }}', 'accordion-control')
    }
</script>
