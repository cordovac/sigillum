<div id="example-basic">
    @foreach($detalleflujotrabajo as $key => $value)
        <h3>{{ $value->tarea->descripcion }} <br>
            {{ \App\Documento::cantidaddocumentosemisor($value->id) }}</h3>
        <section>
        </section>
    @endforeach
</div>

<script>
    $("#example-basic").steps({
        headerTag: "h3",
        bodyTag: "section",
        enableAllSteps: true,
        enablePagination: false
    });
</script>