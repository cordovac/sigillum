@php
    $contador = 0;
    $clasedescripcion = '';
    $clasetitulo = '';
@endphp

@foreach($detalleflujotrabajo as $key => $value)
    @php
        $contenedor = 'listado-tarea-'.$contador;
        if ($contador ===  0){
            $clasetitulo = '';
            $clasedescripcion = ' show';
        }
        else{
            $clasetitulo = 'collapsed ';
            $clasedescripcion = '';
        }
    @endphp
    <div class="card">
        <div class="card-header">
            <h6 class="card-title">
                <a class="{{ $clasetitulo }}text-default" data-toggle="collapse"
                   href="#accordion-control-group{{ $contador }}">{{ $value->tarea->descripcion }}</a>
            </h6>
        </div>

        <div id="accordion-control-group{{ $contador }}" class="collapse{{ $clasedescripcion }}" data-parent="#accordion-control">
            <div class="card-body" id="{{ $contenedor }}">
                <script>
                    cargarRuta('{{ URL::route("seguimiento.documentosportareaemisor", array($value->id)) }}', '{{ $contenedor }}')
                </script>
            </div>
        </div>
    </div>

    @php
        $contador = $contador + 1;
    @endphp
@endforeach
