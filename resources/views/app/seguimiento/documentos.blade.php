<?php
use App\Historial;
?>
@if(count($lista) == 0)
    <h3 class="text-warning">No se encontraron resultados.</h3>
@else
    <table id="example{{ $tarea->id }}" class="table datatable-basic table-hover">
        <thead>
        <tr>
            @foreach($cabecera as $key => $value)
                <th @if((int)$value['numero'] > 1) colspan="{{ $value['numero'] }}" @endif>{!! $value['valor'] !!}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        <?php
        $contador = 1;
        ?>
        @foreach ($lista as $key => $value)
            @php
                $historial = \App\Historial::find($value->historial_id);
                $usuarioemisor = $historial->usuarioemisor->representante;
                $empresaemisor = $usuarioemisor->empresa;
                $detalletipousuarios = $historial->detalleflujotrabajo->detalletipousuarios;
                $contadortipousuarios = 0;
            @endphp
            <tr>
                <td>{{ $contador }}</td>
                <td>{{ $value->tipodocumento->descripcion }}</td>
                <td>{{ $value->numero }}</td>
                <td>
                    <span class="badge badge-success badge-pill">{{ $usuarioemisor->apellidopaterno . ' ' .$usuarioemisor->apellidomaterno . ', ' . $usuarioemisor->nombres . ' - '. $empresaemisor->razonsocial }}</span>
                <td>
                    @foreach($detalletipousuarios as $keydetalle => $valuedetalle)
                        @if($contadortipousuarios === 0)
                            <span class="badge badge-danger badge-pill">{{ $valuedetalle->tipousuario->nombre }} - {{ $valuedetalle->tipousuario->empresa->razonsocial }}</span>
                        @else
                            <br><span
                                    class="badge badge-danger badge-pill">{{ $valuedetalle->tipousuario->nombre }} - {{ $valuedetalle->tipousuario->empresa->razonsocial }}</span>
                        @endif
                        @php
                            $contadortipousuarios++;
                        @endphp
                    @endforeach
                </td>
                @if(!is_null($historial->historial_id))
                    <td>{{ $historial->padre->observacion }}</td>
                @else
                    <td>{{ '' }}</td>
                @endif
                <td>
                    <a href="{{ URL::route('reporte.documentopdf', array($value->id)) }}" target="_blank"
                       class="btn btn-info btn-sm"><i class="icon-file-pdf"></i> Ver</a>
                </td>
                <td>{!! Form::button('<i class="icon-file-eye"></i> Historial', array('onclick' => 'modal (\''.URL::route('historial.historialdocumento', array($value->id)).'\', \'Seguimiento documento: '.$value->numero.'\', this);', 'class' => 'btn btn-default btn-sm')) !!}</td>
                <td>
                    {!! Form::button('<i class="icon-bubbles2"></i> Respuesta', array('onclick' => 'modal (\''.URL::route('historial.edit', array($value->historial_id)).'\', \'Registrar respuesta\', this);', 'class' => 'btn btn-warning btn-sm')) !!}
                </td>
            </tr>
            <?php
            $contador = $contador + 1;
            ?>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            @foreach($cabecera as $key => $value)
                <th @if((int)$value['numero'] > 1) colspan="{{ $value['numero'] }}" @endif>{!! $value['valor'] !!}</th>
            @endforeach
        </tr>
        </tfoot>
    </table>
    <script>
        $(document).ready(function () {
            // Basic datatable
            $('#example{{ $tarea->id }}').DataTable({
                pageLength: 100,
                "searching": false
            });

        });
    </script>
@endif