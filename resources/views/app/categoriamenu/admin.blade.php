<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Administración</span> -
                Categoria de menu</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>

<?php
use Illuminate\Support\Facades\URL;
$base = URL::to('/') . '/';
?>

<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Búsqueda de categoria de menu</h5>
            <div class="header-elements">
                <div class="list-icons">

                </div>
            </div>
        </div>

        <div class="card-body">
            {!! Form::open(array('route' => 'categoriamenu.buscar', 'method' => 'POST' ,'onsubmit' => 'return false;', 'role' => 'form', 'autocomplete' => 'off', 'id' => 'formBusqueda'.$entidad))  !!}
            {!! Form::hidden('page', 1, array('id' => 'page')) !!}
            {!! Form::hidden('filas', 40, array('id' => 'filas')) !!}
            {!! Form::hidden('accion', 'listar', array('id' => 'accion')) !!}
            <div class="text-right">
                {!! Form::button('Buscar <i class="icon-search4"></i>', array('class' => 'btn btn-success btn-sm', 'id' => 'btnBuscar', 'onclick' => 'buscar(\''.$entidad.'\')')) !!}
                {!! Form::button('Registrar <i class="icon-add"></i>', array('class' => 'btn btn-info btn-sm', 'id' => 'btnNuevo', 'onclick' => 'modal (\''.URL::route($ruta["create"], array('listar'=>'SI')).'\', \''.$titulo_registrar.'\', this);')) !!}
            </div>
            {!! Form::close() !!}
        </div>
        <div id="listado{{ $entidad }}">

        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        init(IDFORMBUSQUEDA + '{{ $entidad }}', 'B');
        buscar('{{ $entidad }}');
    });
</script>
