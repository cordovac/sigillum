<div id="divMensajeError{!! $entidad !!}"></div>
{!! Form::model($categoriamenu, $formData) !!}
{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}
<div class="form-group row">
    {!! Form::label('categoriamenu_id', 'Categora menu:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::select('categoriamenu_id', $cboCategoriamenu, NULL, array('class' => 'form-control form-control-select2', 'id' => 'categoriamenu_id')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('nombre', 'Nombre:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('nombre', null, array('class' => 'form-control', 'id' => 'nombre', 'placeholder' => 'Ingrese nombre')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('orden', 'Orden:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('orden', null, array('class' => 'form-control', 'id' => 'orden', 'placeholder' => 'Ingrese orden')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('icono', 'Icono:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('icono', null, array('class' => 'form-control', 'id' => 'icono', 'placeholder' => 'Ingrese icono')) !!}
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardar(\''.$entidad.'\', this)')) !!}
        {!! Form::button('<i class="fa fa-exclamation fa-lg"></i> Cancelar', array('class' => 'btn btn-warning btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
    $(document).ready(function () {
        configurarAnchoModal('620');
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M');
    });
</script>