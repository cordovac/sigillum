<div id="divMensajeError{!! $entidad !!}"></div>
{!! Form::model($permiso, $formData) !!}
{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}
{!! Form::hidden('opcionmenu_id', $opcionmenu_id, array('id' => 'opcionmenu_id')) !!}
<div class="form-group row">
    {!! Form::label('nombre', 'Nombre:', array('class' => 'col-lg-5 col-md-5 col-sm-5 col-form-label')) !!}
    <div class="col-lg-7 col-md-7 col-sm-7">
        {!! Form::text('nombre', null, array('class' => 'form-control', 'id' => 'nombre', 'placeholder' => 'Ingrese descripción')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('codigo', 'Codigo:', array('class' => 'col-lg-5 col-md-5 col-sm-5 col-form-label')) !!}
    <div class="col-lg-7 col-md-7 col-sm-7">
        {!! Form::text('codigo', null, array('class' => 'form-control', 'id' => 'codigo', 'placeholder' => 'Ingrese código')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('superusuario', '¿Exclusivo de superusuario?:', array('class' => 'col-lg-5 col-md-5 col-sm-5 col-form-label')) !!}
    <div class="col-lg-7 col-md-7 col-sm-7">
        {!! Form::checkbox('superusuario', 1, null, array('class' => 'form-check-input-styled')) !!}
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardar(\''.$entidad.'\', this)')) !!}
        {!! Form::button('<i class="fa fa-exclamation fa-lg"></i> Cancelar', array('class' => 'btn btn-warning btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
    $(document).ready(function () {
        configurarAnchoModal('620');
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M');
        $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="superusuario"]').uniform();
    });
</script>