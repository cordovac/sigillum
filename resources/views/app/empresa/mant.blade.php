@php
    $fechaalta = null;
    $fechabaja = null;
    if ($empresa !== NULL) {
        if (!is_null($empresa->fechaalta)){
            $fechaalta = date('d/m/Y',strtotime($empresa->fechaalta));
        }
        if (!is_null($empresa->fechabaja)){
            $fechabaja = date('d/m/Y',strtotime($empresa->fechabaja));
        }
    }
@endphp
<div id="divMensajeError{!! $entidad !!}"></div>
{!! Form::model($empresa, $formData) !!}
{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}
<div class="form-group row">
    {!! Form::label('ruc', 'RUC:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('ruc', null, array('class' => 'form-control', 'id' => 'ruc', 'placeholder' => 'Ingrese ruc')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('razonsocial', 'Razón social:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('razonsocial', null, array('class' => 'form-control', 'id' => 'razonsocial', 'placeholder' => 'Ingrese razonsocial')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('direccionfiscal', 'Dirección fiscal:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('direccionfiscal', null, array('class' => 'form-control', 'id' => 'direccionfiscal', 'placeholder' => 'Ingrese direccionfiscal')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('fechaalta', 'Fecha de alta:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-5 col-md-5 col-sm-5">
        <div class='input-group' id='divfechaalta'>
            {!! Form::text('fechaalta', $fechaalta, array('class' => 'form-control', 'id' => 'fechaalta', 'placeholder' => 'Ingrese fecha')) !!}
            <span class="input-group-prepend">
                <button type="button" class="btn btn-light btn-icon" id="btnFechaalta"><i
                            class="icon-calendar3"></i></button>
            </span>
        </div>
    </div>
</div>
<div class="form-group row">
    {!! Form::label('fechabaja', 'Fecha de baja:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-5 col-md-5 col-sm-5">
        <div class='input-group' id='divfechabaja'>
            {!! Form::text('fechabaja', $fechabaja, array('class' => 'form-control', 'id' => 'fechabaja', 'placeholder' => 'Ingrese fecha')) !!}
            <span class="input-group-prepend">
                <button type="button" class="btn btn-light btn-icon" id="btnFechabaja"><i
                            class="icon-calendar3"></i></button>
            </span>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardar(\''.$entidad.'\', this)')) !!}
        {!! Form::button('<i class="fa fa-exclamation fa-lg"></i> Cancelar', array('class' => 'btn btn-warning btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
    $(document).ready(function () {
        configurarAnchoModal('620');
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M');
        $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="fechaalta"]').inputmask("dd/mm/yyyy");
        $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="fechaalta"]').pickadate({
            format: 'dd/mm/yyyy',
        });
        $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="fechabaja"]').pickadate({
            format: 'dd/mm/yyyy',
        });
        $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="btnFechabaja"]').on('click', function (e) {
            $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="fechabaja"]').focus();
            e.stopPropagation()
        });
        $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="btnFechaalta"]').on('click', function (e) {
            $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="fechaalta"]').focus();
            e.stopPropagation()
        });
    });
</script>