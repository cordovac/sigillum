<div id="divMensajeError{!! $entidad !!}"></div>
{!! Form::model($representante, $formData) !!}
{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}
<div class="form-group row">
    {!! Form::label('nombres', 'Nombres:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('nombres', null, array('class' => 'form-control', 'id' => 'nombres', 'placeholder' => 'Ingrese nombres')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('apellidopaterno', 'Apellido paterno:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('apellidopaterno', null, array('class' => 'form-control', 'id' => 'apellidopaterno', 'placeholder' => 'Ingrese apellidopaterno')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('apellidomaterno', 'Apellido materno:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('apellidomaterno', null, array('class' => 'form-control', 'id' => 'apellidomaterno', 'placeholder' => 'Ingrese apellidomaterno')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('dni', 'DNI:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('dni', null, array('class' => 'form-control', 'id' => 'dni', 'placeholder' => 'Ingrese dni')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('rol', 'Rol:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('rol', null, array('class' => 'form-control', 'id' => 'rol', 'placeholder' => 'Ingrese rol')) !!}
    </div>
</div>
<h5>
    <p class="text-warning">Generar Certificado PEM</p>
</h5>
<div class="form-group row">
    {!! Form::label('password', 'Contraseña:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('password', null, array('class' => 'form-control', 'id' => 'password', 'placeholder' => 'Ingrese contraseña del certificado')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('certificado', 'Certificado:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        <input class="form-control" type="file" id="certificado" name="certificado">
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardararchivo(\''.$entidad.'\', this)')) !!}
        {!! Form::button('<i class="fa fa-exclamation fa-lg"></i> Cancelar', array('class' => 'btn btn-warning btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
    $(document).ready(function () {
        configurarAnchoModal('620');
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M');
    });
</script>