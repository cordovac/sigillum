@if(count($lista) == 0)
    <h3 class="text-warning">No se encontraron resultados.</h3>
@else
    <table id="datatable-basic-detalle" class="table datatable-basic" style="width:100%">
        <thead>
        <tr>
            @foreach($cabecera as $key => $value)
                <th @if((int)$value['numero'] > 1) colspan="{{ $value['numero'] }}" @endif>{!! $value['valor'] !!}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        <?php
        $contador = $inicio + 1;
        $contadortipousuarios = 0;
        ?>
        @foreach ($lista as $key => $value)
            <tr>
                <td>{{ $value->secuencia }}</td>
                <td>{{ $value->tarea->descripcion }}</td>
                <td>
                    @if($value->editararchivos)
                        {{ 'SI' }}
                    @else
                        {{ 'NO' }}
                    @endif
                </td>
                <td>
                    @foreach($value->detalletipousuarios as $keytipousuario => $valuetipousuario)
                        @if($contadortipousuarios === 0)
                            <span class="badge badge-dark badge-pill">{{ $valuetipousuario->tipousuario->empresa->razonsocial }} - {{ $valuetipousuario->tipousuario->nombre }}</span>
                        @else
                            <br><span class="badge badge-dark badge-pill">{{ $valuetipousuario->tipousuario->empresa->razonsocial }} - {{ $valuetipousuario->tipousuario->nombre }}</span>
                        @endif
                        @php
                            $contadortipousuarios++;
                        @endphp
                    @endforeach
                </td>
                <td>{!! Form::button('<i class="icon-bubbles4"></i> Respuestas', array('onclick' => 'modal (\''.URL::route('respuestadetalleflujo.index', array('detalleflujotrabajo_id'=>$value->id)).'\', \''.$value->tarea->descripcion.'\', this);', 'class' => 'btn btn-sm btn-default')) !!}</td>
                <td>{!! Form::button('<i class="icon-pencil4"></i> Editar', array('onclick' => 'modal (\''.URL::route($ruta["edit"], array($value->id, 'listar'=>'SI')).'\', \''.$titulo_modificar.'\', this);', 'class' => 'btn btn-warning btn-sm')) !!}</td>
                <td>{!! Form::button('<i class="icon-cross2"></i> Eliminar', array('onclick' => 'modal (\''.URL::route($ruta["delete"], array($value->id, 'SI')).'\', \''.$titulo_eliminar.'\', this);', 'class' => 'btn btn-danger btn-sm')) !!}</td>
            </tr>
            <?php
            $contador = $contador + 1;
            ?>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            @foreach($cabecera as $key => $value)
                <th @if((int)$value['numero'] > 1) colspan="{{ $value['numero'] }}" @endif>{!! $value['valor'] !!}</th>
            @endforeach
        </tr>
        </tfoot>
    </table>
    <script>
        $(document).ready(function () {
            // Basic datatable
            $('#datatable-basic-detalle').DataTable({
                pageLength: 100,
                responsive: true,
                "scrollX": true,
                "searching": false
            });
        });
    </script>
@endif