<div id="divMensajeError{!! $entidad !!}"></div>
{!! Form::model($detalleflujotrabajo, $formData) !!}
{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}
{!! Form::hidden('flujotrabajo_id', $flujotrabajo_id, array('id' => 'flujotrabajo_id')) !!}
<div class="form-group row">
    {!! Form::label('tarea_id', 'Tarea:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::select('tarea_id', $cboTarea, NULL, array('class' => 'form-control select', 'id' => 'tarea_id')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('secuencia', 'Secuencia:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('secuencia', null, array('class' => 'form-control', 'id' => 'secuencia', 'placeholder' => 'Ingrese secuencia')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('editararchivos', '¿Modificar archivos?:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::checkbox('editararchivos', 1, null, array('class' => 'form-check-input-styled')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('tipousuario_id', 'Tipos de usuario', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::select('tipousuario_id[]', $cboTipousuario, !is_null($detalleflujotrabajo) ? $detalleflujotrabajo->tipousuarios->pluck('id')->toArray() : null,
        array(
        'data-placeholder' => 'Seleccionar tipos de usuario',
        'multiple' => 'multiple',
        'class' => 'form-control form-control-select2',
        'id' => 'tipousuario_id')) !!}
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardar(\''.$entidad.'\', this)')) !!}
        {!! Form::button('<i class="fa fa-exclamation fa-lg"></i> Cancelar', array('class' => 'btn btn-warning btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
    $(document).ready(function () {
        let idmodal = '#modal' + (contadorModal - 1);
        $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="tarea_id"]').select2({
            dropdownParent: $(idmodal)
        });
        $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="tipousuario_id"]').select2({
            dropdownParent: $(idmodal)
        });
        $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="editararchivos"]').uniform();
        configurarAnchoModal('620');
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M');
    });
</script>