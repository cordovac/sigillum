@php
    $archivos = \Illuminate\Support\Facades\Session::get('archivos')
@endphp

@if(count($archivos) == 0)
    <h3 class="text-warning">No se encontraron archivos.</h3>
@else
    <table id="tabla-archivos-historial" class="table datatable-basic" style="width:100%">
        <thead>
        <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Descargar</th>
            <th>Editar</th>
            <th>Nuevo archivo</th>
            <th>Eliminar</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $contador = 1;
        ?>
        @for ($i=0;$i<count($archivos);$i++)
            <tr>
                <td>{{ $contador }}</td>
                <td>{{ $archivos[$i]['nombre'] }}</td>
                <td><a class="btn btn-sm btn-default" target="_blank"
                       href="{{ URL::route('archivo.mostrararchivo', array($archivos[$i]['id'])) }}"><i
                                class="icon-download7"></i></a>
                </td>
                <td>
                    {!! Form::checkbox('editar-'.$archivos[$i]['id'], 1, null, array('id' => 'editar-'.$archivos[$i]['id'],'class' => 'form-check-input-styled')) !!}
                </td>
                <td>
                    <input type="file" id="archivo-{{ $archivos[$i]['id'] }}" name="archivo-{{ $archivos[$i]['id'] }}">
                </td>
                <td>
                    {!! Form::checkbox('eliminar-'.$archivos[$i]['id'], 1, null, array('id' => 'editar-'.$archivos[$i]['id'], 'class' => 'form-check-input-styled')) !!}
                </td>

            </tr>
            <?php
            $contador = $contador + 1;
            ?>
        @endfor
        </tbody>
        <tfoot>
        <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Nombre</th>
            <th>Editar</th>
            <th>Nuevo archivo</th>
            <th>Eliminar</th>
        </tr>
        </tfoot>
    </table>
@endif
<script type="text/javascript">
    $(document).ready(function () {
        $('#tabla-archivos-historial').DataTable({
            responsive: true,
            pageLength: 10,
            "searching": false,
            "scrollX": true
        });
    });
</script>