<div id="divMensajeError{!! $entidad !!}"></div>
{!! Form::model($historial, $formData) !!}
<div class="form-group row">
    {!! Form::label('respuestadetalleflujo_id', 'Respuesta:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::select('respuestadetalleflujo_id', $cboRespuestadetalleflujo, NULL, array('class' => 'form-control form-control-select2', 'id' => 'respuestadetalleflujo_id')) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('observacion', 'Observación:', array('class' => 'col-lg-3 col-md-3 col-sm-3 col-form-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::textarea('observacion', null, array('rows' => '3', 'cols'=> '3', 'class' => 'form-control', 'id' => 'observacion', 'placeholder' => 'Ingrese observación')) !!}
    </div>
</div>
@if($editararchivos)
    <h4>Archivos</h4>
    <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-sm-3 col-form-label font-weight-semibold">Nuevos archivos:</label>
        <div class="col-lg-9 col-md-9 col-sm-9">
            <input type="file" id="archivo" name="archivo[]" multiple>
        </div>
    </div>
@endif

<div id="listadoarchivos">
    @if(count($archivos) == 0)
        <h3 class="text-warning">No se encontraron archivos.</h3>
    @else
        <table id="tabla-archivos-historial" class="table datatable-basic" style="width:100%">
            <thead>
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Descargar</th>
                <th>Firmar</th>
                @if($editararchivos)
                    <th>Editar</th>
                    <th>Nuevo archivo</th>
                    <th>Eliminar</th>
                @endif
            </tr>
            </thead>
            <tbody>
            <?php
            $contador = 1;
            ?>
            @for ($i=0;$i<count($archivos);$i++)
                <tr>
                    <td>{{ $contador }}</td>
                    <td>{{ $archivos[$i]['nombre'] }}</td>
                    <td><a class="btn btn-sm btn-default" target="_blank"
                           href="{{ URL::route('archivo.mostrararchivo', array($archivos[$i]['id'])) }}"><i
                                    class="icon-download7"></i></a>
                    </td>
                    <td>
                        {!! Form::checkbox('firmar-'.$archivos[$i]['id'], 1, null, array('id' => 'firmar-'.$archivos[$i]['id'],'class' => 'form-check-input-styled')) !!}
                    </td>
                    @if($editararchivos)
                        <td>
                            {!! Form::checkbox('editar-'.$archivos[$i]['id'], 1, null, array('id' => 'editar-'.$archivos[$i]['id'],'class' => 'form-check-input-styled')) !!}
                        </td>
                        <td>
                            <input type="file" id="archivo-{{ $archivos[$i]['id'] }}"
                                   name="archivo-{{ $archivos[$i]['id'] }}">
                        </td>
                        <td>
                            {!! Form::checkbox('eliminar-'.$archivos[$i]['id'], 1, null, array('id' => 'editar-'.$archivos[$i]['id'], 'class' => 'form-check-input-styled')) !!}
                        </td>
                    @endif
                </tr>
                <?php
                $contador = $contador + 1;
                ?>
            @endfor
            </tbody>
            <tfoot>
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Descargar</th>
                <th>Firmar</th>
                @if($editararchivos)
                    <th>Editar</th>
                    <th>Nuevo archivo</th>
                    <th>Eliminar</th>
                @endif
            </tr>
            </tfoot>
        </table>
    @endif
</div>

<div class="form-group row">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardararchivo(\''.$entidad.'\', this, null, actualizar)')) !!}
        {!! Form::button('<i class="fa fa-exclamation fa-lg"></i> Cancelar', array('class' => 'btn btn-warning btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
    $(document).ready(function () {
        configurarAnchoModal('800');
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M');
        $('#tabla-archivos-historial').DataTable({
            responsive: true,
            pageLength: 10,
            "searching": false,
            "scrollX": true
        });
        // cargararchivos();
    });

    /**
     * Function para cargar archivos a partir de session
     */
    function cargararchivos() {
        cargarRuta('{{ URL::route('historial.verarchivos') }}', 'listadoarchivos')
    }
</script>