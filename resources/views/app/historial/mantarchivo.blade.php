<div id="divMensajeError{!! $entidad !!}"></div>
{!! Form::model($archivo, $formData) !!}
{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}

<div class="form-group row">
    <label class="col-lg-3 col-md-3 col-sm-3 col-form-label font-weight-semibold">Archivos:</label>
    <div class="col-lg-9 col-md-9 col-sm-9">
        <input type="file" id="archivo" name="archivo">
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardararchivo(\''.$entidad.'\', this, null, cargararchivos)')) !!}
        {!! Form::button('<i class="fa fa-exclamation fa-lg"></i> Cancelar', array('class' => 'btn btn-warning btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
    $(document).ready(function () {
        configurarAnchoModal('620');
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M');
    });
</script>