<blockquote><p class="text-danger">{{ $mensaje }}</p></blockquote>
<button class="btn btn-warning btn-sm" id="btnCerrarexiste"><i class="fa fa-times fa-lg"></i> Cerrar</button>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btnCerrarexiste').attr('onclick', 'cerrarModal(' + (contadorModal - 1) + ');').unbind('click');
    });
</script>