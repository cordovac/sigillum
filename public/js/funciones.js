/**
 * Funciones generales que serán usadas por la mayoria de las operaciones
 */

let IDFORMBUSQUEDA = "#formBusqueda";
let IDFORMMANTENIMIENTO = "#formMantenimiento";
let modales = new Array();
let IMAGENLOADING = "";
let contadorModal = 0;

/**
 * Permite hacer un submit a un formulario y retorna la respuesta del envio, o el error que se produzca
 * @param  {string} idformulario
 * @return {string} respuesta
 */
function submitForm(idformulario) {
    let parametros = $(idformulario).serialize();
    let accion = $(idformulario).attr('action').toLowerCase();
    let metodo = $(idformulario).attr('method').toLowerCase();
    return $.ajax({
        url: accion,
        type: metodo,
        data: parametros
    });
}

/**
 * @param idformulario
 * @returns {jQuery}
 */
function submitFormFile(idformulario) {
    let accion = $(idformulario).attr('action').toLowerCase();
    let metodo = $(idformulario).attr('method').toLowerCase();
    let parametros = new FormData($(idformulario)[0]);
    return $.ajax({
        dataType: 'text',
        url: accion,
        type: metodo,
        data: parametros,
        contentType: false,
        cache: false,
        processData: false,
    });
}

/**
 * Funcion para cargar la respuesta de la funcion 'submitForm', e incrustar en contenedor
 * @param idformulario
 * @param idContenedor
 * @param entidad
 */
function cargarSubmitForm(idformulario, idContenedor, entidad = null) {
    let contenedor = '#' + idContenedor;
    $(contenedor).html(imgCargando());
    let respuesta;
    let data = submitForm(idformulario);
    data.done(function (msg) {
        respuesta = msg;
    }).fail(function (xhr, textStatus, errorThrown) {
        respuesta = 'ERROR';
    }).always(function () {
        if (respuesta === 'ERROR') {
            $(contenedor).html('<button type="button" onclick="cerrarModal(' + (contadorModal - 1) + ');" class="btn btn-warning btn-sm"><i class="fa fa-exclamation fa-lg"></i> Cancelar</button>');
        } else {
            if (respuesta.substring(0, 5) === 'ERROR') {
                let errores = respuesta.substring(5);
                mostrarErrores(errores, idformulario, entidad)
            } else {
                $(contenedor).html(respuesta);
            }
        }
    });
}

/**
 * Enviar un direccion
 * @param  {string} ruta
 * @return {response}
 */
function sendRuta(ruta) {
    return $.ajax({
        url: ruta,
        type: 'GET'
    });
}

/**
 * Incrustar respuesta de una ruta en un contenedor
 * @param  {string} ruta
 * @param  {string} idContenedor
 * @return {html}
 */
function cargarRuta(ruta, idContenedor) {
    let contenedor = '#' + idContenedor;
    $(contenedor).html(imgCargando());
    let respuesta = '';
    let data = sendRuta(ruta);
    data.done(function (msg) {
        respuesta = msg;
    }).fail(function (xhr, textStatus, errorThrown) {
        respuesta = 'ERROR';
    }).always(function () {
        if (respuesta === 'ERROR') {
            $(contenedor).html('<button type="button" onclick="cerrarModal(' + (contadorModal - 1) + ');" class="btn btn-warning btn-sm"><i class="fa fa-exclamation fa-lg"></i> Cancelar</button>');
        } else {
            $(contenedor).html(respuesta);
        }
    });
}

/**
 * Dar estilo a un error
 * @param  {string} error
 * @return {html}
 */
function estiloError(error) {
    return '<div class="bs-docs-section"><div class="row"><div class="page-header"><h2 id="forms">' + error + '</h2></div></div></div>';
}

/**
 * Función para realizar busqueda e inscrustar el RESPONSE en un div, los parametros se envian por medio de
 * $.POST de jquery a una URL
 * @param  {string} entidad     Nombre de la entidad donde se va a inscrustar. Ejm: 'Usuario', 'Area', ect.
 * @param idcontenedor
 * @return {html}               Se incrusta el codigo HTML en el DIV
 */
function buscar(entidad, idcontenedor = null) {
    let formulario = IDFORMBUSQUEDA + entidad;
    let contenedor = 'listado' + entidad;
    if (idcontenedor !== null) {
        contenedor = idcontenedor;
    }
    $(formulario + ' :input[id = "page"]').val(1);
    cargarSubmitForm(formulario, contenedor, entidad);
}

/**
 * Función para cargar imagen LOADING.GIF de espera al cargar contenido
 * @return {html} Codigo HTML de imagen de carga
 */
function imgCargando() {
    return "<br>&nbsp;&nbsp;&nbsp;&nbsp;<font style='color: #505053;'>Cargando, por favor espere...</font><br><br>&nbsp;&nbsp;&nbsp;&nbsp;<img src='" + IMAGENLOADING + "'>";
}

/**
 * Función para realizar busqueda con paginación e inscrustar el RESPONSE en un div,
 * los parametros se envian por medio de $.POST de jquery a una URL
 * @param  {string} pagina      Numero de pagina a mostrar
 * @param  {string} mensaje     Mensajes luegos de INSERTAR, ACTUALIZAR y ELIMINAR un registro
 * @param  {string} entidad     Nombre de la entidad donde se va a inscrustar. Ejm: 'Usuario', 'Area', ect.
 * @return {html}               Se incrusta el codigo HTML en el DIV
 */
function buscarCompaginado(pagina, mensaje, entidad, tipomesaje) {
    let formulario = IDFORMBUSQUEDA + entidad;
    let contenedor = 'listado' + entidad;
    if (pagina !== '') {
        $(formulario + ' :input[id = "page"]').val(pagina);
    }
    cargarSubmitForm(formulario, contenedor);
    if (mensaje !== '') {
        mostrarMensaje(mensaje, tipomesaje);
    }
}

/**
 * Función para generar ventanas modales
 * @param  {string} controlador   URL del controlador a donde se enviará el REQUEST
 * @param  {string} titulo        Título que se colocará en la ventanada modal
 * @param idboton
 * @return {html}                 Se genera un modal
 */
function modal(controlador, titulo, idboton = '') {
    let htmlbtnoriginal;
    if (typeof idboton != 'undefined' && idboton !== '') {
        let btn = $(idboton);
        let textobtn = btn.text();
        htmlbtnoriginal = btn.html();
        console.log(htmlbtnoriginal);
        let htmlbtn = "<i class='icon-spinner4 mr-2'></i> " + textobtn;
        btn.html(htmlbtn).addClass('disabled');
    }
    let idContenedor = "divModal" + contadorModal;
    let divmodal = "<div id=\"" + idContenedor + "\"></div>";
    let box = bootbox.dialog({
        message: divmodal,
        className: 'modal' + contadorModal,
        title: titulo,
        closeButton: false
    });
    box.prop('id', 'modal' + contadorModal);
    /*$('#modal'+contadorModal).draggable({
        handle: ".modal-header"
    });*/
    modales[contadorModal] = box;
    contadorModal = contadorModal + 1;
    setTimeout(function () {
        cargarRuta(controlador, idContenedor);
        if (typeof idboton != 'undefined' && idboton !== '') {
            let btn = $(idboton);
            btn.html(htmlbtnoriginal).removeClass('disabled');
        }
    }, 400);
}

/**
 * Registrar o modificar un recurso
 * @param  {string} entidad
 * @param idboton
 * @param entidad2
 * @param mycallback
 * @return {void}
 */
function guardar(entidad, idboton, entidad2 = null, mycallback) {
    let idformulario = IDFORMMANTENIMIENTO + entidad;
    let data = submitForm(idformulario);
    let respuesta = '';
    let listar = 'NO';
    if ($(idformulario + ' :input[id = "listar"]').length) {
        listar = $(idformulario + ' :input[id = "listar"]').val();
    }
    let btn = $(idboton);
    let textobtn = btn.text();
    let htmlbtn = "<i class='icon-spinner4 mr-2'></i> " + textobtn;
    btn.html(htmlbtn).addClass('disabled');
    data.done(function (msg) {
        respuesta = msg;
    }).fail(function (xhr, textStatus, errorThrown) {
        respuesta = 'ERROR';
    }).always(function () {
        btn.html(textobtn).removeClass('disabled');
        if (respuesta === 'ERROR') {
        } else {
            if (respuesta === 'OK') {
                cerrarModal();
                if (listar === 'SI') {
                    if (typeof entidad2 != 'undefined' && entidad2 !== '' && entidad2 != null) {
                        entidad = entidad2;
                    }
                    buscarCompaginado('', 'Accion realizada correctamente', entidad, 'OK');
                }
                if (typeof mycallback != 'undefined' && mycallback !== '') {
                    mycallback();
                }
            } else {
                mostrarErrores(respuesta, idformulario, entidad);
            }
        }
    });
}

/**
 *
 * @param entidad
 * @param idboton
 * @param entidad2
 * @param mycallback
 */
function guardararchivo(entidad, idboton, entidad2 = null, mycallback) {
    let idformulario = IDFORMMANTENIMIENTO + entidad;
    let data = submitFormFile(idformulario);
    let respuesta = '';
    let listar = 'NO';
    if ($(idformulario + ' :input[id = "listar"]').length) {
        listar = $(idformulario + ' :input[id = "listar"]').val();
    }
    let btn = $(idboton);
    let textobtn = btn.text();
    let htmlbtn = "<i class='icon-spinner4 mr-2'></i> " + textobtn;
    btn.html(htmlbtn).addClass('disabled');
    data.done(function (msg) {
        respuesta = msg;
    }).fail(function (xhr, textStatus, errorThrown) {
        respuesta = 'ERROR';
    }).always(function () {
        btn.html(textobtn).removeClass('disabled');
        if (respuesta === 'ERROR') {
        } else {
            if (respuesta === 'OK') {
                cerrarModal();
                if (listar === 'SI') {
                    if (typeof entidad2 != 'undefined' && entidad2 !== '' && entidad2 != null) {
                        entidad = entidad2;
                    }
                    buscarCompaginado('', 'Accion realizada correctamente', entidad, 'OK');
                }
                if (typeof mycallback != 'undefined' && mycallback !== '') {
                    mycallback();
                }
            } else {
                respuesta = JSON.parse(respuesta);
                mostrarErrores(respuesta, idformulario, entidad);
            }
        }
    });
}

/**
 * Mostrar errordes de valicadión
 * @param data
 * @param idformulario
 * @param entidad
 * @param divmensaje
 */
function mostrarErrores(data, idformulario, entidad, divmensaje = null) {
    try {
        let mensajes = data;
        let divError = '#divMensajeError' + entidad;
        if (divmensaje !== null) {
            divError = '#' + divmensaje + entidad;
        }
        $(divError).html('');
        $(idformulario).find(':input').each(function () {
            let elemento = this;
            let cadena = idformulario + " :input[id='" + elemento.id + "']";
            let elementoValidado = $(cadena);
            elementoValidado.removeClass('border-danger');
        });
        let cadenaError = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Por favor corrige los siguentes errores:</strong><ul>';
        for (let valor in mensajes) {
            let cadena = idformulario + " :input[id='" + valor + "']";
            let elementoValidado = $(cadena);
            elementoValidado.addClass('border-danger');
            let cantidad = mensajes[valor].length;
            for (let i = 0; i < cantidad; i++) {
                if (mensajes[valor][i] !== '') {
                    cadenaError += ' <li>' + mensajes[valor][i] + '</li>';
                }
            }
        }
        new PNotify({
            title: 'Errores en formulario',
            text: 'Existen errores en el formulario, por favor revisar.',
            icon: 'icon-blocked',
            type: 'error'
        });
        cadenaError += '</ul></div>';
        $(divError).html(cadenaError);
    } catch (e) {
        $('#divMensajeError' + (contadorModal - 1)).html(data);
    }
}

/**
 *
 * @param titulo
 * @param idreserva
 * @param habitacion_id
 */
function modadlEvento(titulo, idreserva, habitacion_id,) {
    bootbox.dialog({
            title: titulo,
            message: '<div class="row">  ' +
                '<div class="col-md-12">' +
                '<form action="">' +
                '<div class="form-group row">' +
                '<label class="col-md-4 col-form-label">Name</label>' +
                '<div class="col-md-8">' +
                '<input id="name" name="name" type="text" placeholder="Your name" class="form-control">' +
                '<span class="form-text text-muted">Here goes your name</span>' +
                '</div>' +
                '</div>' +
                '<div class="form-group row">' +
                '<label class="col-md-4 col-form-label">How awesome is this?</label>' +
                '<div class="col-md-8">' +
                '<div class="form-check">' +
                '<label class="form-check-label">' +
                '<input type="radio" class="form-check-input" name="awesomeness" id="awesomeness-0" value="Really awesome" checked>' +
                'Really awesomeness' +
                '</label>' +
                '</div>' +
                '<div class="form-check">' +
                '<label class="form-check-label">' +
                '<input type="radio" class="form-check-input" name="awesomeness" id="awesomeness-1" value="Super awesome">' +
                'Super awesome' +
                '</label>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</form>' +
                '</div>' +
                '</div>',
            buttons: {
                success: {
                    label: 'Save',
                    className: 'btn-success',
                    callback: function () {
                        let name = $('#name').val();
                        let answer = $('input[name="awesomeness"]:checked').val()
                        bootbox.alert({
                            title: 'Hello ' + name + '!',
                            message: 'You have chosen <strong>' + answer + '</strong>.'
                        });
                    }
                }
            }
        }
    );
}

/**
 * Eliminar recurso
 * @param  {string} idformulario
 * @param  {string} entidad
 * @param {string} mensajepersonalizado
 * @return {html}
 */
function eliminar(idformulario, entidad, mensajepersonalizado) {
    let mensaje = '¿Está seguro de eliminar el registro?';
    if (typeof mensajepersonalizado != 'undefined' && mensajepersonalizado !== '') {
        mensaje = mensajepersonalizado;
    }
    bootbox.confirm({
        message: mensaje,
        buttons: {
            'cancel': {
                label: 'Cancelar',
                className: 'btn btn-default btn-sm'
            },
            'confirm': {
                label: 'Eliminar',
                className: 'btn btn-danger btn-sm'
            }
        },
        callback: function (result) {
            if (result) {
                let respuesta = '';
                let data = submitForm('#' + idformulario);
                data.done(function (msg) {
                    respuesta = msg;
                }).fail(function (xhr, textStatus, errorThrown) {
                    respuesta = 'ERROR';
                }).always(function () {
                    if (respuesta === 'OK') {
                        buscarCompaginado('', 'Accion realizada correctamente', entidad, 'OK');
                    } else if (respuesta === 'ERROR') {
                        mostrarMensaje('Error procesando la eliminacion', 'ERROR');
                    } else {
                        let mensajes = JSON.parse(respuesta);
                        let cadenaError = '';
                        for (let valor in mensajes) {
                            let cantidad = mensajes[valor].length;
                            for (let i = 0; i < cantidad; i++) {
                                if (mensajes[valor][i] !== '') {
                                    cadenaError += '- ' + mensajes[valor][i] + '\n';
                                }
                            }
                        }
                        alert(cadenaError);
                    }
                });
            }
        }
    }).find("div.modal-content").addClass("bootboxConfirmWidth");
    setTimeout(function () {
        if (contadorModal !== 0) {
            $('.modal' + (contadorModal - 1)).css('pointer-events', 'auto');
            $('body').addClass('modal-open');
        }
    }, 2000);
}


/**
 * Funcion para inicar un formulario de mantenimiento
 * @return {[type]}
 * @param idformulario
 * @param tipoformulario
 */
function init(idformulario, tipoformulario) {
    $(idformulario).submit(function (event) {
        event.preventDefault();
    });
    $('label').click(function (event) {
        event.preventDefault();
        let a = $(this).attr("for");
        let b = $(idformulario + ' :input[id="' + a + '"]');
        let tipo = b.attr('type');
        if (tipo === 'checkbox') {
            if (b.is(':checked')) {
                b.prop("checked", false);
            } else {
                b.prop("checked", true);
            }
            b.trigger('onchange');
        } else {
            b.focus();
        }
    });
    if (tipoformulario === 'M') {
        $(idformulario + ' input, ' + idformulario + ' select, ' + idformulario + ' .calendar').keydown(function (e) {
            let key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
            if (key === 13) {
                e.preventDefault();
                let inputs = $(this).closest('form').find(':input:visible:not([disabled]):not([readonly])');
                inputs.eq(inputs.index(this) + 1).focus();
            }
        });
    }
}


/**
 * Función para cerrar el último modal generado
 * @return {void} Se ejecuta la acción de cerrar el último modal generado
 */
function cerrarModal() {
    contadorModal = contadorModal - 1;
    modales[contadorModal].modal('hide');
    if (contadorModal !== 0) {
        $('.modal' + (contadorModal - 1)).css('pointer-events', 'auto');
        $('body').addClass('modal-open');
    }
}

/**
 * Función para cerrar el último modal generado mediante confirmacion
 * @return {void} Se ejecuta la acción de cerrar el último modal generado
 */
function cerrarModalConfirm() {
    bootbox.confirm({
        message: '¿Está seguro de cerrar la ventana?',
        buttons: {
            'cancel': {
                label: 'Cancelar',
                className: 'btn btn-default btn-sm'
            },
            'confirm': {
                label: 'Cerrar',
                className: 'btn btn-danger btn-sm'
            }
        },
        callback: function (result) {
            if (result) {
                contadorModal = contadorModal - 1;
                modales[contadorModal].modal('hide');
            }
        }
    }).find("div.modal-content").addClass("bootboxConfirmWidth");
    setTimeout(function () {
        if (contadorModal !== 0) {
            $('.modal' + (contadorModal - 1)).css('pointer-events', 'auto');
            $('body').addClass('modal-open');
        }
    }, 2000);
}

/**
 * Generar mensaje que aparece a la derecha de la pantalla
 * @param  {string} mensaje
 * @param tipo
 * @return {html}
 */
function mostrarMensaje(mensaje, tipo) {
    let divMensaje = $('#divMensaje');
    divMensaje.removeClass('alert-danger');
    divMensaje.removeClass('alert-success');
    if (tipo === 'ERROR') {
        divMensaje.addClass('alert-danger');
    } else if (tipo === 'OK') {
        divMensaje.addClass('alert-success');
    }
    divMensaje.html('<span>' + mensaje + '</span>');
    divMensaje.show('slow');
    setTimeout(function () {
        divMensaje.html('');
        divMensaje.hide("slow");
    }, 3000);
}

/**
 * Mostrar provincias después de seleccionar departamento
 * @param ruta
 * @param entidad
 * @param tipo
 */
function mostrarProvincias(ruta, entidad, tipo) {
    let iddepartamento;
    if (tipo === 'B') {
        iddepartamento = $(IDFORMBUSQUEDA + entidad + " :input[id='departamento_id']").val();
    }
    if (tipo === 'M') {
        iddepartamento = $(IDFORMMANTENIMIENTO + entidad + " :input[id='departamento_id']").val();
    }
    if (iddepartamento !== '') {
        ruta = ruta + '/' + iddepartamento;
    }
    let respuesta = '';
    let data = sendRuta(ruta);
    data.done(function (msg) {
        respuesta = msg;
    }).fail(function (xhr, textStatus, errorThrown) {
        respuesta = estiloError('Error en el procesamiento de la ruta');
    }).always(function () {
        if (tipo === 'B') {
            $(IDFORMBUSQUEDA + entidad + " :input[id='provincia_id']").html("'<option value=''>Todas</option>");
            $(IDFORMBUSQUEDA + entidad + " :input[id='provincia_id']").append(respuesta);
            $(IDFORMBUSQUEDA + entidad + " :input[id='distrito_id']").html("'<option value=''>Todas</option>");
        }
        if (tipo === 'M') {
            $(IDFORMMANTENIMIENTO + entidad + " :input[id='provincia_id']").html("'<option value=''>Seleccione provincia</option>");
            $(IDFORMMANTENIMIENTO + entidad + " :input[id='provincia_id']").append(respuesta);
            $(IDFORMMANTENIMIENTO + entidad + " :input[id='distrito_id']").html("'<option value=''>Seleccione distrito</option>");
        }
    });
}

/**
 * Cargar combo con distritos, después de seleccionar provincia
 * @param ruta
 * @param entidad
 * @param tipo
 */
function mostrarDistritos(ruta, entidad, tipo) {
    let idprovincia;
    if (tipo === 'B') {
        idprovincia = $(IDFORMBUSQUEDA + entidad + " :input[id='provincia_id']").val();
    }
    if (tipo === 'M') {
        idprovincia = $(IDFORMMANTENIMIENTO + entidad + " :input[id='provincia_id']").val();
    }
    if (idprovincia !== '') {
        ruta = ruta + '/' + idprovincia;
    }
    let respuesta = '';
    let data = sendRuta(ruta);
    data.done(function (msg) {
        respuesta = msg;
    }).fail(function (xhr, textStatus, errorThrown) {
        respuesta = estiloError('Error en el procesamiento de la ruta');
    }).always(function () {
        if (tipo === 'B') {
            $(IDFORMBUSQUEDA + entidad + " :input[id='distrito_id']").html("'<option value=''>Todas</option>");
            $(IDFORMBUSQUEDA + entidad + " :input[id='distrito_id']").append(respuesta);
        }
        if (tipo === 'M') {
            $(IDFORMMANTENIMIENTO + entidad + " :input[id='distrito_id']").html("'<option value=''>Seleccione distrito</option>");
            $(IDFORMMANTENIMIENTO + entidad + " :input[id='distrito_id']").append(respuesta);
        }
    });
}

/**
 * Completar con ceros un número
 * @param numero
 * @param length
 * @returns {string}
 */
function completarCeros(numero, length) {
    let resultado = "";
    let ceros = "0";
    let lengthAux = numero.length;
    if (lengthAux === length) {
        resultado = numero;
    } else if (lengthAux < length) {
        for (i = 0; i < (length - lengthAux); i++) {
            resultado = resultado + ceros;
        }
        resultado = resultado + numero;
    } else if (lengthAux > length) {
        resultado = "NUMERO MAXIMO ALCANZADO";
    }
    return resultado;
}

/**
 * Permitir solo decimales en una caja de texto
 * @param evt
 * @returns {boolean}
 */
function solo_decimal(evt) {
    let charCode = (evt.which) ? evt.which : event.keyCode;
    return !((charCode > 31 && (charCode < 46 || charCode > 57)) || charCode === 47);

}

/**
 * Permitir solo numeros en una caja de text
 * @param evt
 * @returns {boolean}
 */
function solo_numero(evt) {
    let charCode = (evt.which) ? evt.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));

}

function configurarAnchoModal(ancho) {
    let nuevoancho = ancho + 'px';
    if (contadorModal === 1) {
        let divModal = '.modal' + (contadorModal - 1);
        $(divModal).children('.modal-dialog').css('width', 'auto');
        $(divModal).children('.modal-dialog').css('max-width', nuevoancho);
        $(divModal).children('.modal-dialog').css('margin-left', 'auto');
        $(divModal).children('.modal-dialog').css('margin-right', 'auto');
    } else {
        let divModal = '.modal' + (contadorModal - 1);
        let divModal2 = '.modal' + (contadorModal - 2);
        $(divModal).children('.modal-dialog').css('width', 'auto');
        $(divModal).children('.modal-dialog').css('max-width', nuevoancho);
        $(divModal).children('.modal-dialog').css('margin-left', 'auto');
        $(divModal).children('.modal-dialog').css('margin-right', 'auto');
        $(divModal2).css('pointer-events', 'none');
    }
}

function nuevaVentana(URL, tipo) {
    window.open(URL, tipo);
}

function confirmarEliminar(entidad, idboton, entidad2) {
    let idformulario = IDFORMMANTENIMIENTO + entidad;
    let data = submitForm(idformulario);
    let respuesta = '';
    let listar = 'NO';
    if ($(idformulario + ' :input[id = "listar"]').length) {
        let listar = $(idformulario + ' :input[id = "listar"]').val()
    }
    let btn = $(idboton);
    let textobtn = btn.text();
    let htmlbtn = "<i class='icon-spinner4 mr-2'></i> " + textobtn;
    btn.html(htmlbtn).addClass('disabled');
    data.done(function (msg) {
        respuesta = msg;
    }).fail(function (xhr, textStatus, errorThrown) {
        respuesta = 'ERROR';
    }).always(function () {
        btn.html(textobtn).removeClass('disabled');
        if (respuesta === 'ERROR') {
        } else {
            if (respuesta === 'OK') {
                cerrarModal();
                buscar(entidad)
            } else {
                mostrarErrores(respuesta, idformulario, entidad);
            }
        }
    });
}

/**
 *
 * @param idformulario
 * @param idboton
 */
function confirmarpermiso(idformulario, idboton) {
    let data = submitForm('#' + idformulario);
    let respuesta = '';
    let url = $('#' + idformulario + ' :input[id="url"]').val();
    let btn = $(idboton);
    let textobtn = btn.text();s
    let htmlbtn = "<i class='icon-spinner4 mr-2'></i> " + textobtn;
    btn.html(htmlbtn).addClass('disabled');
    data.done(function (msg) {
        respuesta = msg;
    }).fail(function (xhr, textStatus, errorThrown) {
        respuesta = 'ERROR';
    }).always(function () {
        btn.html(textobtn).removeClass('disabled');
        if (respuesta === 'ERROR') {
        } else {
            if (respuesta === 'OK') {
                let divModal = 'divModal' + (contadorModal - 1);
                cargarRuta(url, divModal);
            } else {
                alert(respuesta);
            }
        }
    });
}

/**
 * cargar facultades en un select, enviando el id de la universidad
 * @param  {string} ruta    ruta del generador del combo
 * @param  {string} entidad para identificar en que formulario se va a cargar
 * @param  {string} tipo    B: para formulario de búsqueda, M: Para formulario de mantenimiento
 * @return {string}         contenido que se cargará en el select
 */
function mostrarFacultades(ruta, entidad, tipo) {
    let iduniversidad;
    if (tipo === 'B') {
        iduniversidad = $(IDFORMBUSQUEDA + entidad + " :input[id='universidad_id']").val();
    }
    if (tipo === 'M') {
        iduniversidad = $(IDFORMMANTENIMIENTO + entidad + " :input[id='universidad_id']").val();
    }
    if (iduniversidad !== '') {
        ruta = ruta + '/' + iduniversidad;
        let respuesta = '';
        let data = sendRuta(ruta);
        data.done(function (msg) {
            respuesta = msg;
        }).fail(function (xhr, textStatus, errorThrown) {
            respuesta = estiloError('Error en el procesamiento de la ruta');
        }).always(function () {
            if (tipo === 'B') {
                $(IDFORMBUSQUEDA + entidad + " :input[id='facultad_id']").html("'<option value=''>Todas</option>");
                $(IDFORMBUSQUEDA + entidad + " :input[id='facultad_id']").append(respuesta);
            }
            if (tipo === 'M') {
                $(IDFORMMANTENIMIENTO + entidad + " :input[id='facultad_id']").html("'<option value=''>Seleccione facultad</option>");
                $(IDFORMMANTENIMIENTO + entidad + " :input[id='facultad_id']").append(respuesta);
            }
        });
    } else {
        $(IDFORMMANTENIMIENTO + entidad + " :input[id='facultad_id']").html("'<option value=''>Seleccione facultad</option>");
    }
}

/**
 * cargar escuelas en un select, enviando el id de la facultad
 * @param  {string} ruta    ruta del generador del combo
 * @param  {string} entidad para identificar en que formulario se va a cargar
 * @param  {string} tipo    B: para formulario de búsqueda, M: Para formulario de mantenimiento
 * @return {string}         contenido que se cargará en el select
 */
function mostrarEscuelas(ruta, entidad, tipo) {
    let idfacultad;
    if (tipo === 'B') {
        idfacultad = $(IDFORMBUSQUEDA + entidad + " :input[id='facultad_id']").val();
    }
    if (tipo === 'M') {
        idfacultad = $(IDFORMMANTENIMIENTO + entidad + " :input[id='facultad_id']").val();
    }
    if (idfacultad !== '') {
        ruta = ruta + '/' + idfacultad;
    }
    let respuesta = '';
    let data = sendRuta(ruta);
    data.done(function (msg) {
        respuesta = msg;
    }).fail(function (xhr, textStatus, errorThrown) {
        respuesta = estiloError('Error en el procesamiento de la ruta');
    }).always(function () {
        if (tipo === 'B') {
            $(IDFORMBUSQUEDA + entidad + " :input[id='escuela_id']").html("'<option value=''>Todas</option>");
            $(IDFORMBUSQUEDA + entidad + " :input[id='escuela_id']").append(respuesta);
        }
        if (tipo === 'M') {
            $(IDFORMMANTENIMIENTO + entidad + " :input[id='escuela_id']").html("'<option value=''>Seleccione distrito</option>");
            $(IDFORMMANTENIMIENTO + entidad + " :input[id='escuela_id']").append(respuesta);
        }
    });
}