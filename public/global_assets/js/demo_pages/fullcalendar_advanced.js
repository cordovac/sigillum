/* ------------------------------------------------------------------------------
 *
 *  # Fullcalendar advanced options
 *
 *  Demo JS code for extra_fullcalendar_advanced.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var FullCalendarAdvanced = function () {

    // External events
    var _componentFullCalendarEvents = function () {
        if (!$().fullCalendar || typeof Switchery == 'undefined' || !$().draggable) {
            console.warn('Warning - fullcalendar.min.js, switchery.min.js or jQuery UI is not loaded.');
            return;
        }

        // External events
        // ------------------------------

        // Add switcher for events removal

        // Initialize the calendar
        $('.fullcalendar-external').fullCalendar({
            header: {
                left: 'prev,next today myCustomButton',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            editable: true,
            defaultDate: new Date(),
            // events: eventColors,
            locale: 'es',
            droppable: true, // this allows things to be dropped onto the calendar
            drop: function () {
                if ($('#drop-remove').is(':checked')) { // is the "remove after drop" checkbox checked?
                    $(this).remove(); // if so, remove the element from the "Draggable Events" list
                }
            },
            customButtons: {
                myCustomButton: {
                    id: 'btnRegistrar',
                    text: 'Registrar',
                    click: function () {
                        modal('reserva/create?listar=SI', 'Registrar reserva');
                        // modadlEvento("Registrar evento");
                    }
                }
            },
            eventClick: function (event) {
                if (event.id) {
                    modal('reserva/' + event.id + '/edit?listar=NO', 'Modificar reserva');
                }
            },
            isRTL: $('html').attr('dir') == 'rtl' ? true : false
        })
        ;
    };

    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _componentFullCalendarEvents();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function () {
    FullCalendarAdvanced.init();
});
