<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Categoriamenu extends Model
{
    use SoftDeletes;

    protected $table = 'categoriamenu';
    protected $dates = ['deleted_at'];


    public function mostrar()
    {
        return 'nombre';
    }

    public function opcionmenu()
    {
        return $this->hasMany('App\Opcionmenu');
    }

    public function categoriamenupadre()
    {
        return $this->belongsTo('App\Categoriamenu', 'categoriamenu_id');
    }

    public function Soncategoriamenu()
    {
        return $this->hasMany('App\Categoriamenu', 'categoriamenu_id');
    }

    public function scopelistar($query, $querysearch)
    {
        return $query->where(function ($subquery) use ($querysearch) {
            if (!is_null($querysearch)) {
                $subquery->orwhere('nombre', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('orden', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('icono', 'LIKE', '%' . $querysearch . '%');
            }
        })->orderBy('nombre', 'ASC')
            ->orderBy('categoriamenu_id', 'desc');

    }

}
