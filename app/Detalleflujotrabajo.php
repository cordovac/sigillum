<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Detalleflujotrabajo extends Model
{
    use SoftDeletes;

    protected $table = 'detalle_flujotrabajo';
    protected $dates = ['deleted_at'];

    /**
     * @param $query
     * @param $nombre
     * @param $flujotrabajo_id
     * @return mixed
     */
    public function scopelistar($query, $nombre, $flujotrabajo_id)
    {
        return $query->join('tarea', 'detalle_flujotrabajo.tarea_id', 'tarea.id')
            ->where(function ($subquery) use ($nombre, $flujotrabajo_id) {
                if (!is_null($nombre)) {
                    $subquery->where('detalle_flujotrabajo.secuencia', 'LIKE', '%' . $nombre . '%');
                }
                if (!is_null($flujotrabajo_id)) {
                    $subquery->where('detalle_flujotrabajo.flujotrabajo_id', $flujotrabajo_id);
                }
            })->where('tarea.editable', 1)
            ->whereNull('tarea.deleted_at')
            ->select('detalle_flujotrabajo.*')
            ->orderBy('detalle_flujotrabajo.secuencia', 'ASC');
    }

    /**
     * @return BelongsTo
     */
    public function tarea()
    {
        return $this->belongsTo('App\Tarea', 'tarea_id');
    }

    /**
     * @return BelongsTo
     */
    public function flujotrabajo()
    {
        return $this->belongsTo('App\Flujotrabajo', 'flujotrabajo_id');
    }

    public function detalletipousuarios()
    {
        return $this->hasMany('App\Detalleflujotrabajotipousuario', 'detalleflujotrabajo_id');
    }

    /**
     * @param $query
     * @param $secuencia
     * @param $flujotrabajo_id
     * @return mixed
     */
    public function scopesiguiente($query, $secuencia, $flujotrabajo_id)
    {
        return $query->join('tarea', 'detalle_flujotrabajo.tarea_id', 'tarea.id')
            ->where('tarea.editable', 1)
            ->whereNull('tarea.deleted_at')
            ->where('detalle_flujotrabajo.flujotrabajo_id', $flujotrabajo_id)
            ->where('detalle_flujotrabajo.secuencia', '>', $secuencia)
            ->orderBy('detalle_flujotrabajo.secuencia', 'ASC')
            ->select('detalle_flujotrabajo.*')
            ->first();
    }

    /**
     * @param $query
     * @param $secuencia
     * @param $flujotrabajo_id
     * @return mixed
     */
    public function scopeanterior($query, $secuencia, $flujotrabajo_id)
    {
        return $query->join('tarea', 'detalle_flujotrabajo.tarea_id', 'tarea.id')
            ->where('tarea.editable', 1)
            ->whereNull('tarea.deleted_at')
            ->where('detalle_flujotrabajo.flujotrabajo_id', $flujotrabajo_id)
            ->where('detalle_flujotrabajo.secuencia', '<', $secuencia)
            ->select('detalle_flujotrabajo.*')
            ->orderBy('detalle_flujotrabajo.secuencia', 'DESC')->first();
    }

    /**
     * @param $query
     * @param $flujotrabajo_id
     * @return |null
     */
    public function scopeinicio($query, $flujotrabajo_id)
    {
        return $query->where('flujotrabajo_id', $flujotrabajo_id)->where('secuencia', 'I')->first();
    }

    /**
     * @param $query
     * @param $flujotrabajo_id
     * @return |null
     */
    public function scopefin($query, $flujotrabajo_id)
    {
        return $query->where('flujotrabajo_id', $flujotrabajo_id)->where('secuencia', 'F')->first();
    }

    /**
     * @param $query
     * @param $secuencia
     * @param $flujotrabajo_id
     * @return |null
     */
    public function scopenodo($query, $secuencia, $flujotrabajo_id)
    {
        $anterior = null;
        $detalle = $query->where('flujotrabajo_id', $flujotrabajo_id)->where('secuencia', $secuencia)->first();
        if (!is_null($detalle)) {
            $anterior = $detalle;
        }
        return $anterior;
    }

    /**
     * @return BelongsToMany
     */
    public function tipousuarios()
    {
        return $this->belongsToMany('App\Tipousuario', 'detalleflujotrabajo_tipousuario', 'detalleflujotrabajo_id', 'tipousuario_id')
            ->whereNull('detalleflujotrabajo_tipousuario.deleted_at')
            ->withTimestamps();

    }

    /**
     * @param $query
     * @param $flujotrabajo_id
     * @param bool $incluiriniciofin
     * @return mixed
     */
    public function scopedetalleporflujotrabajo($query, $flujotrabajo_id, $incluiriniciofin = false)
    {
        return $query->join('tarea', 'detalle_flujotrabajo.tarea_id', 'tarea.id')
            ->where('detalle_flujotrabajo.flujotrabajo_id', $flujotrabajo_id)
            ->whereNull('tarea.deleted_at')
            ->where(function ($subquery) use ($incluiriniciofin) {
                if (!$incluiriniciofin) {
                    $subquery->where('tarea.editable', 1);
                }
            })
            ->select('detalle_flujotrabajo.*');
    }

    /**
     * @param $query
     * @param $flujotrabajo_id
     * @return mixed
     */
    public function scopepasoinicial($query, $flujotrabajo_id)
    {
        return $query->where('flujotrabajo_id', $flujotrabajo_id)->where('secuencia', 'I');
    }
}
