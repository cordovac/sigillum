<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Respuesta extends Model
{
    use SoftDeletes;

    protected $table = 'respuesta';
    protected $dates = ['deleted_at'];

    /**
     * @param $query
     * @param $nombre
     * @return mixed
     */
    public function scopelistar($query, $nombre)
    {
        return $query->where(function ($subquery) use ($nombre) {
            if (!is_null($nombre)) {
                $subquery->where('descripcion', 'LIKE', '%' . $nombre . '%');
            }
        })
            ->orderBy('descripcion', 'ASC');
    }

    /**
     * @return BelongsTo
     */
    public  function tarea(){
        return $this->belongsTo('App\Tarea', 'tarea_id');
    }
}
