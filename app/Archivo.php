<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class Archivo extends Model
{
    use SoftDeletes;

    protected $table = 'archivo';
    protected $dates = ['deleted_at'];

    /**
     * @return BelongsTo
     */
    public function historial()
    {
        return $this->belongsTo('App\Historial', 'historial_id');
    }

    /**
     * @return BelongsTo
     */
    public function padre()
    {
        return $this->belongsTo('App\Archivo', 'archivo_id');
    }
}
