<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Metadataregla extends Model
{
    use SoftDeletes;
    protected $table = 'metadata_regla';
    protected $dates = ['deleted_at'];

    /**
     * @param $query
     * @param $metadata_id
     * @return mixed
     */
    public function scopelistar($query, $metadata_id)
    {
        return $query->where(function ($subquery) use ($metadata_id) {
            if (!is_null($metadata_id)) {
                $subquery->where('metadata_id', $metadata_id);
            }
        })->orderBy('metadata_id', 'ASC');
    }

    /**
     * @return BelongsTo
     */
    public function regla()
    {
        return $this->belongsTo('App\Regla', 'regla_id');
    }

    /**
     * @return BelongsTo
     */
    public function metadata()
    {
        return $this->belongsTo('App\Metadata', 'metadata_id');
    }
}
