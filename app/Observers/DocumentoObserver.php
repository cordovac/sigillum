<?php

namespace App\Observers;

use App\Bitacora;
use App\Documento;
use App\Librerias\Libreria;
use DateTime;
use Illuminate\Support\Facades\Auth;

class DocumentoObserver
{
    /**
     * Handle the documento "created" event.
     *
     * @param  \App\Documento $documento
     * @return void
     * @throws \Exception
     */
    public function created(Documento $documento)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'C';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'documento';
        $bitacora->detalle = $documento->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $documento->id;
        $bitacora->save();
    }

    /**
     * Handle the documento "updated" event.
     *
     * @param  \App\Documento $documento
     * @return void
     * @throws \Exception
     */
    public function updated(Documento $documento)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'U';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'documento';
        $bitacora->detalle = $documento->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $documento->id;
        $bitacora->save();
    }

    /**
     * Handle the documento "deleted" event.
     *
     * @param  \App\Documento $documento
     * @return void
     * @throws \Exception
     */
    public function deleted(Documento $documento)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'D';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'documento';
        $bitacora->detalle = $documento->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $documento->id;
        $bitacora->save();
    }

    /**
     * Handle the documento "restored" event.
     *
     * @param  \App\Documento $documento
     * @return void
     */
    public function restored(Documento $documento)
    {
        //
    }

    /**
     * Handle the documento "force deleted" event.
     *
     * @param  \App\Documento $documento
     * @return void
     */
    public function forceDeleted(Documento $documento)
    {
        //
    }
}
