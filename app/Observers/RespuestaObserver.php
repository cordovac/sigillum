<?php

namespace App\Observers;

use App\Bitacora;
use App\Respuesta;
use App\Librerias\Libreria;
use DateTime;
use Illuminate\Support\Facades\Auth;

class RespuestaObserver
{
    /**
     * Handle the respuesta "created" event.
     *
     * @param  \App\Respuesta $respuesta
     * @return void
     * @throws \Exception
     */
    public function created(Respuesta $respuesta)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'C';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'respuesta';
        $bitacora->detalle = $respuesta->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $respuesta->id;
        $bitacora->save();
    }

    /**
     * Handle the respuesta "updated" event.
     *
     * @param  \App\Respuesta $respuesta
     * @return void
     * @throws \Exception
     */
    public function updated(Respuesta $respuesta)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'U';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'respuesta';
        $bitacora->detalle = $respuesta->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $respuesta->id;
        $bitacora->save();
    }

    /**
     * Handle the respuesta "deleted" event.
     *
     * @param  \App\Respuesta $respuesta
     * @return void
     * @throws \Exception
     */
    public function deleted(Respuesta $respuesta)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'D';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'respuesta';
        $bitacora->detalle = $respuesta->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $respuesta->id;
        $bitacora->save();
    }

    /**
     * Handle the respuesta "restored" event.
     *
     * @param  \App\Respuesta $respuesta
     * @return void
     */
    public function restored(Respuesta $respuesta)
    {
        //
    }

    /**
     * Handle the respuesta "force deleted" event.
     *
     * @param  \App\Respuesta $respuesta
     * @return void
     */
    public function forceDeleted(Respuesta $respuesta)
    {
        //
    }
}
