<?php

namespace App\Observers;

use App\Bitacora;
use App\Empresa;
use App\Librerias\Libreria;
use DateTime;
use Illuminate\Support\Facades\Auth;

class EmpresaObserver
{
    /**
     * Handle the empresa "created" event.
     *
     * @param  \App\Empresa $empresa
     * @return void
     * @throws \Exception
     */
    public function created(Empresa $empresa)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'C';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'empresa';
        $bitacora->detalle = $empresa->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $empresa->id;
        $bitacora->save();
    }

    /**
     * Handle the empresa "updated" event.
     *
     * @param  \App\Empresa $empresa
     * @return void
     * @throws \Exception
     */
    public function updated(Empresa $empresa)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'U';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'empresa';
        $bitacora->detalle = $empresa->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $empresa->id;
        $bitacora->save();
    }

    /**
     * Handle the empresa "deleted" event.
     *
     * @param  \App\Empresa $empresa
     * @return void
     * @throws \Exception
     */
    public function deleted(Empresa $empresa)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'D';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'empresa';
        $bitacora->detalle = $empresa->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $empresa->id;
        $bitacora->save();
    }

    /**
     * Handle the empresa "restored" event.
     *
     * @param  \App\Empresa $empresa
     * @return void
     */
    public function restored(Empresa $empresa)
    {
        //
    }

    /**
     * Handle the empresa "force deleted" event.
     *
     * @param  \App\Empresa $empresa
     * @return void
     */
    public function forceDeleted(Empresa $empresa)
    {
        //
    }
}
