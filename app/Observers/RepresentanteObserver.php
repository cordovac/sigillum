<?php

namespace App\Observers;

use App\Bitacora;
use App\Representante;
use App\Librerias\Libreria;
use DateTime;
use Illuminate\Support\Facades\Auth;

class RepresentanteObserver
{
    /**
     * Handle the representante "created" event.
     *
     * @param  \App\Representante $representante
     * @return void
     * @throws \Exception
     */
    public function created(Representante $representante)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'C';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'representante';
        $bitacora->detalle = $representante->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $representante->id;
        $bitacora->save();
    }

    /**
     * Handle the representante "updated" event.
     *
     * @param  \App\Representante $representante
     * @return void
     * @throws \Exception
     */
    public function updated(Representante $representante)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'U';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'representante';
        $bitacora->detalle = $representante->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $representante->id;
        $bitacora->save();
    }

    /**
     * Handle the representante "deleted" event.
     *
     * @param  \App\Representante $representante
     * @return void
     * @throws \Exception
     */
    public function deleted(Representante $representante)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'D';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'representante';
        $bitacora->detalle = $representante->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $representante->id;
        $bitacora->save();
    }

    /**
     * Handle the representante "restored" event.
     *
     * @param  \App\Representante $representante
     * @return void
     */
    public function restored(Representante $representante)
    {
        //
    }

    /**
     * Handle the representante "force deleted" event.
     *
     * @param  \App\Representante $representante
     * @return void
     */
    public function forceDeleted(Representante $representante)
    {
        //
    }
}
