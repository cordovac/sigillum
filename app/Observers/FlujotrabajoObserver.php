<?php

namespace App\Observers;

use App\Bitacora;
use App\Flujotrabajo;
use App\Librerias\Libreria;
use DateTime;
use Illuminate\Support\Facades\Auth;

class FlujotrabajoObserver
{
    /**
     * Handle the flujotrabajo "created" event.
     *
     * @param  \App\Flujotrabajo $flujotrabajo
     * @return void
     * @throws \Exception
     */
    public function created(Flujotrabajo $flujotrabajo)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'C';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'flujotrabajo';
        $bitacora->detalle = $flujotrabajo->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $flujotrabajo->id;
        $bitacora->save();
    }

    /**
     * Handle the flujotrabajo "updated" event.
     *
     * @param  \App\Flujotrabajo $flujotrabajo
     * @return void
     * @throws \Exception
     */
    public function updated(Flujotrabajo $flujotrabajo)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'U';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'flujotrabajo';
        $bitacora->detalle = $flujotrabajo->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $flujotrabajo->id;
        $bitacora->save();
    }

    /**
     * Handle the flujotrabajo "deleted" event.
     *
     * @param  \App\Flujotrabajo $flujotrabajo
     * @return void
     * @throws \Exception
     */
    public function deleted(Flujotrabajo $flujotrabajo)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'D';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'flujotrabajo';
        $bitacora->detalle = $flujotrabajo->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $flujotrabajo->id;
        $bitacora->save();
    }

    /**
     * Handle the flujotrabajo "restored" event.
     *
     * @param  \App\Flujotrabajo $flujotrabajo
     * @return void
     */
    public function restored(Flujotrabajo $flujotrabajo)
    {
        //
    }

    /**
     * Handle the flujotrabajo "force deleted" event.
     *
     * @param  \App\Flujotrabajo $flujotrabajo
     * @return void
     */
    public function forceDeleted(Flujotrabajo $flujotrabajo)
    {
        //
    }
}
