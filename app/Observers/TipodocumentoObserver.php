<?php

namespace App\Observers;

use App\Bitacora;
use App\Tipodocumento;
use App\Librerias\Libreria;
use DateTime;
use Illuminate\Support\Facades\Auth;

class TipodocumentoObserver
{
    /**
     * Handle the tipodocumento "created" event.
     *
     * @param  \App\Tipodocumento $tipodocumento
     * @return void
     * @throws \Exception
     */
    public function created(Tipodocumento $tipodocumento)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'C';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'tipodocumento';
        $bitacora->detalle = $tipodocumento->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $tipodocumento->id;
        $bitacora->save();
    }

    /**
     * Handle the tipodocumento "updated" event.
     *
     * @param  \App\Tipodocumento $tipodocumento
     * @return void
     * @throws \Exception
     */
    public function updated(Tipodocumento $tipodocumento)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'U';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'tipodocumento';
        $bitacora->detalle = $tipodocumento->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $tipodocumento->id;
        $bitacora->save();
    }

    /**
     * Handle the tipodocumento "deleted" event.
     *
     * @param  \App\Tipodocumento $tipodocumento
     * @return void
     * @throws \Exception
     */
    public function deleted(Tipodocumento $tipodocumento)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'D';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'tipodocumento';
        $bitacora->detalle = $tipodocumento->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $tipodocumento->id;
        $bitacora->save();
    }

    /**
     * Handle the tipodocumento "restored" event.
     *
     * @param  \App\Tipodocumento $tipodocumento
     * @return void
     */
    public function restored(Tipodocumento $tipodocumento)
    {
        //
    }

    /**
     * Handle the tipodocumento "force deleted" event.
     *
     * @param  \App\Tipodocumento $tipodocumento
     * @return void
     */
    public function forceDeleted(Tipodocumento $tipodocumento)
    {
        //
    }
}
