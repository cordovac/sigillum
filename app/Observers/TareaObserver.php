<?php

namespace App\Observers;

use App\Bitacora;
use App\Tarea;
use App\Librerias\Libreria;
use DateTime;
use Illuminate\Support\Facades\Auth;

class TareaObserver
{
    /**
     * Handle the tarea "created" event.
     *
     * @param  \App\Tarea $tarea
     * @return void
     * @throws \Exception
     */
    public function created(Tarea $tarea)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'C';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'tarea';
        $bitacora->detalle = $tarea->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $tarea->id;
        $bitacora->save();
    }

    /**
     * Handle the tarea "updated" event.
     *
     * @param  \App\Tarea $tarea
     * @return void
     * @throws \Exception
     */
    public function updated(Tarea $tarea)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'U';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'tarea';
        $bitacora->detalle = $tarea->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $tarea->id;
        $bitacora->save();
    }

    /**
     * Handle the tarea "deleted" event.
     *
     * @param  \App\Tarea $tarea
     * @return void
     * @throws \Exception
     */
    public function deleted(Tarea $tarea)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'D';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'tarea';
        $bitacora->detalle = $tarea->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $tarea->id;
        $bitacora->save();
    }

    /**
     * Handle the tarea "restored" event.
     *
     * @param  \App\Tarea $tarea
     * @return void
     */
    public function restored(Tarea $tarea)
    {
        //
    }

    /**
     * Handle the tarea "force deleted" event.
     *
     * @param  \App\Tarea $tarea
     * @return void
     */
    public function forceDeleted(Tarea $tarea)
    {
        //
    }
}
