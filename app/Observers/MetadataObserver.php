<?php

namespace App\Observers;

use App\Bitacora;
use App\Metadata;
use App\Librerias\Libreria;
use DateTime;
use Illuminate\Support\Facades\Auth;

class MetadataObserver
{
    /**
     * Handle the metadata "created" event.
     *
     * @param  \App\Metadata $metadata
     * @return void
     * @throws \Exception
     */
    public function created(Metadata $metadata)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'C';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'metadata';
        $bitacora->detalle = $metadata->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $metadata->id;
        $bitacora->save();
    }

    /**
     * Handle the metadata "updated" event.
     *
     * @param  \App\Metadata $metadata
     * @return void
     * @throws \Exception
     */
    public function updated(Metadata $metadata)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'U';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'metadata';
        $bitacora->detalle = $metadata->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $metadata->id;
        $bitacora->save();
    }

    /**
     * Handle the metadata "deleted" event.
     *
     * @param  \App\Metadata $metadata
     * @return void
     * @throws \Exception
     */
    public function deleted(Metadata $metadata)
    {
        $ahora = new DateTime;
        $bitacora = new Bitacora();
        $bitacora->accion = 'D';
        $bitacora->fecha = $ahora;
        $bitacora->ip = Libreria::get_client_ip();
        $bitacora->usuario_id = Auth::user()->id;
        $bitacora->tabla = 'metadata';
        $bitacora->detalle = $metadata->toJson(JSON_UNESCAPED_UNICODE);
        $bitacora->registroid = $metadata->id;
        $bitacora->save();
    }

    /**
     * Handle the metadata "restored" event.
     *
     * @param  \App\Metadata $metadata
     * @return void
     */
    public function restored(Metadata $metadata)
    {
        //
    }

    /**
     * Handle the metadata "force deleted" event.
     *
     * @param  \App\Metadata $metadata
     * @return void
     */
    public function forceDeleted(Metadata $metadata)
    {
        //
    }
}
