<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Documento extends Model
{
    use SoftDeletes;
    protected $table = 'documento';
    protected $dates = ['deleted_at'];

    /**
     * @return BelongsTo
     */
    public function tipodocumento()
    {
        return $this->belongsTo('App\Tipodocumento', 'tipodocumento_id');
    }

    public function flujotrabajo()
    {
        return $this->belongsTo('App\Flujotrabajo', 'flujotrabajo_id');
    }

    /**
     * @param $query
     * @param $nombre
     * @return mixed
     */
    public function scopelistar($query, $nombre)
    {
        return $query->join('historial', 'documento.id', '=', 'historial.documento_id')
            ->join('detalle_flujotrabajo', 'historial.detalleflujotrabajo_id', 'detalle_flujotrabajo.id')
            ->join('tarea', 'detalle_flujotrabajo.tarea_id', 'tarea.id')
            ->where('secuencia', 'I')
            ->where(function ($subquery) use ($nombre) {
            if (!is_null($nombre)) {
                $subquery->where('documento.numero', 'LIKE', '%' . $nombre . '%');
            }
        })->select('documento.*')->orderBy('numero', 'ASC');
    }

    public function scopecantidaddocumentos($query, $detalleflujotrabajo_id)
    {
        $detalleflujo = Detalleflujotrabajo::find($detalleflujotrabajo_id);
        $tarea = $detalleflujo->tarea;
        return $query->join('historial', 'documento.id', '=', 'historial.documento_id')
            ->join('detalle_flujotrabajo', 'historial.detalleflujotrabajo_id', 'detalle_flujotrabajo.id')
            ->join('detalleflujotrabajo_tipousuario', 'detalle_flujotrabajo.id', 'detalleflujotrabajo_tipousuario.detalleflujotrabajo_id')
            ->join('tipousuario', 'detalleflujotrabajo_tipousuario.tipousuario_id', 'tipousuario.id')
            ->where('detalle_flujotrabajo.tarea_id', $tarea->id)
            ->where('historial.pendiente', 1)
            ->where('historial.detalleflujotrabajo_id', $detalleflujo->id)
            ->whereNull('historial.fechaoperacion')
            ->whereNull('historial.respuestadetalleflujo_id')
            ->where('detalleflujotrabajo_tipousuario.tipousuario_id', Auth::user()->unicotipousuario->tipousuario->id)
            ->where('tipousuario.empresa_id', Auth::user()->representante->empresa_id)
            ->select('documento.*')->count();
    }

    public function scopecantidaddocumentosemisor($query, $detalleflujotrabajo_id)
    {
        $documentosrpropietarios = Documento::join('historial', 'historial.documento_id', 'documento.id')
            ->join('detalle_flujotrabajo', 'historial.detalleflujotrabajo_id', 'detalle_flujotrabajo.id')
            ->join('tarea', 'detalle_flujotrabajo.tarea_id', 'tarea.id')
            ->whereNull('tarea.deleted_at')
            ->whereNull('historial.deleted_at')
            ->whereNull('detalle_flujotrabajo.deleted_at')
            ->where('tarea.codigo', 'I')
            ->where('historial.usuario_id', Auth::user()->id)
            ->select('documento.id', DB::raw('historial.id AS historial_id'))->get();

        $ids = array();

        foreach ($documentosrpropietarios as $key => $value) {
            $ids[] = $value->id;
        }

        $detalleflujo = Detalleflujotrabajo::find($detalleflujotrabajo_id);
        $tarea = $detalleflujo->tarea;
        return $query->join('historial', 'documento.id', '=', 'historial.documento_id')
            ->join('detalle_flujotrabajo', 'historial.detalleflujotrabajo_id', 'detalle_flujotrabajo.id')
            ->where('detalle_flujotrabajo.tarea_id', $tarea->id)
            ->where('historial.pendiente', 1)
            ->where('historial.detalleflujotrabajo_id', $detalleflujo->id)
            ->whereNull('historial.fechaoperacion')
            ->whereNull('historial.respuestadetalleflujo_id')
            ->whereIn('documento.id', $ids)
            ->select('documento.*')->count();
    }
}
