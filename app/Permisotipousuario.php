<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permisotipousuario extends Model
{
    protected $table = 'permiso_tipousuario';
    public $timestamps = false;

    public function permiso()
    {
        return $this->belongsTo('App\Permiso');
    }

    public function tipousuario()
    {
        return $this->belongsTo('App\Tipousuario');
    }

    public function mostrar($id)
    {
         $tipousuario = Permisotipousuario::find($id);
         return $tipousuario->permiso->nombre. "-". $tipousuario->tipousuario->nombre;
    }


}
