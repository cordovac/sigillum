<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Flujotrabajo extends Model
{
    use SoftDeletes;
    protected $table = 'flujotrabajo';
    protected $dates = ['deleted_at'];

    /**
     * Verificamos si el usuario logado es super usuario y así enviarle solo la lista de flujos de trabajo
     * de su empresa o todas
     * @param $query
     * @param $nombre
     * @return mixed
     */
    public function scopelistar($query, $nombre, $superusuario, $empresa_id)
    {
        if (!$superusuario) {
            return $query->join('empresa_flujotrabajo', 'flujotrabajo.id', 'empresa_flujotrabajo.flujotrabajo_id')
                ->where(function ($subquery) use ($nombre) {
                    if (!is_null($nombre)) {
                        $subquery->where('flujotrabajo.descripcion', 'LIKE', '%' . $nombre . '%');
                    }
                })->where(function ($subquery) use ($superusuario, $empresa_id) {
                    if (!$superusuario) {
                        $subquery->where('empresa_flujotrabajo.empresa_id', $empresa_id);
                    }
                })->whereNull('empresa_flujotrabajo.deleted_at')
                ->orderBy('flujotrabajo.descripcion', 'ASC')
                ->select('flujotrabajo.*');
        } else {
            return $query->where(function ($subquery) use ($nombre) {
                if (!is_null($nombre)) {
                    $subquery->where('flujotrabajo.descripcion', 'LIKE', '%' . $nombre . '%');
                }
            })->orderBy('flujotrabajo.descripcion', 'ASC')
                ->select('flujotrabajo.*');
        }

    }

    /**
     * @return BelongsToMany
     */
    public function tareas()
    {
        return $this->belongsToMany('App\Tarea', 'detalle_flujotrabajo', 'flujotrabajo_id', 'tarea_id');

    }

    /**
     * @return BelongsToMany
     */
    public function empresas()
    {
        return $this->belongsToMany('App\Empresa', 'empresa_flujotrabajo', 'flujotrabajo_id', 'empresa_id')
            ->whereNull('empresa_flujotrabajo.deleted_at')
            ->withTimestamps();
    }

    /**
     * @return HasMany
     */
    public function detalleempresas()
    {
        return $this->hasMany('App\Empresaflujotrabajo', 'flujotrabajo_id');
    }
}
