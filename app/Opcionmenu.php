<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Opcionmenu extends Model
{
    use SoftDeletes;


    protected $table = 'opcionmenu';
    protected $dates = ['deleted_at'];


    public function mostrar()
    {
        return 'nombre';
    }

    public function categoriamenu()
    {
        return $this->belongsTo('App\Categoriamenu', 'categoriamenu_id');
    }

    public function permiso()
    {
        return $this->hasMany('App\Permiso');
    }

    /**
     * Método para listar las opciones de menu
     * @param  [type] $query [description]
     * @return [type]        [description]
     */

    public function scopelistar($query, $querysearch, $categoriamenu_id)
    {
        return $query->where(function ($subquery) use ($querysearch) {
            if (!is_null($querysearch)) {
                $subquery->orwhere('nombre', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('link', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('orden', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('icono', 'LIKE', '%' . $querysearch . '%');
            }
        })
            ->where(function ($subquery) use ($categoriamenu_id) {
                if (!is_null($categoriamenu_id)) {
                    $subquery->where('categoriamenu_id', '=', $categoriamenu_id);
                }
            })
            ->orderBy('categoriamenu_id', 'ASC')
            ->orderBy('orden', 'ASC');
    }
}
