<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Respuestadetalleflujo extends Model
{
    use SoftDeletes;

    protected $table = 'respuesta_detalleflujo';
    protected $dates = ['deleted_at'];

    /**
     * @param $query
     * @param $detalleflujotrabajo_id
     * @return mixed
     */
    public function scopelistar($query, $detalleflujotrabajo_id)
    {
        return $query->where(function ($subquery) use ($detalleflujotrabajo_id) {
            if (!is_null($detalleflujotrabajo_id)) {
                $subquery->where('detalleflujotrabajo_id', $detalleflujotrabajo_id);
            }
        });
    }

    /**
     * @return BelongsTo
     */
    public function respuesta()
    {
        return $this->belongsTo('App\Respuesta', 'respuesta_id');
    }

    /**
     * @return BelongsTo
     */
    public function detalleflujotrabajo()
    {
        return $this->belongsTo('App\Detalleflujotrabajo', 'detalleflujotrabajo_id');
    }
}
