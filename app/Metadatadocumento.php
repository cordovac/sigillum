<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metadatadocumento extends Model
{
    protected $table = 'metadata_documento';
    public $timestamps = false;
}
