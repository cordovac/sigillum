<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bitacora extends Model
{
    use SoftDeletes;

    protected $table = 'bitacora';
    protected $dates = ['deleted_at'];

    /**
     * @param $query
     * @param $nombre
     * @return mixed
     */
    public function scopelistar($query, $nombre)
    {
        return $query->where(function ($subquery) use ($nombre) {
            if (!is_null($nombre)) {
                $subquery->where('detalle', 'LIKE', '%' . $nombre . '%');
            }
        })
            ->orderBy('detalle', 'ASC');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\User', 'usuario_id');
    }
}
