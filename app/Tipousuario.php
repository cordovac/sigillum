<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;


class Tipousuario extends Model
{
    use SoftDeletes;

    protected $table = 'tipousuario';
    protected $dates = ['deleted_at'];

    /**
     * @param $query
     * @param $nombre
     * @return mixed
     */
    public function scopelistar($query, $nombre, $empresa_id)
    {
        return $query->where(function ($subquery) use ($nombre) {
            if (!is_null($nombre)) {
                $subquery->where('nombre', 'LIKE', '%' . $nombre . '%');
            }
        })->where(function ($subquery) use ($empresa_id) {
            if (!is_null($empresa_id)) {
                $subquery->where('empresa_id', $empresa_id);
            }
        })
            ->orderBy('nombre', 'ASC');
    }


    /**
     * Método que hace una relación de muchos a muchos, y que devuelve todas las opciones de menu de un tipo de usuario
     * @return BelongsToMany sql
     */
    public function opcionmenu()
    {
        return $this->belongsToMany('App\Opcionmenu', 'permiso', 'tipousuario_id', 'opcionmenu_id');
    }


    /**
     * Método que retorna los usuarios con el tipo de usuario indicado
     * @return HasMany sql
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }

    /**
     * Método de que retorna todos los permisos para el tpo de usuario indicado
     * @return BelongsToMany sql
     */
    public function permisos()
    {
        return $this->belongsToMany('App\Permiso');
    }

    /**
     * @return BelongsTo
     */
    public function empresa()
    {
        return $this->belongsTo('App\Empresa', 'empresa_id');
    }


}
