<?php

namespace App\Providers;

use App\Documento;
use App\Empresa;
use App\Flujotrabajo;
use App\Metadata;
use App\Observers\DocumentoObserver;
use App\Observers\EmpresaObserver;
use App\Observers\FlujotrabajoObserver;
use App\Observers\MetadataObserver;
use App\Observers\RepresentanteObserver;
use App\Observers\RespuestaObserver;
use App\Observers\TareaObserver;
use App\Observers\TipodocumentoObserver;
use App\Representante;
use App\Respuesta;
use App\Tarea;
use App\Tipodocumento;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Empresa::observe(EmpresaObserver::class);
        Representante::observe(RepresentanteObserver::class);
        Documento::observe(DocumentoObserver::class);
        Metadata::observe(MetadataObserver::class);
        Respuesta::observe(RespuestaObserver::class);
        Tarea::observe(TareaObserver::class);
        Tipodocumento::observe(TipodocumentoObserver::class);
        Flujotrabajo::observe(FlujotrabajoObserver::class);
    }
}
