<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Representante extends Model
{
    use SoftDeletes;
    protected $table = 'representante';
    protected $dates = ['deleted_at'];

    /**
     * @param $query
     * @param $nombre
     * @return mixed
     */
    public function scopelistar($query, $nombre)
    {
        return $query->where(function ($subquery) use ($nombre) {
            if (!is_null($nombre)) {
                $subquery->where('nombre', 'LIKE', '%' . $nombre . '%')
                    ->orWhere('apellidopaterno', 'LIKE', '%' . $nombre . '%')
                    ->orWhere('apellidomaterno', 'LIKE', '%' . $nombre . '%');
            }
        })->orderBy('apellidopaterno', 'ASC')->orderBy('apellidomaterno', 'ASC');
    }

    public function scopelistardatos($query, $nombre)
    {
        return $query->where(function ($subquery) use ($nombre) {
            if (!is_null($nombre)) {
                $subquery->where('nombre', 'LIKE', '%' . $nombre . '%')
                    ->orWhere('apellidopaterno', 'LIKE', '%' . $nombre . '%')
                    ->orWhere('apellidomaterno', 'LIKE', '%' . $nombre . '%');
            }
        })->orderBy('apellidopaterno', 'ASC')->orderBy('apellidomaterno', 'ASC');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function empresa()
    {
        return $this->belongsTo('App\Empresa', 'empresa_id');
    }

    /**
     * @param null $searchquery
     * @return array
     */
    public static function buscarpersona($searchquery = null)
    {
        $results = array();
        $queries = DB::table('representante')
            ->orwhere('apellidopaterno', 'LIKE', '%' . $searchquery . '%')
            ->orWhere('apellidomaterno', 'LIKE', '%' . $searchquery . '%')
            ->orWhere('nombres', 'LIKE', '%' . $searchquery . '%')
            ->take(5)->get();

        foreach ($queries as $query) {
            $results[] = ['id' => $query->id, 'value' => $query->apellidopaterno . ' ' . $query->apellidomaterno . ' ' . $query->nombres];
        }
        return $results;
    }
}
