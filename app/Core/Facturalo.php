<?php

namespace App\Core;

use App\Core\Documents\InvoiceBuilder;
use App\Core\Documents\NoteBuilder;
use App\Core\Documents\SummaryBuilder;
use App\Core\Documents\VoidedBuilder;
use App\Core\Helpers\QrCode\QrCodeGenerate;
use App\Core\Helpers\Xml\XmlFormat;
use App\Core\Helpers\Xml\XmlHash;
use App\Core\Helpers\Storage\StorageDocument;
use App\Core\WS\Client\WsClient;
use App\Core\WS\Services\BillSender;
use App\Core\WS\Services\ExtService;
use App\Core\WS\Services\SummarySender;
use App\Core\WS\Services\SunatEndpoints;
use App\Core\WS\Signed\XmlSigned;
use App\Core\WS\Validator\XmlErrorCodeProvider;
use Exception;
use Mpdf\Mpdf;

class Facturalo
{
    use StorageDocument;

    protected $signer;
    protected $wsClient;
    protected $inputs;
    protected $company;
    protected $document;
    protected $type;
    protected $xmlSigned;
    protected $pathCertificate;
    protected $soapUsername;
    protected $soapPassword;
    protected $endpoint;

    public function __construct($company)
    {
        $this->signer = new XmlSigned();
        $this->wsClient = new WsClient();
        $this->company = $company;
        $this->setDataSoapType();
    }

    public function setInputs($inputs)
    {
        $this->inputs = $inputs;
        $this->type = $inputs['type'];
    }

    public function setDocument($document)
    {
        $this->document = $document;
    }

    public function createXmlAndSign()
    {
        $xmlUnsigned = $this->createXml();
        $xmlSigned = $this->signXml($xmlUnsigned);
        $this->updateDocumentByXml($xmlSigned);
    }

    public function save()
    {
        switch ($this->type) {
            case 'debit':
            case 'credit':
                $builder = new NoteBuilder();
                break;
            case 'summary':
                $builder = new SummaryBuilder();
                break;
            case 'voided':
                $builder = new VoidedBuilder();
                break;
            default:
                $builder = new InvoiceBuilder();
                break;
        }

        $this->document = $builder->save($this->inputs);
    }

    public function createXml()
    {
        $template = new Template();
        $xmlUnsigned = XmlFormat::format($template->xml($this->type, $this->company, $this->document));
        $this->uploadFile($xmlUnsigned, 'unsigned');
        return $xmlUnsigned;
    }

    public function signXml($content)
    {
        $this->signer->setCertificateFromFile($this->pathCertificate);
        $xmlSigned = $this->signer->signXml($content);
        $this->xmlSigned = $xmlSigned;
        $this->uploadFile($xmlSigned, 'signed');
        return $xmlSigned;
    }

    public function loadAndSendXml()
    {
        $content = $this->getStorage($this->document->filename, 'signed', $this->company->number);
        return $this->sendXml($content);
    }

    public function sendXml($content)
    {
        $sender = in_array($this->type, ['summary', 'voided'])?new SummarySender():new BillSender();
        $sender->setClient($this->wsClient);
        $sender->setCodeProvider(new XmlErrorCodeProvider());

        $res = $sender->send($this->document->filename, $content);

        if(!$res->isSuccess()) {
            throw new Exception("Code: {$res->getError()->getCode()}; Description: {$res->getError()->getMessage()}");
        } else {
            if(!in_array($this->type, ['summary', 'voided'])) {
                $cdrResponse = $res->getCdrResponse();
                $this->uploadFile($res->getCdrZip(), 'cdr');
                return [
                    'code' => $cdrResponse->getCode(),
                    'description' => $cdrResponse->getDescription(),
                    'notes' => $cdrResponse->getNotes()
                ];
            } else {
                $this->updateDocumentByTicket($res->getTicket());
                return true;
            }
        }
    }

    public function statusSummary($ticket)
    {
        $extService = new ExtService();
        $extService->setClient($this->wsClient);
        $extService->setCodeProvider(new XmlErrorCodeProvider());
        $res = $extService->getStatus($ticket);
        if(!$res->isSuccess()) {
            throw new Exception("Code: {$res->getError()->getCode()}; Description: {$res->getError()->getMessage()}");
        } else {
            $cdrResponse = $res->getCdrResponse();
            $this->uploadFile($res->getCdrZip(), 'cdr');
            return [
                'code' => $cdrResponse->getCode(),
                'description' => $cdrResponse->getDescription(),
                'notes' => $cdrResponse->getNotes()
            ];
        }
    }

    public function createPdf()
    {
        $template = new Template();
        $html = $template->pdf($this->type, $this->company, $this->document);

        $pdf = new Mpdf();
        $pdf->WriteHTML($html);
        $this->uploadFile($pdf->output('', 'S'), 'pdf');
    }

    public function uploadFile($file_content, $file_type)
    {
        $this->uploadStorage($this->document->filename, $file_content, $file_type, $this->company->number);
    }

    public function getDocument()
    {
        return $this->document;
    }

    public function getXmlSigned()
    {
        return $this->xmlSigned;
    }

    private function setDataSoapType()
    {
        if($this->company->soap_type_id === '01') {
            $this->soapUsername = '20000000000MODDATOS';
            $this->soapPassword = 'moddatos';
            $this->pathCertificate = __DIR__.'/.'WS'.'/.'Signed'.'/.'Resources'.'/.'certificate.pem';
            $this->endpoint = SunatEndpoints::FE_BETA;
        } else {
            $this->soapUsername = $this->company->soap_username;
            $this->soapPassword = $this->company->soap_password;
            $this->pathCertificate = storage_path('app'.'/.'certificates'.'/.$this->company->certificate);
            $this->endpoint = SunatEndpoints::FE_PRODUCCION;
        }
        $this->wsClient->setCredentials($this->soapUsername, $this->soapPassword);
        $this->wsClient->setService($this->endpoint);
    }

    private function updateDocumentByXml($xmlContent)
    {
        if(!in_array($this->type, ['summary', 'voided'])) {
            $hash = $this->getHash($xmlContent);
            $this->document->update([
                'hash' => $hash,
                'qr' => $this->getQr($hash)
            ]);
        }
    }

    private function updateDocumentByTicket($ticket)
    {
        if(in_array($this->type, ['summary', 'voided'])) {
            $this->document->update([
                'ticket' => $ticket
            ]);
        }
    }

    private function getHash($content)
    {
        $helper = new XmlHash();
        return $helper->getHashSign($content);
    }

    private function getQr($hash)
    {
        $customer = $this->document->customer;
        $text = join('|', [
            $this->company->number,
            $this->document->document_type_id,
            $this->document->series,
            $this->document->number,
            $this->document->total_igv,
            $this->document->total,
            $this->document->date_of_issue->format('Y-m-d'),
            $customer->identity_document_type_id,
            $customer->number,
            $hash
        ]);

        $qrCode = new QrCodeGenerate();
        $qr = $qrCode->displayPNGBase64($text);
        return $qr;
    }
}
