<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Detalleflujotrabajotipousuario extends Model
{
    use SoftDeletes;

    protected $table = 'detalleflujotrabajo_tipousuario';
    protected $dates = ['deleted_at'];

    /**
     * @return BelongsTo
     */
    public function detalleflujotrabajo()
    {
        return $this->belongsTo('App\Detalleflujotrabajo', 'detalleflujotrabajo_id');
    }

    /**
     * @return BelongsTo
     */
    public function tipousuario()
    {
        return $this->belongsTo('App\Tipousuario', 'tipousuario_id');
    }
}
