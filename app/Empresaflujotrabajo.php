<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Empresaflujotrabajo extends Model
{
    use SoftDeletes;
    protected $table = 'empresa_flujotrabajo';
    protected $dates = ['deleted_at'];

    /**
     * @return BelongsTo
     */
    public function empresa()
    {
        return $this->belongsTo('App\Empresa', 'empresa_id');
    }

    /**
     * @return BelongsTo
     */
    public function flujotrabajo()
    {
        return $this->belongsTo('App\Flujotrabajo', 'flujotrabajo_id');
    }
}
