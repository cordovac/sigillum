<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipousuariousuario extends Model
{
    protected $table = 'tipousuario_usuario';
    public $timestamps = false;

    public function usuario()
    {
        return $this->belongsTo('App\User', 'usuario_id');
    }

    public function tipousuario()
    {
        return $this->belongsTo('App\Tipousuario', 'tipousuario_id');
    }
}
