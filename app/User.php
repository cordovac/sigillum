<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'usuario';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @param $query
     * @param $querysearch
     * @return mixed
     */
    public function scopelistar($query, $querysearch)
    {
        return $query->where(function ($subquery) use ($querysearch) {
            if (!is_null($querysearch)) {
                $subquery->orwhere('name', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('email', 'LIKE', '%' . $querysearch . '%');
            }
        })->orderBy('name', 'ASC');
    }

    /**
     * @return BelongsTo
     */
    public function representante()
    {
        return $this->belongsTo('App\Representante', 'representante_id');
    }

    /**
     * @return HasMany
     */
    public function tiposusuarios()
    {
        return $this->hasMany('App\Tipousuariousuario', 'usuario_id');
    }

    /**
     * @return BelongsToMany
     */
    public function tipousuarios()
    {
        return $this->belongsToMany('App\Tipousuario', 'tipousuario_usuario', 'usuario_id', 'tipousuario_id');

    }

    /**
     * @return HasOne
     */
    public function unicotipousuario()
    {
        return $this->hasOne('App\Tipousuariousuario', 'usuario_id');
    }

    /**
     * @return mixed
     */
    public function getAvatarUrlAttribute()
    {
        return URL::to('/') . Storage::url('public/avatars/' . $this->id . '/' . $this->avatar);
    }
}
