<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Permiso extends Model
{
    use SoftDeletes;

    protected $table = 'permiso';
    protected $dates = ['deleted_at'];

    protected $cascadeDeletes = ['permiso_tipousuario'];

    /**
     * @param $query
     * @param $opcionmenu_id
     * @param $superusuario
     * @return mixed
     */
    public function scopelistar($query, $opcionmenu_id, $superusuario)
    {
        return $query->where(function ($subquery) use ($opcionmenu_id) {
            if (!is_null($opcionmenu_id)) {
                $subquery->where('opcionmenu_id', $opcionmenu_id);
            }
        })->where(function ($subquery) use ($superusuario) {
            if (!$superusuario) {
                $subquery->where('superusuario', false);
            }
        })->orderBy('nombre', 'ASC');
    }


    public function mostrar()
    {
        return 'nombre';
    }


    /**
     * The Tipousuario that belong to the permiso.
     */
    public function tipousuario()
    {
        return $this->belongsToMany('App\Tipousuario');
    }

    public function opcionmenu()
    {
        return $this->belongsTo('App\Opcionmenu');
    }

    /**
     * Relacion con la tabla intermedia permisotipousuario
     */
    public function permiso_tipousuario()
    {
        return $this->hasMany('App\Permisotipousuario');
    }
}
