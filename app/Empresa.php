<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Empresa extends Model
{
    use SoftDeletes;
    protected $table = 'empresa';
    protected $dates = ['deleted_at'];

    /**
     * @param $query
     * @param $nombre
     * @return mixed
     */
    public function scopelistar($query, $nombre)
    {
        return $query->where(function ($subquery) use ($nombre) {
            if (!is_null($nombre)) {
                $subquery->where('ruc', 'LIKE', '%' . $nombre . '%')
                ->orWhere('razonsocial', 'LIKE', '%' . $nombre . '%');
            }
        })->orderBy('razonsocial', 'ASC');
    }
}
