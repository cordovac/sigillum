<?php

namespace App\Http\Controllers;

use App\Detalleflujotrabajo;
use App\Empresa;
use App\Empresaflujotrabajo;
use App\Flujotrabajo;
use App\Librerias\Libreria;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class FlujotrabajoController extends Controller
{

    protected $folderview = 'app.flujotrabajo';
    protected $tituloAdmin = 'Flujos de trabajo';
    protected $tituloRegistrar = 'Registrar Flujo de trabajo';
    protected $tituloModificar = 'Modificar Flujo de trabajo';
    protected $tituloEliminar = 'Eliminar Flujo de trabajo';
    protected $rutas = array('create' => 'flujotrabajo.create',
        'edit' => 'flujotrabajo.edit',
        'delete' => 'flujotrabajo.eliminar',
        'search' => 'flujotrabajo.buscar',
        'index' => 'flujotrabajo.index',
        'permisos' => 'flujotrabajo.obtenerpermisos',
    );

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $entidad = 'Flujotrabajo';
        $title = 'Flujos de trabajo ';
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        // verificamos que tenga permiso para registrar
        $permisosnecesarios = array('add_flujotrabajo');
        $registrar = Libreria::verificarExistenciaPermiso($permisosnecesarios);
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta', 'registrar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $permisosnecesarios = array('add_flujotrabajo');
        if (!Libreria::verificarExistenciaPermiso($permisosnecesarios)) {
            return view('app.401');
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Flujotrabajo';
        $flujotrabajo = null;
        $formData = array('flujotrabajo.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        $cboEmpresa = Empresa::pluck('razonsocial', 'id')->all();
        $superusuario = Auth::user()->superusuario;
        return view($this->folderview . '.mant')->with(compact('flujotrabajo', 'formData', 'entidad', 'boton', 'listar', 'cboEmpresa', 'superusuario'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $permisosnecesarios = array('add_flujotrabajo');
        if (!Libreria::verificarExistenciaPermiso($permisosnecesarios)) {
            $error = array(
                'descripcion' => array(
                    'Error 401: No tiene permisos para realizar esta acción, consultar con el administrador del sistema'
                )
            );
            return response()->json($error);
        }
        $validacion = Validator::make($request->all(),
            array(
                'descripcion' => 'required|max:50|string',
                'codigo' => 'required|max:10|string',
                'empresa_id' => 'nullable|array',
            )
        );
        $empresas = Libreria::getParamarray($request->input('empresa_id'), array());
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request, $empresas) {
            $flujotrabajo = new Flujotrabajo();
            $flujotrabajo->descripcion = $request->input('descripcion');
            $flujotrabajo->codigo = $request->input('codigo');
            $flujotrabajo->save();
            if (!Auth::user()->superusuario) {
                $empresaflujotrabajo = new Empresaflujotrabajo();
                $empresaflujotrabajo->flujotrabajo_id = $flujotrabajo->id;
                $empresaflujotrabajo->empresa_id = Auth::user()->representante->empresa_id;
                $empresaflujotrabajo->save();
            } else {
                for ($i = 0; $i < count($empresas); $i++) {
                    $empresaflujotrabajo = new Empresaflujotrabajo();
                    $empresaflujotrabajo->flujotrabajo_id = $flujotrabajo->id;
                    $empresaflujotrabajo->empresa_id = $empresas[$i];
                    $empresaflujotrabajo->save();
                }
            }
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param Flujotrabajo $flujotrabajo
     * @return void
     */
    public function show(Flujotrabajo $flujotrabajo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return bool|string
     */
    public function edit(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'flujotrabajo');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $flujotrabajo = Flujotrabajo::find($id);
        $entidad = 'Flujotrabajo';
        $formData = array('flujotrabajo.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        $cboEmpresa = Empresa::pluck('razonsocial', 'id')->all();
        $superusuario = Auth::user()->superusuario;
        return view($this->folderview . '.mant')->with(compact('flujotrabajo', 'formData', 'entidad', 'boton', 'listar', 'cboEmpresa', 'superusuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return string
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'flujotrabajo');
        if ($existe !== true) {
            return $existe;
        }
        $validacion = Validator::make($request->all(),
            array(
                'descripcion' => 'required|max:50|string',
                'codigo' => 'required|max:10|string',
                'empresa_id' => 'nullable|array',
            )
        );
        $empresas = Libreria::getParamarray($request->input('empresa_id'), array());
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request, $id, $empresas) {
            $flujotrabajo = Flujotrabajo::find($id);
            $flujotrabajo->descripcion = $request->input('descripcion');
            $flujotrabajo->codigo = $request->input('codigo');
            $flujotrabajo->save();
            // registramos las nuevas asignaciones
            if (Auth::user()->superusuario) {
                // eliminamos las asignaciones anteriores
                Empresaflujotrabajo::where('flujotrabajo_id', $flujotrabajo->id)->delete();
                for ($i = 0; $i < count($empresas); $i++) {
                    $empresaflujotrabajo = new Empresaflujotrabajo();
                    $empresaflujotrabajo->flujotrabajo_id = $flujotrabajo->id;
                    $empresaflujotrabajo->empresa_id = $empresas[$i];
                    $empresaflujotrabajo->save();
                }
            }
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return bool|string
     */
    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'flujotrabajo');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $flujotrabajo = Flujotrabajo::find($id);
            $flujotrabajo->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @param Request $request
     * @return Response
     */
    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Flujotrabajo';
        $usuario = Auth::user();
        $superusuario = $usuario->superusuario;
        $empresa_id = $usuario->representante->empresa_id;
        $nombre = Libreria::getParam($request->input('nombre'));
        $resultado = Flujotrabajo::listar($nombre, $superusuario, $empresa_id);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Código', 'numero' => '1');
        $cabecera[] = array('valor' => 'Descripción', 'numero' => '1');
        if ($superusuario) {
            $cabecera[] = array('valor' => 'Empresa asignadas', 'numero' => '1');
        }
        // verificamos que tenga permiso para registrar
        $editar = Libreria::verificarExistenciaPermiso(array('change_flujotrabajo'));
        $eliminar = Libreria::verificarExistenciaPermiso(array('delete_flujotrabajo'));
        $verdetalle = Libreria::verificarExistenciaPermiso(array('flujotrabajo_list_detalleflujotrabajo'));
        if ($verdetalle) {
            $cabecera[] = array('valor' => 'Detalle', 'numero' => '1');
        }
        if ($editar) {
            $cabecera[] = array('valor' => 'Editar', 'numero' => '1');
        }
        if ($eliminar) {
            $cabecera[] = array('valor' => 'Eliminar', 'numero' => '1');
        }
        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio',
                'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta', 'editar',
                'eliminar', 'verdetalle', 'superusuario'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }


    /**
     * Función para confirmar la eliminación de un registrlo
     * @param integer $id id del registro a intentar eliminar
     * @param string $listarLuego consultar si luego de eliminar se listará
     * @return bool|string
     */
    public function eliminar($id, $listarLuego)
    {
        $existe = Libreria::verificarExistencia($id, 'flujotrabajo');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Flujotrabajo::find($id);
        $entidad = 'Flujotrabajo';
        $formData = array('route' => array('flujotrabajo.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        $mensaje = null;
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar', 'mensaje'));
    }

    /**
     * @param $flujotrabajo_id
     * @return bool|string
     */
    public function verdocumentos($flujotrabajo_id)
    {
        $existe = Libreria::verificarExistencia($flujotrabajo_id, 'flujotrabajo');
        if ($existe !== true) {
            return $existe;
        }
        $detalleflujotrabajo = Detalleflujotrabajo::where('flujotrabajo_id', $flujotrabajo_id)->orderBy('secuencia', 'ASC')->get();
        $cantidad = $detalleflujotrabajo->count();
        return view($this->folderview . '.documentos')->with(compact('detalleflujotrabajo', 'cantidad'));
    }
}
