<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Representante;
use App\Librerias\Libreria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class RepresentanteController extends Controller
{

    protected $folderview = 'app.representante';
    protected $tituloAdmin = 'Representantes';
    protected $tituloRegistrar = 'Registrar Representante';
    protected $tituloModificar = 'Modificar Representante';
    protected $tituloEliminar = 'Eliminar Representante';
    protected $rutas = array('create' => 'representante.create',
        'edit' => 'representante.edit',
        'delete' => 'representante.eliminar',
        'search' => 'representante.buscar',
        'index' => 'representante.index',
        'permisos' => 'representante.obtenerpermisos',
    );

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entidad = 'Representante';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Representante';
        $representante = null;
        $formData = array('representante.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        $cboEmpresa = ['' => 'Seleccione'] + Empresa::pluck('razonsocial', 'id')->all();
        return view($this->folderview . '.mant')->with(compact('representante', 'formData', 'entidad', 'boton', 'listar', 'cboEmpresa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(),
            array(
                'nombres' => 'required|string|max:100',
                'apellidopaterno' => 'required|string|max:100',
                'apellidomaterno' => 'required|max:100|string',
                'dni' => 'required|integer|regex:/^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/',
                'rol' => 'required|max:40|string',
                'empresa_id' => 'required|integer|exists:empresa,id',
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request) {
            $representante = new Representante();
            $representante->nombres = $request->input('nombres');
            $representante->apellidopaterno = $request->input('apellidopaterno');
            $representante->apellidomaterno = $request->input('apellidomaterno');
            $representante->dni = $request->input('dni');
            $representante->rol = $request->input('rol');
            $representante->empresa_id = $request->input('empresa_id');
            $representante->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Representante $representante
     * @return void
     */
    public function show(Representante $representante)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return bool|string
     */
    public function edit(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'representante');
        if ($existe !== true) {
            return $existe;
        }
        $cboEmpresa = ['' => 'Seleccione'] + Empresa::pluck('razonsocial', 'id')->all();
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $representante = Representante::find($id);
        $entidad = 'Representante';
        $formData = array('representante.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('representante', 'formData', 'entidad', 'boton', 'listar', 'cboEmpresa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return string
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'representante');
        if ($existe !== true) {
            return $existe;
        }
        $validacion = Validator::make($request->all(),
            array(
                'nombres' => 'required|string|max:100',
                'apellidopaterno' => 'required|string|max:100',
                'apellidomaterno' => 'required|max:100|string',
                'dni' => 'required|integer|regex:/^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/',
                'rol' => 'required|max:40|string',
                'empresa_id' => 'required|integer|exists:empresa,id',
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request, $id) {
            $representante = Representante::find($id);
            $representante->nombres = $request->input('nombres');
            $representante->apellidopaterno = $request->input('apellidopaterno');
            $representante->apellidomaterno = $request->input('apellidomaterno');
            $representante->dni = $request->input('dni');
            $representante->rol = $request->input('rol');
            $representante->empresa_id = $request->input('empresa_id');
            $representante->save();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return bool|string
     */
    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'representante');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $representante = Representante::find($id);
            $representante->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Representante';
        $nombre = Libreria::getParam($request->input('nombre'));
        $resultado = Representante::listar($nombre);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Empresa', 'numero' => '1');
        $cabecera[] = array('valor' => 'Apellidos y nombres', 'numero' => '1');
        $cabecera[] = array('valor' => 'DNI', 'numero' => '1');
        $cabecera[] = array('valor' => 'Rol', 'numero' => '1');
        $cabecera[] = array('valor' => 'Editar', 'numero' => '1');
        $cabecera[] = array('valor' => 'Eliminar', 'numero' => '1');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }


    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return bool|string
     */
    public function eliminar($id, $listarLuego)
    {

        $existe = Libreria::verificarExistencia($id, 'representante');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Representante::find($id);
        $entidad = 'Representante';
        $formData = array('route' => array('representante.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }
}
