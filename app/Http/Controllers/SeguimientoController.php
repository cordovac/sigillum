<?php

namespace App\Http\Controllers;

use App\Detalleflujotrabajo;
use App\Documento;
use App\Librerias\Libreria;
use App\Seguimiento;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;


class SeguimientoController extends Controller
{

    protected $folderview = 'app.seguimiento';
    protected $tituloAdmin = 'Seguimiento de documentos';
    protected $tituloRegistrar = 'Registrar Documentos';
    protected $tituloModificar = 'Modificar Documentos';
    protected $tituloEliminar = 'Eliminar Documentos';
    protected $rutas = array('create' => 'seguimiento.create',
        'edit' => 'seguimiento.edit',
        'delete' => 'seguimiento.eliminar',
        'search' => 'seguimiento.buscar',
        'index' => 'seguimiento.index',
    );

    /**
     * Mostrar el resultado de búsquedas
     *
     * @param Request $request
     * @return bool|string
     */
    public function buscar(Request $request)
    {
        $flujotrabajo_id = Libreria::getParam($request->input('flujotrabajo_id'));
        $entidad = 'Seguimiento';
        $existe = Libreria::verificarExistencia($flujotrabajo_id, 'flujotrabajo');
        if ($existe !== true) {
            return $existe;
        }
        $detalleflujotrabajo = Detalleflujotrabajo::join('tarea', 'detalle_flujotrabajo.tarea_id', 'tarea.id')
            ->where('detalle_flujotrabajo.flujotrabajo_id', $flujotrabajo_id)
            ->whereNull('tarea.deleted_at')
            ->where('tarea.editable', 1)
            ->select('detalle_flujotrabajo.*')
            ->orderBy('detalle_flujotrabajo.secuencia', 'ASC')->get();
        $cantidad = $detalleflujotrabajo->count();
        return view($this->folderview . '.list')->with(compact('detalleflujotrabajo', 'cantidad', 'entidad'));
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @param Request $request
     * @return bool|string
     */
    public function buscaremisor(Request $request)
    {
        $flujotrabajo_id = Libreria::getParam($request->input('flujotrabajo_id'));
        $entidad = 'Seguimiento';
        $existe = Libreria::verificarExistencia($flujotrabajo_id, 'flujotrabajo');
        if ($existe !== true) {
            return $existe;
        }
        $detalleflujotrabajo = Detalleflujotrabajo::join('tarea', 'detalle_flujotrabajo.tarea_id', 'tarea.id')
            ->where('detalle_flujotrabajo.flujotrabajo_id', $flujotrabajo_id)
            ->whereNull('tarea.deleted_at')
            ->where('tarea.codigo', '<>', 'I')
            ->select('detalle_flujotrabajo.*')
            ->orderBy('detalle_flujotrabajo.secuencia', 'ASC')->get();
        $cantidad = $detalleflujotrabajo->count();
        return view($this->folderview . '.listemisor')->with(compact('detalleflujotrabajo', 'cantidad', 'entidad'));
    }

    /**
     * @param $flujotrabajo_id
     * @return bool|Factory|View|string
     */
    public function buscartareas($flujotrabajo_id)
    {
        $entidad = 'Seguimiento';
        $existe = Libreria::verificarExistencia($flujotrabajo_id, 'flujotrabajo');
        if ($existe !== true) {
            return $existe;
        }
        $detalleflujotrabajo = Detalleflujotrabajo::join('tarea', 'detalle_flujotrabajo.tarea_id', 'tarea.id')
            ->where('detalle_flujotrabajo.flujotrabajo_id', $flujotrabajo_id)
            ->whereNull('tarea.deleted_at')
            ->where('tarea.editable', 1)
            ->select('detalle_flujotrabajo.*')
            ->orderBy('detalle_flujotrabajo.secuencia', 'ASC')->get();
        $cantidad = $detalleflujotrabajo->count();
        return view($this->folderview . '.tareas')->with(compact('detalleflujotrabajo', 'cantidad', 'entidad'));
    }

    /**
     * @param $flujotrabajo_id
     * @return bool|Factory|View|string
     */
    public function buscartareasemisor($flujotrabajo_id)
    {
        $entidad = 'Seguimiento';
        $existe = Libreria::verificarExistencia($flujotrabajo_id, 'flujotrabajo');
        if ($existe !== true) {
            return $existe;
        }
        $detalleflujotrabajo = Detalleflujotrabajo::join('tarea', 'detalle_flujotrabajo.tarea_id', 'tarea.id')
            ->where('detalle_flujotrabajo.flujotrabajo_id', $flujotrabajo_id)
            ->whereNull('tarea.deleted_at')
            ->where('tarea.codigo', '<>', 'I')
            ->select('detalle_flujotrabajo.*')
            ->orderBy('detalle_flujotrabajo.secuencia', 'ASC')->get();
        $cantidad = $detalleflujotrabajo->count();
        return view($this->folderview . '.tareasemisor')->with(compact('detalleflujotrabajo', 'cantidad', 'entidad'));
    }

    /**
     * @param $detalleflujotrabajo_id
     * @return bool|Factory|View|string
     */
    public function documentosportarea($detalleflujotrabajo_id)
    {
        $existe = Libreria::verificarExistencia($detalleflujotrabajo_id, 'detalle_flujotrabajo');
        if ($existe !== true) {
            return $existe;
        }
        $detalleflujo = Detalleflujotrabajo::find($detalleflujotrabajo_id);
        $tarea = $detalleflujo->tarea;
        $entidad = 'Documentos';

        $lista = Documento::join('historial', 'historial.documento_id', 'documento.id')
            ->join('detalle_flujotrabajo', 'historial.detalleflujotrabajo_id', 'detalle_flujotrabajo.id')
            ->join('detalleflujotrabajo_tipousuario', 'detalle_flujotrabajo.id', 'detalleflujotrabajo_tipousuario.detalleflujotrabajo_id')
            ->join('tipousuario', 'detalleflujotrabajo_tipousuario.tipousuario_id', 'tipousuario.id')
            ->whereNull('historial.deleted_at')
            ->whereNull('detalleflujotrabajo_tipousuario.deleted_at')
            ->whereNull('detalle_flujotrabajo.deleted_at')
            ->whereNull('historial.fechaoperacion')
            ->whereNull('historial.respuestadetalleflujo_id')
            ->where('historial.pendiente', 1)
            ->where('historial.detalleflujotrabajo_id', $detalleflujo->id)
            ->where('detalleflujotrabajo_tipousuario.tipousuario_id', Auth::user()->unicotipousuario->tipousuario->id)
            ->where('tipousuario.empresa_id', Auth::user()->representante->empresa_id)
            ->select('documento.*', DB::raw('historial.id AS historial_id'))->get();

        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Tipo de documento', 'numero' => '1');
        $cabecera[] = array('valor' => 'Número', 'numero' => '1');
        $cabecera[] = array('valor' => 'Enviado por', 'numero' => '1');
        $cabecera[] = array('valor' => 'Asignado a', 'numero' => '1');
        $cabecera[] = array('valor' => 'Comentario emisor', 'numero' => '1');
        $cabecera[] = array('valor' => 'Ver', 'numero' => '1');
        $cabecera[] = array('valor' => 'Historial', 'numero' => '1');
        $cabecera[] = array('valor' => 'Respuesta', 'numero' => '1');
        return view($this->folderview . '.documentos')->with(compact('lista', 'entidad', 'cabecera', 'tarea'));
    }

    /**
     * @param $detalleflujotrabajo_id
     * @return bool|Factory|View|string
     */
    public function documentosportareaemisor($detalleflujotrabajo_id)
    {
        $existe = Libreria::verificarExistencia($detalleflujotrabajo_id, 'detalle_flujotrabajo');
        if ($existe !== true) {
            return $existe;
        }
        $detalleflujo = Detalleflujotrabajo::find($detalleflujotrabajo_id);
        $tarea = $detalleflujo->tarea;
        $entidad = 'Documentos';

        $documentosrpropietarios = Documento::join('historial', 'historial.documento_id', 'documento.id')
            ->join('detalle_flujotrabajo', 'historial.detalleflujotrabajo_id', 'detalle_flujotrabajo.id')
            ->join('tarea', 'detalle_flujotrabajo.tarea_id', 'tarea.id')
            ->whereNull('tarea.deleted_at')
            ->whereNull('historial.deleted_at')
            ->whereNull('detalle_flujotrabajo.deleted_at')
            ->where('tarea.codigo', 'I')
            ->where('historial.usuario_id', Auth::user()->id)
            ->select('documento.id', DB::raw('historial.id AS historial_id'))->get();

        $ids = array();

        foreach ($documentosrpropietarios as $key => $value) {
            $ids[] = $value->id;
        }

        $lista = Documento::join('historial', 'historial.documento_id', 'documento.id')
            ->join('detalle_flujotrabajo', 'historial.detalleflujotrabajo_id', 'detalle_flujotrabajo.id')
            ->whereNull('historial.deleted_at')
            ->whereNull('detalle_flujotrabajo.deleted_at')
            ->whereNull('historial.fechaoperacion')
            ->whereNull('historial.respuestadetalleflujo_id')
            ->where('historial.pendiente', 1)
            ->where('historial.detalleflujotrabajo_id', $detalleflujo->id)
            ->whereIn('documento.id', $ids)
            ->select('documento.*', DB::raw('historial.id AS historial_id'))->get();

        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Tipo de documento', 'numero' => '1');
        $cabecera[] = array('valor' => 'Número', 'numero' => '1');
        $cabecera[] = array('valor' => 'Enviado por', 'numero' => '1');
        $cabecera[] = array('valor' => 'Asignado a', 'numero' => '1');
        $cabecera[] = array('valor' => 'Comentario emisor', 'numero' => '1');
        $cabecera[] = array('valor' => 'Ver', 'numero' => '1');
        $cabecera[] = array('valor' => 'Historial', 'numero' => '1');
        return view($this->folderview . '.documentosemisor')->with(compact('lista', 'entidad', 'cabecera', 'tarea'));
    }
}
