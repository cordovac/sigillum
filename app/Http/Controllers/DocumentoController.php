<?php

namespace App\Http\Controllers;

use App\Archivo;
use App\Detalleflujotrabajo;
use App\Detalleflujotrabajotipousuario;
use App\Documento;
use App\Empresa;
use App\Flujotrabajo;
use App\Historial;
use App\Librerias\Libreria;
use App\Metadata;
use App\Metadatadocumento;
use App\Metadataregla;
use App\Representante;
use App\Tipodocumento;
use DateTime;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;


class DocumentoController extends Controller
{

    protected $folderview = 'app.documento';
    protected $tituloAdmin = 'Documentos';
    protected $tituloRegistrar = 'Registrar Documento';
    protected $tituloModificar = 'Modificar Documento';
    protected $tituloEliminar = 'Eliminar Documento';
    protected $rutas = array('create' => 'documento.create',
        'edit' => 'documento.edit',
        'delete' => 'documento.eliminar',
        'search' => 'documento.buscar',
        'index' => 'documento.index',
        'permisos' => 'documento.obtenerpermisos',
    );

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $entidad = 'Documento';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return bool|string
     */
    public function create(Request $request)
    {
        $flujotrabajo_id = Libreria::getParam($request->input('flujotrabajo_id'));
        $existe = Libreria::verificarExistencia($flujotrabajo_id, 'flujotrabajo');
        if (!is_null($flujotrabajo_id) && $existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Documento';
        $documento = null;
        $formData = array('documento.store');
        $formData = array('route' => $formData, 'enctype' => "multipart/form-data", 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        $documento_id = null;
        $cboTipodocumento = ['' => 'Seleccione'] + Tipodocumento::pluck('descripcion', 'id')->all();
        $cboFlujotrabajo = ['' => 'Seleccione'] + Flujotrabajo::pluck('descripcion', 'id')->all();
        return view($this->folderview . '.mant')->with(compact('documento', 'formData', 'entidad', 'boton', 'listar', 'cboTipodocumento', 'cboFlujotrabajo', 'documento_id', 'flujotrabajo_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return string
     */
    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(),
            array(
                'numero' => 'required|string|max:100',
                'tipodocumento_id' => 'required|integer|exists:tipodocumento,id',
                'flujotrabajo_id' => 'required|integer|exists:flujotrabajo,id',
                'archivo' => 'nullable',
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        // Iniciamos con armado de reglas de validación
        $tipodocumento_id = $request->input('tipodocumento_id');
        $metadata = Metadata::where('tipodocumento_id', $tipodocumento_id)->get();
        $reglasvalidacion = array();
        foreach ($metadata as $key => $value) {
            $metadatareglas = Metadataregla::where('metadata_id', $value->id)->get();
            $reglas = array();
            foreach ($metadatareglas as $keyregla => $valueregla) {
                $cadenaregla = $valueregla->regla->codigo;
                if (!is_null(Libreria::getParam($valueregla->valor))) {
                    $cadenaregla .= ':' . $valueregla->valor;
                }
                $reglas[] = $cadenaregla;
            }
            $reglasvalidacion[$value->codigo] = $reglas;

        }
        $validacion = Validator::make($request->all(), $reglasvalidacion);
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request) {
            $documento = new Documento();
            $documento->numero = $request->input('numero');
            $documento->tipodocumento_id = $request->input('tipodocumento_id');
            $documento->flujotrabajo_id = $request->input('flujotrabajo_id');
            $documento->save();

            $metadata = Metadata::where('tipodocumento_id', $documento->tipodocumento_id)->get();
            foreach ($metadata as $key => $value) {
                $metadatadocumento = new Metadatadocumento();
                if ($value->tipocontrol === 'checkbox') {
                    $seleccionados = Libreria::getParamarray($request->input($value->codigo));
                    $seleccionados = implode(",", $seleccionados);
                    $metadatadocumento->valor = $seleccionados;
                } else {
                    $metadatadocumento->valor = Libreria::getParam($request->input($value->codigo));
                }
                $metadatadocumento->documento_id = $documento->id;
                $metadatadocumento->metadata_id = $value->id;
                $metadatadocumento->save();
            }

            // creamos paso de INICIO para saber quien inicio el trámite
            $detalleflujo = Detalleflujotrabajo::pasoinicial($documento->flujotrabajo_id);
            $idpasoinicial = null;
            if (count($detalleflujo->get()) > 0) {
                $primeropaso = $detalleflujo->first();
                $historial = new Historial();
                $ahora = new DateTime;
                $historial->usuario_id = Auth::user()->id;
                $historial->usuarioemisor_id = Auth::user()->id;
                $historial->fechaoperacion = $ahora;
                $historial->documento_id = $documento->id;
                $historial->pendiente = 0;
                $historial->detalleflujotrabajo_id = $primeropaso->id;
                $historial->observacion = 'Inicio el documento en el flujo del trabajo';
                $historial->save();

                // guardamos archivos
                if ($request->archivo) {
                    foreach ($request->archivo as $temporal) {
                        $url = 'archivos' . DIRECTORY_SEPARATOR . $documento->id . DIRECTORY_SEPARATOR . $historial->id;
                        $nombre = $temporal->storeAs($url, $temporal->getClientOriginalName());
                        $archivo = new Archivo();
                        $archivo->ruta = $nombre;
                        $archivo->nombre = $temporal->getClientOriginalName();
                        $archivo->historial_id = $historial->id;
                        $archivo->accion = 'C';
                        $archivo->save();
                    }
                }
                $idpasoinicial = $historial->id;
            }

            // retrasamos 1 segundo para registrar el primer paso
            sleep(1);

            // Enviamos el documento al primer paso
            $detalleflujo = Detalleflujotrabajo::detalleporflujotrabajo($documento->flujotrabajo_id);
            if (count($detalleflujo->get()) > 0 && !is_null($idpasoinicial)) {
                // obtener el primer resultado de la consulta
                $primeropaso = $detalleflujo->orderBy('detalle_flujotrabajo.secuencia', 'ASC')->first();
                $detalletipousuarios = Detalleflujotrabajotipousuario::where('detalleflujotrabajo_id', $primeropaso->id)->get();
                foreach ($detalletipousuarios as $keytipousuario => $valuetipousuario) {
                    $historial = new Historial();
                    $historial->documento_id = $documento->id;
                    $historial->usuarioemisor_id = Auth::user()->id;
                    $historial->detalleflujotrabajo_id = $primeropaso->id;
                    $historial->historial_id = $idpasoinicial;
                    $historial->save();
                }
            }


        });
        return is_null($error) ? "OK" : $error;
    }


    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return bool|string
     */
    public function show(Request $request, $id)
    {
        $flujotrabajo_id = Libreria::getParam($request->input('flujotrabajo_id'));
        $existe = Libreria::verificarExistencia($flujotrabajo_id, 'flujotrabajo');
        if (!is_null($flujotrabajo_id) && $existe !== true) {
            return $existe;
        }
        $existe = Libreria::verificarExistencia($id, 'documento');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $documento = Documento::find($id);
        $documento_id = $documento->id;
        $entidad = 'Documento';
        $formData = array('documento.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        $cboTipodocumento = ['' => 'Seleccione'] + Tipodocumento::pluck('descripcion', 'id')->all();
        $cboFlujotrabajo = ['' => 'Seleccione'] + Flujotrabajo::pluck('descripcion', 'id')->all();
        $cboEmpresaemisor = ['' => 'Seleccione'] + Empresa::pluck('razonsocial', 'id')->all();
        $cboEmpresareceptor = ['' => 'Seleccione'] + Empresa::pluck('razonsocial', 'id')->all();
        $cboRepresentanteemisor = ['' => 'Seleccione'] + Representante::select(DB::raw('CONCAT(apellidopaterno, " ", apellidomaterno, ", ", nombres) as descripcion'), 'id')->pluck('descripcion', 'id')->all();
        $cboRepresentantereceptor = ['' => 'Seleccione'] + Representante::select(DB::raw('CONCAT(apellidopaterno, " ", apellidomaterno, ", ", nombres) as descripcion'), 'id')->pluck('descripcion', 'id')->all();
        return view($this->folderview . '.show')->with(compact('documento', 'flujotrabajo_id', 'formData', 'entidad', 'boton', 'listar', 'cboTipodocumento', 'cboFlujotrabajo', 'cboEmpresaemisor', 'cboEmpresareceptor', 'cboRepresentanteemisor', 'cboRepresentantereceptor', 'documento_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return bool|string
     */
    public function edit(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'documento');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $documento = Documento::find($id);
        $documento_id = $documento->id;
        $entidad = 'Documento';
        $formData = array('documento.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        $cboTipodocumento = ['' => 'Seleccione'] + Tipodocumento::pluck('descripcion', 'id')->all();
        $cboFlujotrabajo = ['' => 'Seleccione'] + Flujotrabajo::pluck('descripcion', 'id')->all();
        $cboEmpresaemisor = ['' => 'Seleccione'] + Empresa::pluck('razonsocial', 'id')->all();
        $cboEmpresareceptor = ['' => 'Seleccione'] + Empresa::pluck('razonsocial', 'id')->all();
        $cboRepresentanteemisor = ['' => 'Seleccione'] + Representante::select(DB::raw('CONCAT(apellidopaterno, " ", apellidomaterno, ", ", nombres) as descripcion'), 'id')->pluck('descripcion', 'id')->all();
        $cboRepresentantereceptor = ['' => 'Seleccione'] + Representante::select(DB::raw('CONCAT(apellidopaterno, " ", apellidomaterno, ", ", nombres) as descripcion'), 'id')->pluck('descripcion', 'id')->all();
        return view($this->folderview . '.mant')->with(compact('documento', 'formData', 'entidad', 'boton', 'listar', 'cboTipodocumento', 'cboFlujotrabajo', 'cboEmpresaemisor', 'cboEmpresareceptor', 'cboRepresentanteemisor', 'cboRepresentantereceptor', 'documento_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return string
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'documento');
        if ($existe !== true) {
            return $existe;
        }
        $validacion = Validator::make($request->all(),
            array(
                'numero' => 'required|string|max:100',
                'tipodocumento_id' => 'required|integer|exists:tipodocumento,id',
                'flujotrabajo_id' => 'required|integer|exists:flujotrabajo,id',
                'empresaemisor_id' => 'required|integer|exists:empresa,id',
                'empresareceptor_id' => 'required|integer|exists:empresa,id',
                'representanteemisor_id' => 'required|integer|exists:representante,id',
                'representantereceptor_id' => 'required|integer|exists:representante,id',
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        // Iniciamos con armado de reglas de validación
        $tipodocumento_id = $request->input('tipodocumento_id');
        $metadata = Metadata::where('tipodocumento_id', $tipodocumento_id)->get();
        $reglasvalidacion = array();
        foreach ($metadata as $key => $value) {
            $metadatareglas = Metadataregla::where('metadata_id', $value->id)->get();
            $reglas = array();
            foreach ($metadatareglas as $keyregla => $valueregla) {
                $cadenaregla = $valueregla->regla->codigo;
                if (!is_null(Libreria::getParam($valueregla->valor))) {
                    $cadenaregla .= ':' . $valueregla->valor;
                }
                $reglas[] = $cadenaregla;
            }
            $reglasvalidacion[$value->codigo] = $reglas;

        }
        $validacion = Validator::make($request->all(), $reglasvalidacion);
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request, $id) {
            $documento = Documento::find($id);
            $documento->numero = $request->input('numero');
            $documento->tipodocumento_id = $request->input('tipodocumento_id');
            $documento->flujotrabajo_id = $request->input('flujotrabajo_id');
            $documento->empresaemisor_id = $request->input('empresaemisor_id');
            $documento->empresareceptor_id = $request->input('empresareceptor_id');
            $documento->representanteemisor_id = $request->input('representanteemisor_id');
            $documento->representantereceptor_id = $request->input('representantereceptor_id');
            $documento->save();

            $metadata = Metadata::where('tipodocumento_id', $documento->tipodocumento_id)->get();
            foreach ($metadata as $key => $value) {
                $valor = Metadatadocumento::where('metadata_id', $value->id)->where('documento_id', $documento->id)->first();
                if (!is_null($valor) && !is_null($valor->id)) {
                    $metadatadocumento = Metadatadocumento::find($valor->id);
                    if ($value->tipocontrol === 'checkbox') {
                        $seleccionados = Libreria::getParamarray($request->input($value->codigo));
                        $seleccionados = implode(",", $seleccionados);
                        $metadatadocumento->valor = $seleccionados;
                    } else {
                        $metadatadocumento->valor = Libreria::getParam($request->input($value->codigo));
                    }
                    $metadatadocumento->documento_id = $documento->id;
                    $metadatadocumento->metadata_id = $value->id;
                    $metadatadocumento->save();
                } else {
                    $metadatadocumento = new Metadatadocumento();
                    if ($value->tipocontrol === 'checkbox') {
                        $seleccionados = Libreria::getParamarray($request->input($value->codigo));
                        $seleccionados = implode(",", $seleccionados);
                        $metadatadocumento->valor = $seleccionados;
                    } else {
                        $metadatadocumento->valor = Libreria::getParam($request->input($value->codigo));
                    }
                    $metadatadocumento->documento_id = $documento->id;
                    $metadatadocumento->metadata_id = $value->id;
                    $metadatadocumento->save();
                }
            }
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return bool|string
     */
    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'documento');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $documento = Documento::find($id);
            $documento->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @param Request $request
     * @return Response
     */
    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Documento';
        $nombre = Libreria::getParam($request->input('nombre'));
        $resultado = Documento::listar($nombre);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Tipo de documento', 'numero' => '1');
        $cabecera[] = array('valor' => 'Flujo de trabajo', 'numero' => '1');
        $cabecera[] = array('valor' => 'Número', 'numero' => '1');
        $cabecera[] = array('valor' => 'Historial', 'numero' => '1');
        $cabecera[] = array('valor' => 'Ver', 'numero' => '1');
        $cabecera[] = array('valor' => 'Eliminar', 'numero' => '1');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }


    /**
     * Función para confirmar la eliminación de un registrlo
     * @param integer $id id del registro a intentar eliminar
     * @param string $listarLuego consultar si luego de eliminar se listará
     * @return bool|string
     */
    public function eliminar($id, $listarLuego)
    {

        $existe = Libreria::verificarExistencia($id, 'documento');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Documento::find($id);
        $entidad = 'Documento';
        $formData = array('route' => array('documento.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }

    /**
     * @param $tipodocumento_id
     * @param $documento_id
     * @return bool|Factory|View|string
     */
    public function generarmetadata($tipodocumento_id, $documento_id = null)
    {
        $existe = Libreria::verificarExistencia($tipodocumento_id, 'tipodocumento');
        if ($existe !== true) {
            return $existe;
        }
        $metadata = Metadata::where('tipodocumento_id', $tipodocumento_id)->get();
        return view($this->folderview . '.metadata')->with(compact('metadata', 'documento_id'));
    }
}
