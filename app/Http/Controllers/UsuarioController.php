<?php

namespace App\Http\Controllers;

use App\Librerias\Libreria;
use App\Representante;
use App\Tipousuario;
use App\Tipousuariousuario;
use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\View\View;
use Validator;
use Mail;


class Usuariocontroller extends Controller
{
    protected $folderview = 'app.usuario';
    protected $tituloAdmin = 'Usuario';
    protected $tituloRegistrar = 'Registrar Usuario';
    protected $tituloModificar = 'Modificar Usuario';
    protected $tituloEliminar = 'Eliminar Usuario';
    protected $tituloActivarusuario = 'Activar Usuario';
    protected $rutas = array('create' => 'usuario.create',
        'edit' => 'usuario.edit',
        'delete' => 'usuario.eliminar',
        'search' => 'usuario.buscar',
        'index' => 'usuario.index',
        'activarusuario' => 'usuario.activarusuario',
    );

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (!Libreria::verificarExistenciaPermiso('list_usuario')) {
            return view('app.401');
        }
        $entidad = 'Usuario';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        if (!Libreria::verificarExistenciaPermiso('add_usuario')) {
            return view('app.401');
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Usuario';
        $cboRepresentante = ['' => 'Seleccione'] + Representante::orderBy('nombres', 'ASC')->pluck('nombres', 'id')->all();
        $cboTipousuario = Tipousuario::join('empresa', 'tipousuario.empresa_id', 'empresa.id')
                        ->whereNull('tipousuario.deleted_at')
                        ->whereNull('empresa.deleted_at')
                        ->select(DB::raw('CONCAT(empresa.razonsocial, "-", tipousuario.nombre) as nombre'), 'tipousuario.id')->pluck('nombre', 'id')->all();
        $usuario = null;
        $tipousuario_id = null;
        $formData = array('usuario.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('usuario', 'formData', 'cboRepresentante', 'cboTipousuario', 'entidad', 'boton', 'listar', 'tipousuario_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return string
     */
    public function store(Request $request)
    {
        if (!Libreria::verificarExistenciaPermiso('add_usuario')) {
            return view('app.401');
        }
        $mensajes = array(
            'email.required' => 'Debe ingresar email',
            'password.required' => 'Debe ingresar password',
            'tipousuario_id.required' => 'Debe seleccionar tipo usuarios',
            'representante_id.required' => 'Debe seleccionar una representante',
        );
        $reglas = array(
            'email' => [
                'required',
                Rule::unique('usuario'),
                'email',
            ],
            'password' => 'required|string|min:6|confirmed',
            'tipousuario_id' => 'required|integer|exists:tipousuario,id',
            'representante_id' => 'required|integer|exists:representante,id',

        );
        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request) {

            $representante = DB::table('representante')->where('id', $request->input('representante_id'))->first();
            $usuario = new User();
            $usuario->name = $representante->nombres . " " . $representante->apellidopaterno . " " . $representante->apellidomaterno;
            $usuario->email = $request->input('email');
            $usuario->password = Hash::make($request->input('password'));
            $usuario->representante_id = $request->input('representante_id');
            $usuario->save();

            $tipousuario = new Tipousuariousuario();
            $tipousuario->usuario_id = $usuario->id;
            $tipousuario->tipousuario_id = $request->input('tipousuario_id');
            $tipousuario->save();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @param Request $request
     * @return bool|string
     */
    public function edit($id, Request $request)
    {

        if (!Libreria::verificarExistenciaPermiso('change_usuario')) {
            return view('app.401');
        }
        $existe = Libreria::verificarExistencia($id, 'usuario');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $usuario = User::find($id);
        $entidad = 'Usuario';
        $tipousuario_id = Tipousuariousuario::where('usuario_id', $id)->first()->tipousuario_id;
        $cboTipousuario = Tipousuario::join('empresa', 'tipousuario.empresa_id', 'empresa.id')
            ->whereNull('tipousuario.deleted_at')
            ->whereNull('empresa.deleted_at')
            ->select(DB::raw('CONCAT(empresa.razonsocial, "-", tipousuario.nombre) as nombre'), 'tipousuario.id')->pluck('nombre', 'id')->all();
        $cboRepresentante = ['' => 'Seleccione'] + Representante::orderBy('nombres', 'ASC')->pluck('nombres', 'id')->all();
        $formData = array('usuario.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('usuario', 'formData', 'entidad', 'boton', 'listar', 'cboTipousuario', 'cboRepresentante', 'tipousuario_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return bool|string
     */
    public function update(Request $request, $id)
    {
        if (!Libreria::verificarExistenciaPermiso('change_usuario')) {
            return view('app.401');
        }
        $existe = Libreria::verificarExistencia($id, 'usuario');
        if ($existe !== true) {
            return $existe;
        }

        $mensajes = array(
            'email' => 'Debe ingresar email',
            'usuario' => 'Debe ingresar usuario',
            'password' => 'Debe ingresar password',
            'tipousuario_id' => 'Debe seleccionar tipo usuarios',

        );

        $validacion = Validator::make($request->all(),
            array(
                'email' => [
                    'required',
                    Rule::unique('usuario')->ignore($id),
                ],
                'password' => 'nullable|string|min:6|confirmed',
                'tipousuario_id' => 'required|integer|exists:tipousuario,id',
            ), $mensajes);

        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }

        $error = DB::transaction(function () use ($request, $id) {

            $usuario = User::find($id);
            $usuario->email = $request->input('email');
            if (!is_null(Libreria::getParam($request->input('password')))) {
                $usuario->password = Hash::make($request->input('password'));
            }
            $usuario->save();

            /*Tipo Usuario*/
            $tipousuario_usuario = Tipousuariousuario::where('usuario_id', '=', $id)->get();
            foreach ($tipousuario_usuario as $key => $value) {
                DB::table('tipousuario_usuario')->where('usuario_id', '=', $id)->where('tipousuario_id', '=', $value->tipousuario_id)->delete();
            }
            $tipousuario = new Tipousuariousuario();
            $tipousuario->usuario_id = $id;
            $tipousuario->tipousuario_id = $request->input('tipousuario_id');
            $tipousuario->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!Libreria::verificarExistenciaPermiso('delete_usuario')) {
            return view('app.401');
        }
        $existe = Libreria::verificarExistencia($id, 'usuario');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $usuario = User::find($id);
            $usuario->delete();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @param Request $request
     * @return Response
     */
    public function buscar(Request $request)
    {
        $change = Libreria::verificarExistenciaPermiso('change_usuario');
        $delete = Libreria::verificarExistenciaPermiso('delete_usuario');

        if (!Libreria::verificarExistenciaPermiso('list_usuario')) {
            return view('app.401');
        }

        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Usuario';
        $querysearch = Libreria::getParam($request->input('querysearch'));
        $resultado = User::listar($querysearch);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Empresa', 'numero' => '1');
        $cabecera[] = array('valor' => 'Apellidos y nombres', 'numero' => '1');
        $cabecera[] = array('valor' => 'Email', 'numero' => '1');
        $cabecera[] = array('valor' => 'Tipo de usuario', 'numero' => '1');
        $cabecera[] = array('valor' => 'Editar', 'numero' => '1');
        $cabecera[] = array('valor' => 'Eliminar', 'numero' => '1');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $titulo_activarusuario = $this->tituloActivarusuario;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'change', 'delete', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta', 'titulo_activarusuario'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }

    /**
     * Función para confirmar la eliminación de un registrlo
     * @param integer $id id del registro a intentar eliminar
     * @param string $listarLuego consultar si luego de eliminar se listará
     * @return html              se retorna html, con la ventana de confirmar eliminar
     */
    public function eliminar($id, $listarLuego)
    {
        if (!Libreria::verificarExistenciaPermiso('delete_usuario')) {
            return view('app.401');
        }

        $existe = Libreria::verificarExistencia($id, 'usuario');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = User::find($id);
        $entidad = 'Usuario';
        $formData = array('route' => array('usuario.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }

    /**
     * Función para autocompletar representante
     * @param $querysearchrepresentante
     * @return html se retorna html, con la ventana de autocompletar
     */
    public function autocompletarrepresentante($querysearchrepresentante)
    {
        if (!Libreria::verificarExistenciaPermiso('read_usuario')) {
            return view('app.401');
        }
        $representante = Representante::buscarpersona($querysearchrepresentante);
        return json_encode($representante);

    }


    public function activarusuario($id, $listarLuego)
    {
        if (!Libreria::verificarExistenciaPermiso('add_usuario')) {
            return view('app.401');
        }

        $existe = Libreria::verificarExistencia($id, 'usuario');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = User::find($id);
        $entidad = 'Usuario';
        $formData = array('route' => array('usuario.activarcuenta', $id), 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Activar';
        return view($this->folderview . '.activarusuario')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }

    public function activarcuenta($id)
    {
        if (!Libreria::verificarExistenciaPermiso('add_usuario')) {
            return view('app.401');
        }
        $existe = Libreria::verificarExistencia($id, 'usuario');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $usuario = User::find($id);
            $usuario->estado = 'Activado';
            $usuario->save();

            /*Enviar Email*/

            $data = array(
                'usuario' => $usuario,
            );

            Mail::send($this->folderview . '.plantillaemail', $data, function ($message) use ($usuario) {
                $message->from('egresados@udl.edu.pe', 'Universidad de Lambayeque');
                $message->to($usuario->email)->subject('Activación cuenta bolsa trabajo');
            });

            /**/

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * @param $id
     * @param Request $request
     * @return bool|Factory|View|string
     */
    public function editclave($id, Request $request)
    {
        $existe = Libreria::verificarExistencia($id, 'usuario');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $usuario = User::find($id);
        $entidad = 'Usuario';
        $formData = array('usuario.updateclave', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.change')->with(compact('usuario', 'formData', 'entidad', 'boton', 'listar'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return bool|mixed|string
     */
    public function updateclave($id, Request $request)
    {
        $existe = Libreria::verificarExistencia($id, 'usuario');
        if ($existe !== true) {
            return $existe;
        }
        $reglas = array(
            'password' => 'nullable|string|min:6|confirmed',
        );
        $validacion = Validator::make($request->all(), $reglas);
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request, $id) {
            $usuario = User::find($id);
            $usuario->password = Hash::make($request->input('password'));
            $usuario->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * @param $id
     * @param Request $request
     * @return bool|Factory|View|string
     */
    public function editdatos($id, Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $usuario = Auth::user();
        $entidad = 'Usuario';
        $representante = Representante::find($usuario->representante_id);
        $formData = array('usuario.updatedatos', $usuario->id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.datos')->with(compact('usuario', 'formData', 'entidad', 'boton', 'listar', 'representante'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return bool|mixed|string
     */
    public function updatedatos($id, Request $request)
    {
        $existe = Libreria::verificarExistencia($id, 'usuario');
        if ($existe !== true) {
            return $existe;
        }
        $reglas = array(
            'nombres' => 'required|string|max:100',
            'apellidopaterno' => 'required|string|max:100',
            'apellidomaterno' => 'required|max:100|string',
            'dni' => 'required|integer|regex:/^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/',
            'rol' => 'required|max:40|string',
            'email' => [
                'required',
                Rule::unique('usuario')->ignore($id),
            ],
        );
        $validacion = Validator::make($request->all(), $reglas);
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request, $id) {
            $usuario = User::find($id);

            $representante = Representante::find($usuario->representante_id);
            $representante->nombres = $request->input('nombres');
            $representante->apellidopaterno = $request->input('apellidopaterno');
            $representante->apellidomaterno = $request->input('apellidomaterno');
            $representante->dni = $request->input('dni');
            $representante->rol = $request->input('rol');
            $representante->save();

            $usuario->name = $representante->nombres . " " . $representante->apellidopaterno . " " . $representante->apellidomaterno;
            $usuario->email = $request->input('email');
            $usuario->save();


        });
        return is_null($error) ? "OK" : $error;
    }
}
