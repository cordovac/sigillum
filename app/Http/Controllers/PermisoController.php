<?php

namespace App\Http\Controllers;

use App\Librerias\Libreria;
use App\Permiso;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class PermisoController extends Controller
{

    protected $folderview = 'app.permiso';
    protected $tituloAdmin = 'Permiso';
    protected $tituloRegistrar = 'Registrar Permiso';
    protected $tituloModificar = 'Modificar Permiso';
    protected $tituloEliminar = 'Eliminar Permiso';
    protected $rutas = array('create' => 'permiso.create',
        'edit' => 'permiso.edit',
        'delete' => 'permiso.eliminar',
        'search' => 'permiso.buscar',
        'index' => 'permiso.index',
        'permisos' => 'permiso.obtenerpermisos',
    );

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $opcionmenu_id = $request->input('opcionmenu_id');
        $entidad = 'Permiso';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta', 'opcionmenu_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $opcionmenu_id = Libreria::getParam($request->input('opcionmenu_id'));
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Permiso';
        $permiso = null;
        $formData = array('permiso.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        $cboTipocontrol = ['text' => 'Caja de texto simple'] + ['textarea' => 'Caja de texto amplia'] + ['select' => 'Selección'] + ['radio' => 'Radio Button'] + ['checkbox' => 'Checkbox'];
        return view($this->folderview . '.mant')->with(compact('permiso', 'formData', 'entidad', 'boton', 'listar', 'opcionmenu_id', 'cboTipocontrol'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(),
            array(
                'nombre' => 'required|max:50|string',
                'codigo' => 'required|max:50|string',
                'opcionmenu_id' => 'required|integer|exists:opcionmenu,id',
                'superusuario' => 'nullable|boolean'
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request) {
            $permiso = new Permiso();
            $permiso->nombre = $request->input('nombre');
            $permiso->codigo = $request->input('codigo');
            $permiso->opcionmenu_id = $request->input('opcionmenu_id');
            $permiso->superusuario = Libreria::getParam($request->input('superusuario'), 0);
            $permiso->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param Permiso $permiso
     * @return void
     */
    public function show(Permiso $permiso)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return bool|string
     */
    public function edit(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'permiso');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $permiso = Permiso::find($id);
        $opcionmenu_id = $permiso->opcionmenu_id;
        $entidad = 'Permiso';
        $formData = array('permiso.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $cboTipocontrol = ['text' => 'Caja de texto simple'] + ['textarea' => 'Caja de texto amplia'] + ['select' => 'Selección'] + ['radio' => 'Radio Button'] + ['checkbox' => 'Checkbox'];
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('permiso', 'formData', 'entidad', 'boton', 'listar', 'opcionmenu_id', 'cboTipocontrol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return string
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'permiso');
        if ($existe !== true) {
            return $existe;
        }
        $validacion = Validator::make($request->all(),
            array(
                'nombre' => 'required|max:45|string',
                'codigo' => 'required|max:45|string',
                'opcionmenu_id' => 'required|integer|exists:opcionmenu,id',
                'superusuario' => 'nullable|boolean'
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request, $id) {
            $permiso = Permiso::find($id);
            $permiso->nombre = $request->input('nombre');
            $permiso->codigo = $request->input('codigo');
            $permiso->opcionmenu_id = $request->input('opcionmenu_id');
            $permiso->superusuario = Libreria::getParam($request->input('superusuario'), 0);
            $permiso->save();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return bool|string
     */
    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'permiso');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $permiso = Permiso::find($id);
            $permiso->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @param Request $request
     * @return Response
     */
    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Permiso';
        $opcionmenu_id = Libreria::getParam($request->input('opcionmenu_id'));
        $superusuario = Auth::user()->superusuario;
        $resultado = Permiso::listar($opcionmenu_id, $superusuario);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => 'Nombre', 'numero' => '1');
        $cabecera[] = array('valor' => 'Codigo', 'numero' => '1');
        $cabecera[] = array('valor' => 'Superusuario', 'numero' => '1');
        $cabecera[] = array('valor' => 'Editar', 'numero' => '1');
        $cabecera[] = array('valor' => 'Eliminar', 'numero' => '1');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }


    /**
     * Función para confirmar la eliminación de un registrlo
     * @param integer $id id del registro a intentar eliminar
     * @param string $listarLuego consultar si luego de eliminar se listará
     * @return bool|string
     */
    public function eliminar($id, $listarLuego)
    {

        $existe = Libreria::verificarExistencia($id, 'permiso');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Permiso::find($id);
        $entidad = 'Permiso';
        $formData = array('route' => array('permiso.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        $mensaje = null;
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar', 'mensaje'));
    }
}
