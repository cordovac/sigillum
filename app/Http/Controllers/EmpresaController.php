<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Librerias\Libreria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class EmpresaController extends Controller
{

    protected $folderview = 'app.empresa';
    protected $tituloAdmin = 'Empresas';
    protected $tituloRegistrar = 'Registrar Empresa';
    protected $tituloModificar = 'Modificar Empresa';
    protected $tituloEliminar = 'Eliminar Empresa';
    protected $rutas = array('create' => 'empresa.create',
        'edit' => 'empresa.edit',
        'delete' => 'empresa.eliminar',
        'search' => 'empresa.buscar',
        'index' => 'empresa.index',
        'permisos' => 'empresa.obtenerpermisos',
    );

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entidad = 'Empresa';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Empresa';
        $empresa = null;
        $formData = array('empresa.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('empresa', 'formData', 'entidad', 'boton', 'listar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(),
            array(
                'ruc' => [
                    'required',
                    Rule::unique('empresa'),
                    'regex:/^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/',
                    'integer',
                ],
                'razonsocial' => [
                    'required',
                    'string',
                    'max:200'
                ],
                'direccionfiscal' => 'required|max:200|string',
                'fechaalta' => 'required|date_format:d/m/Y',
                'fechabaja' => 'nullable|date_format:d/m/Y',
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request) {
            $empresa = new Empresa();
            $empresa->ruc = $request->input('ruc');
            $empresa->razonsocial = $request->input('razonsocial');
            $empresa->direccionfiscal = $request->input('direccionfiscal');
            $empresa->fechaalta = Libreria::formatoFecha($request->input('fechaalta'), 'd/m/Y', 'Y-m-d');
            $empresa->fechabaja = Libreria::formatoFecha($request->input('fechabaja'), 'd/m/Y', 'Y-m-d');
            $empresa->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Empresa $empresa
     * @return void
     */
    public function show(Empresa $empresa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return bool|string
     */
    public function edit(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'empresa');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $empresa = Empresa::find($id);
        $entidad = 'Empresa';
        $formData = array('empresa.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('empresa', 'formData', 'entidad', 'boton', 'listar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return string
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'empresa');
        if ($existe !== true) {
            return $existe;
        }
        $validacion = Validator::make($request->all(),
            array(
                'ruc' => [
                    'required',
                    Rule::unique('empresa')->ignore($id),
                    'regex:/^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/',
                    'integer',
                ],
                'razonsocial' => [
                    'required',
                    'string',
                    'max:200'
                ],
                'direccionfiscal' => 'required|max:200|string',
                'fechaalta' => 'required|date_format:d/m/Y',
                'fechabaja' => 'nullable|date_format:d/m/Y',
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request, $id) {
            $empresa = Empresa::find($id);
            $empresa->ruc = $request->input('ruc');
            $empresa->razonsocial = $request->input('razonsocial');
            $empresa->direccionfiscal = $request->input('direccionfiscal');
            $empresa->fechaalta = Libreria::formatoFecha($request->input('fechaalta'), 'd/m/Y', 'Y-m-d');
            $empresa->fechabaja = Libreria::formatoFecha($request->input('fechabaja'), 'd/m/Y', 'Y-m-d');
            $empresa->save();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return bool|string
     */
    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'empresa');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $empresa = Empresa::find($id);
            $empresa->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Empresa';
        $nombre = Libreria::getParam($request->input('nombre'));
        $resultado = Empresa::listar($nombre);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'RUC', 'numero' => '1');
        $cabecera[] = array('valor' => 'Razón social', 'numero' => '1');
        $cabecera[] = array('valor' => 'Dirección', 'numero' => '1');
        $cabecera[] = array('valor' => 'Fecha alta', 'numero' => '1');
        $cabecera[] = array('valor' => 'Fecha baja', 'numero' => '1');
        $cabecera[] = array('valor' => 'Editar', 'numero' => '1');
        $cabecera[] = array('valor' => 'Eliminar', 'numero' => '1');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }


    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return bool|string
     */
    public function eliminar($id, $listarLuego)
    {

        $existe = Libreria::verificarExistencia($id, 'empresa');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Empresa::find($id);
        $entidad = 'Empresa';
        $formData = array('route' => array('empresa.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }
}
