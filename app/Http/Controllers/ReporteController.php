<?php

namespace App\Http\Controllers;

use App\Documento;
use App\Librerias\Libreria;
use App\Metadata;
use Illuminate\Http\Request;

class ReporteController extends Controller
{
    /**
     * @param $documento_id
     * @return mixed
     */
    public function documentopdf($documento_id)
    {
        $existe = Libreria::verificarExistencia($documento_id, 'documento');
        if ($existe !== true) {
            return $existe;
        }
        $documento = Documento::find($documento_id);
        $metadata = Metadata::where('tipodocumento_id', $documento->tipodocumento_id)->get();
        include_once(resource_path('views/app/documento/reporte/detalle.php'));
    }

}
