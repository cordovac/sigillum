<?php

namespace App\Http\Controllers;

use App\Librerias\Libreria;
use App\Metadata;
use App\Tipodocumento;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class MetadataController extends Controller
{

    protected $folderview = 'app.metadata';
    protected $tituloAdmin = 'Metadata';
    protected $tituloRegistrar = 'Registrar Metadata';
    protected $tituloModificar = 'Modificar Metadata';
    protected $tituloEliminar = 'Eliminar Metadata';
    protected $rutas = array('create' => 'metadata.create',
        'edit' => 'metadata.edit',
        'delete' => 'metadata.eliminar',
        'search' => 'metadata.buscar',
        'index' => 'metadata.index',
        'permisos' => 'metadata.obtenerpermisos',
    );

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $tipodocumento_id = $request->input('tipodocumento_id');
        $entidad = 'Metadata';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta', 'tipodocumento_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $tipodocumento_id = Libreria::getParam($request->input('tipodocumento_id'));
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Metadata';
        $metadata = null;
        $formData = array('metadata.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        $cboTipocontrol = ['text' => 'Caja de texto simple'] + ['textarea' => 'Caja de texto amplia'] + ['select' => 'Selección'] + ['radio' => 'Radio Button'] + ['checkbox' => 'Checkbox'];
        return view($this->folderview . '.mant')->with(compact('metadata', 'formData', 'entidad', 'boton', 'listar', 'tipodocumento_id', 'cboTipocontrol'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(),
            array(
                'nombre' => 'required|max:50|string',
                'tipocontrol' => 'required|in:text,textarea,select,radio,checkbox',
                'tipodocumento_id' => 'required|integer|exists:tipodocumento,id',
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request) {
            $metadata = new Metadata();
            $metadata->nombre = $request->input('nombre');
            $metadata->codigo = Libreria::limpiarCadena($request->input('nombre'));
            $metadata->tipocontrol = $request->input('tipocontrol');
            $metadata->tipodocumento_id = $request->input('tipodocumento_id');
            $metadata->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param Metadata $metadata
     * @return void
     */
    public function show(Metadata $metadata)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return bool|string
     */
    public function edit(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'metadata');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $metadata = Metadata::find($id);
        $tipodocumento_id = $metadata->tipodocumento_id;
        $entidad = 'Metadata';
        $formData = array('metadata.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $cboTipocontrol = ['text' => 'Caja de texto simple'] + ['textarea' => 'Caja de texto amplia'] + ['select' => 'Selección'] + ['radio' => 'Radio Button'] + ['checkbox' => 'Checkbox'];
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('metadata', 'formData', 'entidad', 'boton', 'listar', 'tipodocumento_id', 'cboTipocontrol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return string
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'metadata');
        if ($existe !== true) {
            return $existe;
        }
        $validacion = Validator::make($request->all(),
            array(
                'nombre' => 'required|max:45|string',
                'tipocontrol' => 'required|in:text,textarea,select,radio,checkbox',
                'tipodocumento_id' => 'required|integer|exists:tipodocumento,id',
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request, $id) {
            $metadata = Metadata::find($id);
            $metadata->nombre = $request->input('nombre');
            $metadata->codigo = Libreria::limpiarCadena($request->input('nombre'));
            $metadata->tipocontrol = $request->input('tipocontrol');
            $metadata->tipodocumento_id = $request->input('tipodocumento_id');
            $metadata->save();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return bool|string
     */
    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'metadata');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $metadata = Metadata::find($id);
            $metadata->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @param Request $request
     * @return Response
     */
    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Metadata';
        $nombre = Libreria::getParam($request->input('nombre'));
        $tipodocumento_id = Libreria::getParam($request->input('tipodocumento_id'));
        $resultado = Metadata::listar($nombre, $tipodocumento_id);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => 'Nombre', 'numero' => '1');
        $cabecera[] = array('valor' => 'Codigo', 'numero' => '1');
        $cabecera[] = array('valor' => 'Tipo de control', 'numero' => '1');
        $cabecera[] = array('valor' => 'Reglas', 'numero' => '1');
        $cabecera[] = array('valor' => 'Editar', 'numero' => '1');
        $cabecera[] = array('valor' => 'Eliminar', 'numero' => '1');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }


    /**
     * Función para confirmar la eliminación de un registrlo
     * @param integer $id id del registro a intentar eliminar
     * @param string $listarLuego consultar si luego de eliminar se listará
     * @return bool|string
     */
    public function eliminar($id, $listarLuego)
    {

        $existe = Libreria::verificarExistencia($id, 'metadata');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Metadata::find($id);
        $entidad = 'Metadata';
        $formData = array('route' => array('metadata.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        $mensaje = null;
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar', 'mensaje'));
    }
}
