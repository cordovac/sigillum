<?php

namespace App\Http\Controllers;

use App\Categoriamenu;
use App\Librerias\Libreria;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Validator;


class CategoriamenuController extends Controller
{
    protected $folderview = 'app.categoriamenu';
    protected $tituloAdmin = 'Categoría Menú';
    protected $tituloRegistrar = 'Registrar Categoría Menú';
    protected $tituloModificar = 'Modificar Categoría Menú';
    protected $tituloEliminar = 'Eliminar Categoría Menú';
    protected $rutas = array('create' => 'categoriamenu.create',
        'edit' => 'categoriamenu.edit',
        'delete' => 'categoriamenu.eliminar',
        'search' => 'categoriamenu.buscar',
        'index' => 'categoriamenu.index',
    );

    /**
     * CategoriamenuController constructor.
     */
    public function __construct()
    {
        $this->middleware('role:modal,delete_categoriamenu')->only('eliminar');
        $this->middleware('role:modal,add_categoriamenu')->only('create');
        $this->middleware('role:modal,change_categoriamenu')->only('edit');
        $this->middleware('role:nomodal,list_categoriamenu')->only('index');
        $this->middleware('role:fragmento,list_categoriamenu')->only('buscar');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $entidad = 'Categoriamenu';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Categoriamenu';
        $cboCategoriamenu = ['' => 'Seleccione una categoría'] + Categoriamenu::pluck('nombre', 'id')->all();
        $categoriamenu = null;
        $formData = array('categoriamenu.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('categoriamenu', 'formData', 'entidad', 'cboCategoriamenu', 'boton', 'listar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return string
     */
    public function store(Request $request)
    {
        $permisosnecesarios = array('add_categoriamenu');
        if (!Libreria::verificarExistenciaPermiso($permisosnecesarios)) {
            $error = array(
                'error401' => array(
                    'Error 401: No tiene permisos para realizar esta acción, consultar con el administrador del sistema'
                )
            );
            return response()->json($error);
        }
        $validacion = Validator::make($request->all(),
            array(
                'nombre' => 'required|string|unique:categoriamenu',
                'orden' => 'required|integer',
                'icono' => 'required|string',
                'categoriamenu_id' => 'nullable|integer|exists:categoriamenu,id,deleted_at,NULL',
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request) {
            $categoriamenu = new Categoriamenu();
            $categoriamenu->nombre = $request->input('nombre');
            $categoriamenu->orden = $request->input('orden');
            $categoriamenu->icono = $request->input('icono');
            $categoriamenu->categoriamenu_id = Libreria::obtenerParametro($request->input('categoriamenu_id'));
            $categoriamenu->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param Categoriamenu $categoriamenu
     * @return void
     */
    public function show(Categoriamenu $categoriamenu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @param Request $request
     * @return bool|string
     */
    public function edit($id, Request $request)
    {
        $existe = Libreria::verificarExistencia($id, 'categoriamenu');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $categoriamenu = Categoriamenu::find($id);
        $entidad = 'Categoriamenu';
        $cboCategoriamenu = ['' => 'Seleccione una categoría'] + Categoriamenu::where('id', '<>', $id)->pluck('nombre', 'id')->all();
        $formData = array('categoriamenu.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('categoriamenu', 'formData', 'entidad', 'cboCategoriamenu', 'boton', 'listar'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return string
     */
    public function update(Request $request, $id)
    {
        $permisosnecesarios = array('change_categoriamenu');
        if (!Libreria::verificarExistenciaPermiso($permisosnecesarios)) {
            $error = array(
                'error401' => array(
                    'Error 401: No tiene permisos para realizar esta acción, consultar con el administrador del sistema'
                )
            );
            return response()->json($error);
        }
        $existe = Libreria::verificarExistencia($id, 'categoriamenu');
        if ($existe !== true) {
            return $existe;
        }
        $validacion = Validator::make($request->all(),
            array(
                'nombre' => [
                    'required',
                    Rule::unique('categoriamenu')->ignore($id),
                ],
                'orden' => 'required|integer',
                'icono' => 'required|string',
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request, $id) {
            $categoriamenu = Categoriamenu::find($id);
            $categoriamenu->nombre = $request->input('nombre');
            $categoriamenu->orden = $request->input('orden');
            $categoriamenu->icono = $request->input('icono');
            $categoriamenu->categoriamenu_id = Libreria::obtenerParametro($request->input('categoriamenu_id'));
            $categoriamenu->save();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return bool|string
     */
    public function destroy($id)
    {
        $permisosnecesarios = array('delete_categoriamenu');
        if (!Libreria::verificarExistenciaPermiso($permisosnecesarios)) {
            $error = array(
                'error401' => array(
                    'Error 401: No tiene permisos para realizar esta acción, consultar con el administrador del sistema'
                )
            );
            return response()->json($error);
        }
        $existe = Libreria::verificarExistencia($id, 'categoriamenu');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $categoriamenu = Categoriamenu::find($id);
            $categoriamenu->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @param Request $request
     * @return Response
     */
    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Categoriamenu';
        $querysearch = Libreria::getParam($request->input('querysearch'));
        $resultado = Categoriamenu::listar($querysearch);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Nombre', 'numero' => '1');
        $cabecera[] = array('valor' => 'Orden', 'numero' => '1');
        $cabecera[] = array('valor' => 'Icono', 'numero' => '1');
        $cabecera[] = array('valor' => 'Categoría', 'numero' => '1');
        $cabecera[] = array('valor' => 'Editar', 'numero' => '1');
        $cabecera[] = array('valor' => 'Eliminar', 'numero' => '1');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;

        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }

    /**
     * Función para confirmar la eliminación de un registrlo
     * @param integer $id id del registro a intentar eliminar
     * @param string $listarLuego consultar si luego de eliminar se listará
     * @return bool|string
     */
    public function eliminar($id, $listarLuego)
    {
        $existe = Libreria::verificarExistencia($id, 'categoriamenu');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Categoriamenu::find($id);
        $entidad = 'Categoriamenu';
        $formData = array('route' => array('categoriamenu.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        $mensaje = null;
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar', 'mensaje'));
    }
}
