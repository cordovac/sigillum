<?php

namespace App\Http\Controllers;

use App\Archivo;
use App\Librerias\Libreria;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class ArchivoController extends Controller
{

    protected $folderview = 'app.archivo';
    protected $tituloAdmin = 'Archivo';
    protected $tituloRegistrar = 'Registrar Archivo';
    protected $tituloModificar = 'Modificar Archivo';
    protected $tituloEliminar = 'Eliminar Archivo';
    protected $rutas = array('create' => 'archivo.create',
        'edit' => 'archivo.edit',
        'delete' => 'archivo.eliminar',
        'search' => 'archivo.buscar',
        'index' => 'archivo.index',
        'permisos' => 'archivo.obtenerpermisos',
    );

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $historial_id = $request->input('historial_id');
        $entidad = 'Archivo';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta', 'historial_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $historial_id = Libreria::getParam($request->input('historial_id'));
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Archivo';
        $archivo = null;
        $formData = array('archivo.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        $cboTipocontrol = ['text' => 'Caja de texto simple'] + ['textarea' => 'Caja de texto amplia'] + ['select' => 'Selección'] + ['radio' => 'Radio Button'] + ['checkbox' => 'Checkbox'];
        return view($this->folderview . '.mant')->with(compact('archivo', 'formData', 'entidad', 'boton', 'listar', 'historial_id', 'cboTipocontrol'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(),
            array(
                'nombre' => 'required|max:50|string',
                'tipocontrol' => 'required|in:text,textarea,select,radio,checkbox',
                'historial_id' => 'required|integer|exists:tipodocumento,id',
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request) {
            $archivo = new Archivo();
            $archivo->nombre = $request->input('nombre');
            $archivo->codigo = Libreria::limpiarCadena($request->input('nombre'));
            $archivo->tipocontrol = $request->input('tipocontrol');
            $archivo->historial_id = $request->input('historial_id');
            $archivo->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param Archivo $archivo
     * @return void
     */
    public function show(Archivo $archivo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return bool|string
     */
    public function edit(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'archivo');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $archivo = Archivo::find($id);
        $historial_id = $archivo->historial_id;
        $entidad = 'Archivo';
        $formData = array('archivo.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $cboTipocontrol = ['text' => 'Caja de texto simple'] + ['textarea' => 'Caja de texto amplia'] + ['select' => 'Selección'] + ['radio' => 'Radio Button'] + ['checkbox' => 'Checkbox'];
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('archivo', 'formData', 'entidad', 'boton', 'listar', 'historial_id', 'cboTipocontrol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return string
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'archivo');
        if ($existe !== true) {
            return $existe;
        }
        $validacion = Validator::make($request->all(),
            array(
                'nombre' => 'required|max:45|string',
                'tipocontrol' => 'required|in:text,textarea,select,radio,checkbox',
                'historial_id' => 'required|integer|exists:tipodocumento,id',
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request, $id) {
            $archivo = Archivo::find($id);
            $archivo->nombre = $request->input('nombre');
            $archivo->codigo = Libreria::limpiarCadena($request->input('nombre'));
            $archivo->tipocontrol = $request->input('tipocontrol');
            $archivo->historial_id = $request->input('historial_id');
            $archivo->save();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return bool|string
     */
    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'archivo');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $archivo = Archivo::find($id);
            $archivo->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @param Request $request
     * @return Response
     */
    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Archivo';
        $nombre = Libreria::getParam($request->input('nombre'));
        $historial_id = Libreria::getParam($request->input('historial_id'));
        $resultado = Archivo::listar($nombre, $historial_id);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => 'Nombre', 'numero' => '1');
        $cabecera[] = array('valor' => 'Codigo', 'numero' => '1');
        $cabecera[] = array('valor' => 'Tipo de control', 'numero' => '1');
        $cabecera[] = array('valor' => 'Reglas', 'numero' => '1');
        $cabecera[] = array('valor' => 'Editar', 'numero' => '1');
        $cabecera[] = array('valor' => 'Eliminar', 'numero' => '1');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }


    /**
     * Función para confirmar la eliminación de un registrlo
     * @param integer $id id del registro a intentar eliminar
     * @param string $listarLuego consultar si luego de eliminar se listará
     * @return bool|string
     */
    public function eliminar($id, $listarLuego)
    {

        $existe = Libreria::verificarExistencia($id, 'archivo');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Archivo::find($id);
        $entidad = 'Archivo';
        $formData = array('route' => array('archivo.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        $mensaje = null;
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar', 'mensaje'));
    }

    public function mostrararchivo($idarchivo)
    {
        $archivo = Archivo::find($idarchivo);
        return response()->download(storage_path('app/' . $archivo->ruta));
    }
}
