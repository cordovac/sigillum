<?php

namespace App\Http\Controllers;

use App\Flujotrabajo;
use App\Tipousuario;
use App\Tipousuariousuario;
use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $tipousuario = Tipousuariousuario::where('usuario_id', Auth::user()->id)->first();
        $tipousuario_id = $tipousuario->tipousuario_id;
        $permisos = [];
        $permisosasignados = array();
        if ($tipousuario_id != 0 || $tipousuario_id !== null) {
            $permisos = Tipousuario::find($tipousuario_id)->permisos()->get();
        }
        foreach ($permisos as $key => $value) {
            $permisosasignados[] = $value->codigo;
        }

        $menu = $this->arbolpermisos($tipousuario_id, $permisosasignados);
        return view('dashboard.home')->with(compact("menu"));
    }

    /**
     * @return Factory|View
     */
    public function emisor()
    {
        $usuario_id = Auth::user()->id;
        $usuario = User::find($usuario_id);
        $entidad = 'Emisor';
        $cboFlujotrabajo = ['' => 'Seleccione'] + Flujotrabajo::join('empresa_flujotrabajo', 'flujotrabajo.id', 'empresa_flujotrabajo.flujotrabajo_id')
                ->where('empresa_flujotrabajo.empresa_id', $usuario->representante->empresa_id)
                ->whereNull('empresa_flujotrabajo.deleted_at')
                ->whereNull('flujotrabajo.deleted_at')
                ->orderBy('flujotrabajo.descripcion')
                ->select('flujotrabajo.descripcion', 'flujotrabajo.id')
                ->pluck('descripcion', 'id')
                ->all();
        return view('dashboard.emisor')->with(compact('cboFlujotrabajo', 'entidad'));
    }

    public function receptor()
    {
        $usuario_id = Auth::user()->id;
        $usuario = User::find($usuario_id);
        $entidad = 'Receptor';
        $cboFlujotrabajo = ['' => 'Seleccione'] + Flujotrabajo::join('empresa_flujotrabajo', 'flujotrabajo.id', 'empresa_flujotrabajo.flujotrabajo_id')
                ->where('empresa_flujotrabajo.empresa_id', $usuario->representante->empresa_id)
                ->whereNull('empresa_flujotrabajo.deleted_at')
                ->whereNull('flujotrabajo.deleted_at')
                ->orderBy('flujotrabajo.descripcion')
                ->select('flujotrabajo.descripcion', 'flujotrabajo.id')
                ->pluck('descripcion', 'id')
                ->all();
        return view('dashboard.receptor')->with(compact('cboFlujotrabajo', 'entidad'));
    }

    /**
     *método para mostrar el árbol de los permisos
     * @param $tipousuario_id
     * @param $permisosasignados
     * @return string
     */

    public function arbolpermisos($tipousuario_id, $permisosasignados)
    {
        $cadenacategoria = '';
        $catprincipales = DB::table('categoriamenu')->whereNull('categoriamenu_id')->whereNull('deleted_at')->orderBy('orden', 'ASC')->get();
        foreach ($catprincipales as $catprincipal) {
            $subcategoria = $this->buscarsubcategorias($catprincipal->id, $tipousuario_id, $permisosasignados);
            $usar = false;
            $aux = array();
            $opcionesmenu = DB::table('opcionmenu')->where('categoriamenu_id', '=', $catprincipal->id)->whereNull('deleted_at')->orderBy('orden', 'ASC')->get();

            if ($opcionesmenu->count() > 0) {
                foreach ($opcionesmenu as $opcionmenu) {
                    $permisos = DB::table('permiso')->where('opcionmenu_id', '=', $opcionmenu->id)->whereNull('deleted_at')->get();
                    foreach ($permisos as $permiso) {
                        $permiso_codigo = explode('_', $permiso->codigo);

                        if ($permiso_codigo[0] == 'list' && in_array($permiso->codigo, $permisosasignados)) {
                            $usar = true;
                            $aux[] = array(
                                'nombre' => $opcionmenu->nombre,
                                'link' => $opcionmenu->link,
                                'icono' => $opcionmenu->icono
                            );
                        }
                    }

                }
            }

            if ($subcategoria != '' || $usar) {
                $cadenacategoria .= '<li class="nav-item nav-item-submenu"><a href="" class="nav-link"><i class="' . $catprincipal->icono . '"></i><span>' . $catprincipal->nombre . '</span></a>';
                $cadenacategoria .= '<ul class="nav nav-group-sub" data-submenu-title="Starter kit">';
                foreach ($aux as $key => $value) {
                    $cadenacategoria .= '<li class="nav-item"><a href="javascript:void(0);" class="nav-link" onclick="cargarRuta(\'' . route($value['link']) . '\', \'content\');"><i class="' . $value['icono'] . '"></i>' . $value['nombre'] . '</a></li>';
                }
                if ($subcategoria != '') {
                    $cadenacategoria .= $subcategoria;
                }
                $cadenacategoria .= '</ul></li>';
            }
        }
        return $cadenacategoria;
    }

    /**
     *método para mostrar el árbol de los permisos
     * @param $categoria_id
     * @param $tipousuario_id
     * @param $permisosasignados
     * @return string
     */

    public function buscarsubcategorias($categoria_id, $tipousuario_id, $permisosasignados)
    {
        $cadenasubcategoria = '';
        $subcategorias = DB::table('categoriamenu')->where('categoriamenu_id', '=', $categoria_id)->whereNull('deleted_at')->orderBy('orden', 'ASC')->get();
        foreach ($subcategorias as $subcategoria) {
            $usar = false;
            $aux = array();
            $subcats = $this->buscarsubcategorias($subcategoria->id, $tipousuario_id, $permisosasignados);
            $opcionesmenu = DB::table('opcionmenu')->where('categoriamenu_id', '=', $subcategoria->id)->whereNull('deleted_at')->orderBy('orden', 'ASC')->get();
            if ($opcionesmenu->count()) {
                foreach ($opcionesmenu as $opcionmenu) {
                    $permisos = DB::table('permiso')->where('opcionmenu_id', '=', $opcionmenu->id)->whereNull('deleted_at')->get();
                    foreach ($permisos as $permiso) {
                        $permiso_codigo = (explode('_', $permiso->codigo))[0];
                        if ($permiso_codigo == 'list' && in_array($permiso->codigo, $permisosasignados)) {
                            $usar = true;
                            $aux[] = array(
                                'nombre' => $opcionmenu->nombre,
                                'link' => $opcionmenu->link,
                                'icono' => $opcionmenu->icono
                            );
                        }
                    }
                }
            }
            if ($subcats != '' || $usar) {
                $cadenasubcategoria .= '<li class="nav-item nav-item-submenu"><a href="" class="nav-link"><i class="' . $subcategoria->icono . '"></i><span>' . $subcategoria->nombre . '</span></a>';
                $cadenasubcategoria .= '<ul class="nav nav-group-sub" data-submenu-title="Starter kit">';
                foreach ($aux as $key => $value) {
                    $cadenasubcategoria .= '<li class="nav-item"><a href="javascript:void(0);" class="nav-link" onclick="cargarRuta(\'' . route($value['link']) . '\', \'content\');"><i class="' . $value['icono'] . '"></i>' . $value['nombre'] . '</a></li>';
                }
                if ($subcats != '') {
                    $cadenasubcategoria .= $subcats;
                }
                $cadenasubcategoria .= '</ul></li>';
            }
        }

        return $cadenasubcategoria;
    }

    /**
     * @return Factory|View
     */
    public function main()
    {
        return view('dashboard.main');
    }

    /**
     * Function para retornar mensaje que no tiene permiso
     * @param $tipo
     * @return Factory|View
     */
    public function noautorizado($tipo)
    {
        if ($tipo === 'nomodal') {
            return view('dashboard.401');
        } elseif ($tipo === 'modal') {
            return view('app.401');
        } elseif ($tipo === 'fragmento') {
            return view('app.401-fragmento');
        }
    }

}
