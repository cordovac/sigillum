<?php

namespace App\Http\Controllers;

use App\Archivo;
use App\Detalleflujotrabajo;
use App\Detalleflujotrabajousuario;
use App\Documento;
use App\Empresa;
use App\Historial;
use App\Librerias\Libreria;
use App\Representante;
use App\Respuesta;
use App\Respuestadetalleflujo;
use DateTime;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;


class HistorialController extends Controller
{

    protected $folderview = 'app.historial';
    protected $tituloAdmin = 'Historial';
    protected $tituloRegistrar = 'Registrar Historial';
    protected $tituloModificar = 'Modificar Historial';
    protected $tituloEliminar = 'Eliminar Historial';
    protected $rutas = array('create' => 'historial.create',
        'edit' => 'historial.edit',
        'delete' => 'historial.eliminar',
        'search' => 'historial.buscar',
        'index' => 'historial.index',
        'permisos' => 'historial.obtenerpermisos',
    );

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $tipodocumento_id = $request->input('tipodocumento_id');
        $entidad = 'Historial';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta', 'tipodocumento_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $tipodocumento_id = Libreria::getParam($request->input('tipodocumento_id'));
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Historial';
        $historial = null;
        $formData = array('historial.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        $cboRespuesta = ['' => 'Seleccione'] + Respuesta::pluck('descripcion', 'id')->all();
        $cboEmpresa = ['' => 'Seleccione'] + Empresa::pluck('razonsocial', 'id')->all();
        $cboRepresentante = ['' => 'Seleccione'] + ['' => 'Seleccione'] + Representante::select(DB::raw('CONCAT(apellidopaterno, " ", apellidomaterno, ", ", nombres) as descripcion'), 'id')->pluck('descripcion', 'id')->all();
        return view($this->folderview . '.mant')->with(compact('historial', 'formData', 'entidad', 'boton', 'listar', 'tipodocumento_id', 'cboRespuesta', 'cboRepresentante', 'cboEmpresa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(),
            array(
                'nombre' => 'required|max:50|string',
                'codigo' => 'required|max:50|string',
                'tipocontrol' => 'required|in:text,textarea,select,radio,checkbox',
                'tipodocumento_id' => 'required|integer|exists:tipodocumento,id',
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request) {
            $historial = new Historial();
            $historial->nombre = $request->input('nombre');
            $historial->codigo = $request->input('codigo');
            $historial->tipocontrol = $request->input('tipocontrol');
            $historial->tipodocumento_id = $request->input('tipodocumento_id');
            $historial->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param Historial $historial
     * @return void
     */
    public function show(Historial $historial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return bool|string
     */
    public function edit(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'historial');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $historial = Historial::find($id);
        $entidad = 'Historial';
        $formData = array('historial.update', $id);
        $formData = array('route' => $formData, 'enctype' => "multipart/form-data", 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $cboRespuestadetalleflujo = ['' => 'Seleccione'] + Respuestadetalleflujo::where('detalleflujotrabajo_id', $historial->detalleflujotrabajo_id)->pluck('descripcion', 'id')->all();
        $archivos = Archivo::join('historial', 'archivo.historial_id', 'historial.id')
            ->join('documento', 'historial.documento_id', 'documento.id')
            ->whereNull('historial.deleted_at')
            ->whereNull('documento.deleted_at')
            ->where('historial.id', $historial->historial_id)
            ->where('archivo.activo', 1)
            ->select('archivo.*')->get();
        $listado = array();
        foreach ($archivos as $key => $value) {
            $listado[] = array(
                'id' => $value->id,
                'nombre' => $value->nombre
            );
        }
        $boton = 'Modificar';
        Session::put('archivos', $listado);
        $editararchivos = $historial->detalleflujotrabajo->editararchivos;
        return view($this->folderview . '.mant')->with(compact('historial', 'formData', 'entidad', 'boton', 'listar', 'cboRespuestadetalleflujo', 'archivos', 'editararchivos'));
    }

    /**
     * @return Factory|View
     */
    public function verarchivos()
    {
        return view($this->folderview . '.archivos');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return string
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'historial');
        if ($existe !== true) {
            return $existe;
        }
        $validacion = Validator::make($request->all(),
            array(
                'respuestadetalleflujo_id' => 'required|integer|exists:respuesta_detalleflujo,id',
                'descripcion' => 'nullable|string'
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }

        // verificamos que en caso se eliga editar un archivo, se haya seleccionado uno nuevo
        $archivos = Session::get('archivos');
        for ($i = 0; $i < count($archivos); $i++) {
            $idarchivo = $archivos[$i]['id'];
            $editar = Libreria::getParam($request->input('editar-' . $idarchivo), 0);
            if ($editar === '1') {
                $mensajes = array(
                    'archivo-' . $idarchivo . '.required' => 'Se ha seleccionado EDITAR un archivo, pero no se ha seleccionado el archivo nuevo',

                );
                $validacion = Validator::make($request->all(),
                    array(
                        'archivo-' . $idarchivo => 'required'
                    ), $mensajes
                );
                if ($validacion->fails()) {
                    return response()->json($validacion->messages());
                }
            }
        }
        $error = DB::transaction(function () use ($request, $id) {
            $ahora = new DateTime;
            $historial = Historial::find($id);
            $idhistorialpadre = $historial->id;
            // verificamos a que tarea corresponde
            $historial->fechaoperacion = $ahora;
            $historial->observacion = Libreria::getParam($request->input('observacion'));
            $historial->respuestadetalleflujo_id = $request->input('respuestadetalleflujo_id');
            $historial->usuario_id = Auth::user()->id;
            $historial->pendiente = 0;
            $historial->save();

            // retrasamos el registro 1 segundo
            sleep(1);

            // creamos carpeta para el historial
            $carpetahistorial = 'archivos' . DIRECTORY_SEPARATOR . $historial->documento_id . DIRECTORY_SEPARATOR .
                $historial->id;
            Storage::makeDirectory($carpetahistorial);

            // guardamos archivos nuevos
            if ($request->archivo) {
                foreach ($request->archivo as $temporal) {
                    $url = 'archivos' . DIRECTORY_SEPARATOR . $historial->documento_id . DIRECTORY_SEPARATOR . $historial->id;
                    $nombre = $temporal->storeAs($url, $temporal->getClientOriginalName());
                    $archivonuevo = new Archivo();
                    $archivonuevo->ruta = $nombre;
                    $archivonuevo->nombre = $temporal->getClientOriginalName();
                    $archivonuevo->historial_id = $historial->id;
                    $archivonuevo->save();
                }
            }

            // verificamos que acciones se han elegido
            $archivos = Session::get('archivos');
            for ($i = 0; $i < count($archivos); $i++) {
                $idarchivo = $archivos[$i]['id'];
                $editar = Libreria::getParam($request->input('editar-' . $idarchivo), 0);
                $eliminar = Libreria::getParam($request->input('eliminar-' . $idarchivo), 0);
                $archivomodificar = $request->{'archivo-' . $idarchivo};
                if ($editar === '1') {
                    $original = Archivo::find($idarchivo);
                    $url = 'archivos' . DIRECTORY_SEPARATOR . $historial->documento_id . DIRECTORY_SEPARATOR . $historial->id;
                    $ruta = $archivomodificar->storeAs($url, $archivomodificar->getClientOriginalName());
                    $archivoactualizado = new Archivo();
                    $archivoactualizado->ruta = $ruta;
                    $archivoactualizado->nombre = $archivomodificar->getClientOriginalName();
                    $archivoactualizado->historial_id = $historial->id;
                    $archivoactualizado->archivo_id = $original->id;
                    $archivoactualizado->accion = 'U';
                    $archivoactualizado->save();
                }
                if ($eliminar === '1') {
                    $original = Archivo::find($idarchivo);
                    $baseruta = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR;
                    $origen = $original->ruta;
                    $destino = 'archivos' .
                        DIRECTORY_SEPARATOR . $historial->documento_id . DIRECTORY_SEPARATOR .
                        $historial->id . DIRECTORY_SEPARATOR . $original->nombre;
                    File::copy($baseruta . $origen, $baseruta . $destino);
                    // nuevo archivo copiado
                    $copia = new Archivo();
                    $copia->ruta = $destino;
                    $copia->nombre = $original->nombre;
                    $copia->historial_id = $historial->id;
                    $copia->archivo_id = $original->id;
                    $copia->accion = 'D';
                    $copia->activo = false;
                    $copia->save();
                }
                if ($editar === 0 && $eliminar === 0) {
                    $original = Archivo::find($idarchivo);
                    $baseruta = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR;
                    $origen = $original->ruta;
                    $destino = 'archivos' . DIRECTORY_SEPARATOR . $historial->documento_id . DIRECTORY_SEPARATOR .
                        $historial->id . DIRECTORY_SEPARATOR . $original->nombre;
                    File::copy($baseruta . $origen, $baseruta . $destino);
                    // nuevo archivo copiado
                    $copia = new Archivo();
                    $copia->ruta = $destino;
                    $copia->nombre = $original->nombre;
                    $copia->historial_id = $historial->id;
                    $copia->archivo_id = $original->id;
                    $copia->accion = 'M';
                    $copia->save();
                }
            }

            Session::remove('archivos');
            $documento_id = $historial->documento_id;
            $respuestadetalle = Respuestadetalleflujo::find($historial->respuestadetalleflujo_id);
            $secuenciaactual = $historial->detalleflujotrabajo->secuencia;
            $flujotrabajo_id = $historial->detalleflujotrabajo->flujotrabajo_id;
            switch ($respuestadetalle->respuesta->accion) {
                case 'siguiente':
                    $detalleflujo = Detalleflujotrabajo::join('tarea', 'detalle_flujotrabajo.tarea_id', 'tarea.id')
                        ->where('tarea.editable', 1)
                        ->where('detalle_flujotrabajo.flujotrabajo_id', $flujotrabajo_id)
                        ->where('detalle_flujotrabajo.secuencia', '>', $secuenciaactual)
                        ->whereNull('tarea.deleted_at')
                        ->orderBy('detalle_flujotrabajo.secuencia', 'ASC')
                        ->select('detalle_flujotrabajo.*')
                        ->first();
                    if (!is_null($detalleflujo)) {
                        $historial = new Historial();
                        $historial->documento_id = $documento_id;
                        $historial->usuarioemisor_id = Auth::user()->id;
                        $historial->detalleflujotrabajo_id = $detalleflujo->id;
                        $historial->historial_id = $idhistorialpadre;
                        $historial->save();
                    } else {
                        $detalleflujo = Detalleflujotrabajo::fin($flujotrabajo_id);
                        if (!is_null($detalleflujo)) {
                            $fin = new Historial();
                            $fin->fechaoperacion = $ahora;
                            $fin->documento_id = $documento_id;
                            $fin->usuarioemisor_id = Auth::user()->id;
                            $fin->usuario_id = Auth::user()->id;
                            $fin->detalleflujotrabajo_id = $detalleflujo->id;
                            $fin->pendiente = false;
                            $historial->historial_id = $idhistorialpadre;
                            $fin->save();
                        }
                    }
                    break;
                case 'anterior':
                    $detalleflujo = Detalleflujotrabajo::join('tarea', 'detalle_flujotrabajo.tarea_id', 'tarea.id')
                        ->where('tarea.editable', 1)
                        ->whereNull('tarea.deleted_at')
                        ->where('detalle_flujotrabajo.flujotrabajo_id', $flujotrabajo_id)
                        ->where('detalle_flujotrabajo.secuencia', '<', $secuenciaactual)
                        ->select('detalle_flujotrabajo.*')
                        ->orderBy('detalle_flujotrabajo.secuencia', 'DESC')->first();
                    if (!is_null($detalleflujo)) {
                        $historial = new Historial();
                        $historial->documento_id = $documento_id;
                        $historial->usuarioemisor_id = Auth::user()->id;
                        $historial->detalleflujotrabajo_id = $detalleflujo->id;
                        $historial->historial_id = $idhistorialpadre;
                        $historial->save();
                    }
                    break;
                case 'inicio':
                    $detalleflujo = Detalleflujotrabajo::detalleporflujotrabajo($flujotrabajo_id)->orderBy('detalle_flujotrabajo.secuencia', 'ASC')->first();
                    if (!is_null($detalleflujo)) {
                        $inicio = new Historial();
                        $inicio->fechaoperacion = $ahora;
                        $inicio->documento_id = $documento_id;
                        $inicio->usuarioemisor_id = Auth::user()->id;
                        $inicio->pendiente = false;
                        $inicio->detalleflujotrabajo_id = $detalleflujo->id;
                        $historial->historial_id = $idhistorialpadre;
                        $inicio->save();
                    }
                    break;
                case 'fin':
                    $detalleflujo = Detalleflujotrabajo::fin($flujotrabajo_id);
                    if (!is_null($detalleflujo)) {
                        $fin = new Historial();
                        $fin->fechaoperacion = $ahora;
                        $fin->documento_id = $documento_id;
                        $fin->usuarioemisor_id = Auth::user()->id;
                        $fin->usuario_id = Auth::user()->id;
                        $fin->detalleflujotrabajo_id = $detalleflujo->id;
                        $fin->pendiente = false;
                        $historial->historial_id = $idhistorialpadre;
                        $fin->save();
                    }
                    break;
                case 'nodo':
                    $detalleflujo = Detalleflujotrabajo::nodo($respuestadetalle->secuencia, $flujotrabajo_id);
                    if (!is_null($detalleflujo)) {
                        $historial = new Historial();
                        $historial->documento_id = $documento_id;
                        $historial->usuarioemisor_id = Auth::user()->id;
                        $historial->detalleflujotrabajo_id = $detalleflujo->id;
                        $historial->historial_id = $idhistorialpadre;
                        $historial->save();
                    }
                    break;
            }
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return bool|string
     */
    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'historial');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $historial = Historial::find($id);
            $historial->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @param Request $request
     * @return Response
     */
    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Historial';
        $nombre = Libreria::getParam($request->input('nombre'));
        $tipodocumento_id = Libreria::getParam($request->input('tipodocumento_id'));
        $resultado = Historial::listar($nombre, $tipodocumento_id);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => 'Nombre', 'numero' => '1');
        $cabecera[] = array('valor' => 'Codigo', 'numero' => '1');
        $cabecera[] = array('valor' => 'Tipo de control', 'numero' => '1');
        $cabecera[] = array('valor' => 'Reglas', 'numero' => '1');
        $cabecera[] = array('valor' => 'Editar', 'numero' => '1');
        $cabecera[] = array('valor' => 'Eliminar', 'numero' => '1');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }


    /**
     * Función para confirmar la eliminación de un registrlo
     * @param integer $id id del registro a intentar eliminar
     * @param string $listarLuego consultar si luego de eliminar se listará
     * @return bool|string
     */
    public function eliminar($id, $listarLuego)
    {
        $existe = Libreria::verificarExistencia($id, 'historial');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Historial::find($id);
        $entidad = 'Historial';
        $formData = array('route' => array('historial.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        $mensaje = null;
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar', 'mensaje'));
    }

    public function historialdocumento($documento_id)
    {
        $listar = "NO";
        $historial = Historial::join('detalle_flujotrabajo', 'historial.detalleflujotrabajo_id', 'detalle_flujotrabajo.id')
            ->join('tarea', 'detalle_flujotrabajo.tarea_id', 'tarea.id')
            ->where('historial.pendiente', 0)
            ->where('historial.documento_id', $documento_id)
            ->orderBy('historial.fechaoperacion', 'ASC')
            ->select('historial.*')->get();

        $historialpendiente = Historial::join('detalle_flujotrabajo', 'historial.detalleflujotrabajo_id', 'detalle_flujotrabajo.id')
            ->join('tarea', 'detalle_flujotrabajo.tarea_id', 'tarea.id')
            ->where('historial.pendiente', 1)
            ->where('historial.documento_id', $documento_id)
            ->orderBy('historial.created_at', 'ASC')
            ->select('historial.*')->get();
        $entidad = 'Historial';
        $formData = array('route' => array('historial.destroy', $documento_id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.documento.historial')->with(compact('historial', 'formData', 'entidad', 'boton', 'listar', 'historialpendiente'));
    }
}
