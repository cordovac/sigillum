<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Http\Requests;
use App\Librerias\Libreria;
use App\Permiso;
use App\Permisotipousuario;
use App\Tipousuario;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\View\View;
use Validator;


class TipousuarioController extends Controller
{

    protected $folderview = 'app.tipousuario';
    protected $tituloAdmin = 'Tipos de Usuario';
    protected $tituloRegistrar = 'Registrar Tipo de Usuario';
    protected $tituloModificar = 'Modificar Tipo de Usuario';
    protected $tituloEliminar = 'Eliminar Tipo de Usuario';
    protected $rutas = array('create' => 'tipousuario.create',
        'edit' => 'tipousuario.edit',
        'delete' => 'tipousuario.eliminar',
        'search' => 'tipousuario.buscar',
        'index' => 'tipousuario.index',
        'permisos' => 'tipousuario.obtenerpermisos',
    );

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $entidad = 'Tipousuario';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        $superusuario = $this->issuperusuario();
        $cboEmpresa = ['' => 'Todas'] + Empresa::pluck('razonsocial', 'id')->all();
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta', 'superusuario', 'cboEmpresa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Tipousuario';
        $tipousuario = null;
        $formData = array('tipousuario.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        $cboEmpresa = ['' => 'Seleccione'] + Empresa::pluck('razonsocial', 'id')->all();
        $superusuario = Auth::user()->superusuario;
        return view($this->folderview . '.mant')->with(compact('tipousuario', 'formData', 'entidad', 'boton', 'listar', 'cboEmpresa', 'superusuario'));
    }

    protected function issuperusuario()
    {
        return Auth::user()->superusuario;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $reglas = array(
            'nombre' => 'required|string|unique:tipousuario',
        );
        if ($this->issuperusuario()) {
            $reglas = array(
                'nombre' => 'required|string',
                'empresa_id' => 'nullable|integer|exists:empresa,id',
            );
        }
        $validacion = Validator::make($request->all(), $reglas);
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request) {
            $tipousuario = new Tipousuario();
            $tipousuario->nombre = $request->input('nombre');
            if ($this->issuperusuario()) {
                $tipousuario->empresa_id = Libreria::getParam($request->input('empresa_id'));
            } else {
                $tipousuario->empresa_id = Auth::user()->representante->empresa_id;
            }
            $tipousuario->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param Tipousuario $tipousuario
     * @return Response
     */
    public function show(Tipousuario $tipousuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return bool|string
     */
    public function edit(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'tipousuario');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $tipousuario = Tipousuario::find($id);
        $entidad = 'Tipousuario';
        $formData = array('tipousuario.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        $cboEmpresa = ['' => 'Seleccione'] + Empresa::pluck('razonsocial', 'id')->all();
        $superusuario = Auth::user()->superusuario;
        return view($this->folderview . '.mant')->with(compact('tipousuario', 'formData', 'entidad', 'boton', 'listar', 'cboEmpresa', 'superusuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return bool|string
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'tipousuario');
        if ($existe !== true) {
            return $existe;
        }
        $validacion = Validator::make($request->all(),
            array(
                'nombre' => [
                    'required',
                    Rule::unique('tipousuario')->ignore($id),
                ],
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request, $id) {
            $tipousuario = Tipousuario::find($id);
            $tipousuario->nombre = $request->input('nombre');
            $tipousuario->save();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Tipousuario $tipousuario
     * @return Response
     */
    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'tipousuario');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $tipousuario = Tipousuario::find($id);
            $tipousuario->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @param Request $request
     * @return Response
     */
    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Tipousuario';
        $nombre = Libreria::getParam($request->input('nombre'));
        $superusuario = $this->issuperusuario();
        $empresa_id = Libreria::getParam($request->input('empresa_id'));
        if (!$superusuario) {
            $empresa_id = Auth::user()->representante->empresa_id;
        }
        $resultado = Tipousuario::listar($nombre, $empresa_id);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Nombre', 'numero' => '1');
        if ($superusuario) {
            $cabecera[] = array('valor' => 'Empresa', 'numero' => '1');
        }
        $cabecera[] = array('valor' => 'Permisos', 'numero' => '1');
        $cabecera[] = array('valor' => 'Editar', 'numero' => '1');
        $cabecera[] = array('valor' => 'Eliminar', 'numero' => '1');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta', 'superusuario'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }


    /**
     * Función para confirmar la eliminación de un registrlo
     * @param integer $id id del registro a intentar eliminar
     * @param string $listarLuego consultar si luego de eliminar se listará
     * @return html              se retorna html, con la ventana de confirmar eliminar
     */
    public function eliminar($id, $listarLuego)
    {

        $existe = Libreria::verificarExistencia($id, 'tipousuario');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Tipousuario::find($id);
        $entidad = 'Tipousuario';
        $formData = array('route' => array('tipousuario.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        $mensaje = null;
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar', 'mensaje'));
    }

    /**
     * método para cargar view para asignación de permisos
     * @param $listarParam
     * @param $id
     * @return bool|Factory|View|string [type]              [description]
     */
    public function obtenerpermisos($listarParam, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'tipousuario');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        $entidad = 'Tipousuario';
        if (isset($listarParam)) {
            $listar = $listarParam;
        }
        $tipousuario = Tipousuario::find($id);

        /*Permisos*/
        $permisosasignados = array();
        $permisos = Tipousuario::find($tipousuario->id)->permisos()->get();
        foreach ($permisos as $key => $value) {
            $permisosasignados[] = $value->id;
        }

        $vista = $this->arbolpermisos($tipousuario->id, $permisosasignados);
        return view($this->folderview . '.permisos')->with(compact('tipousuario', 'listar', 'entidad', 'vista'));
    }

    /**
     * método para guardar asignación de permisos
     * @param $tipousuario_id
     * @param $permiso_id
     * @param $accion
     * @return bool|mixed|string [type]     [description]
     */
    public function guardarpermisos($tipousuario_id, $permiso_id, $accion)
    {
        $existetipousuario = Libreria::verificarExistencia($tipousuario_id, 'tipousuario');
        $existepermiso = Libreria::verificarExistencia($permiso_id, 'permiso');
        if ($existetipousuario !== true) {
            return $existetipousuario;
        }
        if ($existepermiso !== true) {
            return $existepermiso;
        }
        if ($accion == 'true') {
            $error = DB::transaction(function () use ($permiso_id, $tipousuario_id) {
                $permisotipousuario = new Permisotipousuario();
                $permisotipousuario->permiso_id = $permiso_id;
                $permisotipousuario->tipousuario_id = $tipousuario_id;
                $permisotipousuario->save();
            });
        } else {
            $error = DB::transaction(function () use ($permiso_id, $tipousuario_id) {
                $permisotipousuario = Permisotipousuario::Where('permiso_id', $permiso_id)->Where('tipousuario_id', $tipousuario_id)->firstOrFail();;
                $permisotipousuario->delete();
            });
        }

        return is_null($error) ? "OK" : $error;


    }


    /**
     *método para mostrar el árbol de los permisos
     * @param $tipousuario_id
     * @param $permisosasignados
     * @return string de permisos
     */

    public function arbolpermisos($tipousuario_id, $permisosasignados)
    {
        $superusuario = Auth::user()->superusuario;
        $entidad = 'Tipousuario';
        $cadenacategoria = '<ul style="list-style: none">';
        $catprincipales = DB::table('categoriamenu')->whereNull('categoriamenu_id')->whereNull('deleted_at')->orderBy('orden', 'ASC')->get();
        foreach ($catprincipales as $catprincipal) {
            $subcategoria = $this->buscarsubcategorias($catprincipal->id, $tipousuario_id, $permisosasignados, $superusuario);
            $cadenacategoria .= '<li><i class="fa ' . $catprincipal->icono . '">&nbsp;</i>' . $catprincipal->nombre;
            if ($subcategoria != '') {
                $cadenacategoria .= $subcategoria;
            }
            $opcionesmenu = DB::table('opcionmenu')->where('categoriamenu_id', '=', $catprincipal->id)->whereNull('deleted_at')->orderBy('orden', 'ASC')->get();
            foreach ($opcionesmenu as $opcionmenu) {
                $permisos = Permiso::listar($opcionmenu->id, $superusuario)->get();
                $cadenacategoria .= '<ul style="list-style: none"><li><i class="fa ' . $opcionmenu->icono . '">&nbsp;</i>' . $opcionmenu->nombre . '<ul style="list-style: none">';
                foreach ($permisos as $permiso) {
                    if (in_array($permiso->id, $permisosasignados)) {
                        $cadenacategoria .= '<li><label for="' . $permiso->id . '">' . $permiso->nombre .
                            '</label>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="' . $permiso->id . '" id="' . $permiso->id .
                            '" checked onchange="guardarpermiso(\'' . $entidad . '\',\'' . $permiso->id . '\',this)"></li>';
                    } else {
                        $cadenacategoria .= '<li><label for="' . $permiso->id . '">' . $permiso->nombre .
                            '</label>&nbsp;&nbsp;&nbsp;<input type="checkbox"  name="' . $permiso->id . '" id="' . $permiso->id .
                            '"onchange="guardarpermiso(\'' . $entidad . '\',\'' . $permiso->id . '\',this)"></li>';
                    }
                }
                $cadenacategoria .= '</ul></li></ul>';

            }
            $cadenacategoria .= '</li>';

        }
        return $cadenacategoria . '</ul>';
    }

    /**
     * @param $categoria_id
     * @param $tipousuario_id
     * @param $permisosasignados
     * @param $superusuario
     * @return string
     */
    public function buscarsubcategorias($categoria_id, $tipousuario_id, $permisosasignados, $superusuario)
    {
        $entidad = 'Tipousuario';
        $cadenasubcategoria = '';
        $subcategorias = DB::table('categoriamenu')->where('categoriamenu_id', '=', $categoria_id)->whereNull('deleted_at')->orderBy('orden', 'ASC')->get();
        if ($subcategorias->count() == 0) {
            return $cadenasubcategoria;
        }
        $cadenasubcategoria = '<ul style="list-style: none">';
        foreach ($subcategorias as $subcategoria) {
            $subcats = $this->buscarsubcategorias($subcategoria->id, $tipousuario_id, $permisosasignados, $superusuario);
            $cadenasubcategoria .= '<li><i class="fa ' . $subcategoria->icono . '">&nbsp;</i>' . $subcategoria->nombre;
            if ($subcats != '') {
                $cadenasubcategoria .= $subcats;
            }
            $opcionesmenu = DB::table('opcionmenu')->where('categoriamenu_id', '=', $subcategoria->id)->whereNull('deleted_at')->orderBy('orden', 'ASC')->get();
            foreach ($opcionesmenu as $opcionmenu) {
                $cadenasubcategoria .= '<ul style="list-style: none"><li><i class="fa ' . $opcionmenu->icono . '">&nbsp;</i>' . $opcionmenu->nombre . '<ul style="list-style: none">';
                $permisos = Permiso::listar($opcionmenu->id, $superusuario)->get();
                foreach ($permisos as $permiso) {
                    if (in_array($permiso->id, $permisosasignados)) {
                        $cadenasubcategoria .= '<li><label for="' . $permiso->id . '">' . $permiso->nombre . '</label>' .
                            '&nbsp;&nbsp;&nbsp;<input type="checkbox" checked  name="' . $permiso->id . '" id="' . $permiso->id .
                            '" onchange="guardarpermiso(\'' . $entidad . '\',\'' . $permiso->id . '\',this)"></li>';

                    } else {
                        $cadenasubcategoria .= '<li><label for="' . $permiso->id . '">' . $permiso->nombre . '</label>' .
                            '&nbsp;&nbsp;&nbsp;<input type="checkbox" name="' . $permiso->id . '" id="' . $permiso->id .
                            '" onchange="guardarpermiso(\'' . $entidad . '\',\'' . $permiso->id . '\',this)"></li>';
                    }
                }
                $cadenasubcategoria .= '</ul></li></ul>';
            }
            $cadenasubcategoria .= '</li>';

        }

        return $cadenasubcategoria . '</ul>';
    }


}
