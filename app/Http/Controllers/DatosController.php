<?php

namespace App\Http\Controllers;

use App\Core\Helpers\Certificate\GenerateCertificate;
use App\Empresa;
use App\Librerias\Libreria;
use App\Representante;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class DatosController extends Controller
{

    protected $folderview = 'app.datos';
    protected $tituloAdmin = 'Representantes';
    protected $tituloRegistrar = 'Registrar Representante';
    protected $tituloModificar = 'Modificar Representante';
    protected $tituloEliminar = 'Eliminar Representante';
    protected $rutas = array('create' => 'representante.create',
        'edit' => 'datos.edit',
        'delete' => 'datos.eliminar',
        'search' => 'datos.buscar',
        'index' => 'datos.index',
    );

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $entidad = 'Representante';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Representante';
        $representante = null;
        $formData = array('representante.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        $cboEmpresa = ['' => 'Seleccione'] + Empresa::pluck('razonsocial', 'id')->all();
        return view($this->folderview . '.mant')->with(compact('representante', 'formData', 'entidad', 'boton', 'listar', 'cboEmpresa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(),
            array(
                'nombres' => 'required|string|max:100',
                'apellidopaterno' => 'required|string|max:100',
                'apellidomaterno' => 'required|max:100|string',
                'dni' => 'required|integer|regex:/^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/',
                'rol' => 'required|max:40|string',
                'empresa_id' => 'required|integer|exists:empresa,id',
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request) {
            $representante = new Representante();
            $representante->nombres = $request->input('nombre');
            $representante->apellidopaterno = $request->input('apellidopaterno');
            $representante->apellidomaterno = $request->input('apellidomaterno');
            $representante->dni = $request->input('dni');
            $representante->rol = $request->input('rol');
            $representante->empresa_id = $request->input('empresa_id');
            $representante->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param Representante $representante
     * @return void
     */
    public function show(Representante $representante)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return bool|string
     */
    public function edit(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'representante');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $representante = Representante::find($id);
        $entidad = 'Representante';
        $formData = array('datos.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('representante', 'formData', 'entidad', 'boton', 'listar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return string
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'representante');
        if ($existe !== true) {
            return $existe;
        }
        $validacion = Validator::make($request->all(),
            array(
                'nombres' => 'required|string|max:100',
                'apellidopaterno' => 'required|string|max:100',
                'apellidomaterno' => 'required|max:100|string',
                'dni' => 'required|integer|regex:/^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/',
                'rol' => 'required|max:40|string',
                'password' => 'required_with:certificado',
                'certificado' => 'required_with:password'
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request, $id) {
            $representante = Representante::find($id);
            $representante->nombres = $request->input('nombres');
            $representante->apellidopaterno = $request->input('apellidopaterno');
            $representante->apellidomaterno = $request->input('apellidomaterno');
            $representante->dni = $request->input('dni');
            $representante->rol = $request->input('rol');
            $representante->save();

            try {
                if ($request->hasFile('certificado')) {
                    $password = $request->input('password');
                    $file = $request->file('certificado');
                    $pfx = file_get_contents($file);
                    $pem = GenerateCertificate::typePEM($pfx, $password);
                    $name = 'certificate_' . $representante->dni . '.pem';
                    if (!file_exists(storage_path('app' . '/ . 'certificates'))) {
                        mkdir(storage_path('app' . '/ . 'certificates'));
                    }
                    file_put_contents(storage_path('app' . '/ . 'certificates' . '/ . $name), $pem);
                    $representante->certificado = $name;
                    $representante->save();
                }
            } catch (Exception $e) {
                $mensajeerror = '';
                if ($e->getMessage() === 'error:23076071:PKCS12 routines:PKCS12_parse:mac verify failure') {
                    $mensajeerror = 'La contraseña del certificado no es correcta';
                }
                $error = array(
                    'password' => array(
                        $mensajeerror
                    )
                );
                return response()->json($error);
            }
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return bool|string
     */
    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'representante');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $representante = Representante::find($id);
            $representante->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @param Request $request
     * @return Response
     */
    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Representante';
        $representante = Auth::user()->representante;
        $resultado = Representante::where('id', $representante->id);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Empresa', 'numero' => '1');
        $cabecera[] = array('valor' => 'Apellidos y nombres', 'numero' => '1');
        $cabecera[] = array('valor' => 'DNI', 'numero' => '1');
        $cabecera[] = array('valor' => 'Rol', 'numero' => '1');
        $cabecera[] = array('valor' => 'Certificado', 'numero' => '1');
        $cabecera[] = array('valor' => 'Operaciones', 'numero' => '1');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }


    /**
     * Función para confirmar la eliminación de un registrlo
     * @param integer $id id del registro a intentar eliminar
     * @param string $listarLuego consultar si luego de eliminar se listará
     * @return bool|string
     */
    public function eliminar($id, $listarLuego)
    {

        $existe = Libreria::verificarExistencia($id, 'representante');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Representante::find($id);
        $entidad = 'Representante';
        $formData = array('route' => array('representante.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }
}
