<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Tipousuariousuario;
use Illuminate\Contracts\Session\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * @return RedirectResponse|Redirector|string
     * @throws ValidationException
     */
//    public function login()
//    {
//        $remember = false;
//        $this->validate(request(), [
//            'email' => 'required|string|email',
//            'password' => 'required|string',
//        ]);
//        if (request()->remember) {
//            $remember = true;
//        }
//        if (Auth::attempt(['email' => request()->email, 'password' => request()->password, 'deleted_at' => null, 'estado' => 'H'], $remember)) {
//            session(['tipousuario_id' => Auth::user()->unicotipousuario->tipousuario->id]);
//            return redirect('/');
//        }
//        return redirect(route('login'))->withErrors(['failed' => trans('auth.failed')])->withInput(request(['usuario']));
//    }
}
