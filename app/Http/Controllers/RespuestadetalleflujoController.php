<?php

namespace App\Http\Controllers;

use App\Detalleflujotrabajo;
use App\Librerias\Libreria;
use App\Respuesta;
use App\Respuestadetalleflujo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class RespuestadetalleflujoController extends Controller
{

    protected $folderview = 'app.respuestadetalleflujo';
    protected $tituloAdmin = 'Respuesta de detalle de flujos de trabajos';
    protected $tituloRegistrar = 'Registrar Respuesta de detalle de flujo de trabajo';
    protected $tituloModificar = 'Modificar Respuesta de detalle de flujo de trabajo';
    protected $tituloEliminar = 'Eliminar Respuesta de detalle de flujo de trabajo';
    protected $rutas = array('create' => 'respuestadetalleflujo.create',
        'edit' => 'respuestadetalleflujo.edit',
        'delete' => 'respuestadetalleflujo.eliminar',
        'search' => 'respuestadetalleflujo.buscar',
        'index' => 'respuestadetalleflujo.index',
    );

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $detalleflujotrabajo_id = $request->input('detalleflujotrabajo_id');
        $entidad = 'Respuestadetalleflujo';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta', 'detalleflujotrabajo_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $detalleflujotrabajo_id = Libreria::getParam($request->input('detalleflujotrabajo_id'));
        $entidad = 'Respuestadetalleflujo';
        $respuestadetalleflujo = null;
        $formData = array('respuestadetalleflujo.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        $detalleflujotrabajo = Detalleflujotrabajo::find($detalleflujotrabajo_id);
        $cboRespuesta = ['' => 'Seleccione'] + Respuesta::pluck('descripcion', 'id')->all();
        $cboDetalleflujotrabajo = ['' => 'Seleccione'] + Detalleflujotrabajo::join('tarea', 'detalle_flujotrabajo.tarea_id', 'tarea.id')
                ->where('detalle_flujotrabajo.flujotrabajo_id', $detalleflujotrabajo->flujotrabajo_id)
                ->where('tarea.editable', 1)
                ->select(DB::raw('tarea.descripcion as descripcion'), DB::raw('detalle_flujotrabajo.secuencia as secuencia'))
                ->pluck('descripcion', 'secuencia')->all();
        return view($this->folderview . '.mant')->with(compact('respuestadetalleflujo', 'formData', 'entidad', 'boton', 'listar', 'cboRespuesta', 'detalleflujotrabajo_id', 'cboDetalleflujotrabajo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $respuestas = [
            'secuencia.required_if' => "Si la respuesta es <b>Ir a la tarea...</b> debe especificar la secuencia",
        ];
        $validacion = Validator::make($request->all(),
            array(
                'respuesta_id' => 'required|integer|exists:respuesta,id',
                'descripcion' => 'required|string|max:100',
                'detalleflujotrabajo_id' => 'required|integer|exists:detalle_flujotrabajo,id',
                'secuencia' => 'nullable|required_if:respuesta_id,5|integer'
            ), $respuestas
        );

        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request) {
            $respuestadetalleflujo = new Respuestadetalleflujo();
            $respuestadetalleflujo->respuesta_id = $request->input('respuesta_id');
            $respuestadetalleflujo->detalleflujotrabajo_id = $request->input('detalleflujotrabajo_id');
            $respuestadetalleflujo->descripcion = $request->input('descripcion');
            if ($respuestadetalleflujo->respuesta_id == 5) {
                $respuestadetalleflujo->secuencia = Libreria::getParam($request->input('secuencia'));
            } else {
                $respuestadetalleflujo->secuencia = null;
            }
            $respuestadetalleflujo->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param Respuestadetalleflujo $respuestadetalleflujo
     * @return void
     */
    public function show(Respuestadetalleflujo $respuestadetalleflujo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return bool|string
     */
    public function edit(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'respuesta_detalleflujo');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $respuestadetalleflujo = Respuestadetalleflujo::find($id);
        $entidad = 'Respuestadetalleflujo';
        $formData = array('respuestadetalleflujo.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        $cboRespuesta = ['' => 'Seleccione'] + Respuesta::pluck('descripcion', 'id')->all();
        $detalleflujotrabajo_id = $respuestadetalleflujo->detalleflujotrabajo_id;
        $cboDetalleflujotrabajo = ['' => 'Seleccione'] + Detalleflujotrabajo::join('tarea', 'detalle_flujotrabajo.tarea_id', 'tarea.id')
                ->whereNull('tarea.deleted_at')
                ->where('detalle_flujotrabajo.flujotrabajo_id', $respuestadetalleflujo->detalleflujotrabajo->flujotrabajo_id)
                ->where('tarea.editable', 1)
                ->select(DB::raw('tarea.descripcion as descripcion'), DB::raw('detalle_flujotrabajo.secuencia as secuencia'))
                ->pluck('descripcion', 'secuencia')->all();
        return view($this->folderview . '.mant')->with(compact('respuestadetalleflujo', 'formData', 'entidad', 'boton', 'listar', 'cboFlujotrabajo', 'cboRespuesta', 'detalleflujotrabajo_id', 'cboDetalleflujotrabajo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return string
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'respuesta_detalleflujo');
        if ($existe !== true) {
            return $existe;
        }
        $respuestas = [
            'secuencia.required_if' => "Si la respuesta es <b>Ir a la tarea...</b> debe especificar la secuencia",
        ];
        $validacion = Validator::make($request->all(),
            array(
                'respuesta_id' => 'required|integer|exists:respuesta,id',
                'descripcion' => 'required|string|max:100',
                'detalleflujotrabajo_id' => 'required|integer|exists:detalle_flujotrabajo,id',
                'secuencia' => 'nullable|required_if:respuesta_id,5|integer'
            ), $respuestas
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request, $id) {
            $respuestadetalleflujo = Respuestadetalleflujo::find($id);
            $respuestadetalleflujo->respuesta_id = $request->input('respuesta_id');
            $respuestadetalleflujo->detalleflujotrabajo_id = $request->input('detalleflujotrabajo_id');
            $respuestadetalleflujo->descripcion = $request->input('descripcion');
            if ($respuestadetalleflujo->respuesta_id == 5) {
                $respuestadetalleflujo->secuencia = Libreria::getParam($request->input('secuencia'));
            } else {
                $respuestadetalleflujo->secuencia = null;
            }
            $respuestadetalleflujo->save();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return bool|string
     */
    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'respuesta_detalleflujo');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $respuestadetalleflujo = Respuestadetalleflujo::find($id);
            $respuestadetalleflujo->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @param Request $request
     * @return Response
     */
    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $detalleflujotrabajo_id = Libreria::getParam($request->input('detalleflujotrabajo_id'));
        $entidad = 'Respuestadetalleflujo';
        $resultado = Respuestadetalleflujo::listar($detalleflujotrabajo_id);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Descripción', 'numero' => '1');
        $cabecera[] = array('valor' => 'Respuesta', 'numero' => '1');
        $cabecera[] = array('valor' => 'Secuencia', 'numero' => '1');
        $cabecera[] = array('valor' => 'Editar', 'numero' => '1');
        $cabecera[] = array('valor' => 'Eliminar', 'numero' => '1');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }


    /**
     * Función para confirmar la eliminación de un registrlo
     * @param integer $id id del registro a intentar eliminar
     * @param string $listarLuego consultar si luego de eliminar se listará
     * @return bool|string
     */
    public function eliminar($id, $listarLuego)
    {

        $existe = Libreria::verificarExistencia($id, 'respuesta_detalleflujo');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Respuestadetalleflujo::find($id);
        $entidad = 'Respuestadetalleflujo';
        $formData = array('route' => array('respuestadetalleflujo.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        $mensaje = null;
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar', 'mensaje'));
    }
}
