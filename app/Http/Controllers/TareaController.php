<?php

namespace App\Http\Controllers;

use App\Librerias\Libreria;
use App\Tarea;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class TareaController extends Controller
{

    protected $folderview = 'app.tarea';
    protected $tituloAdmin = 'Tareas';
    protected $tituloRegistrar = 'Registrar Tarea';
    protected $tituloModificar = 'Modificar Tarea';
    protected $tituloEliminar = 'Eliminar Tarea';
    protected $rutas = array('create' => 'tarea.create',
        'edit' => 'tarea.edit',
        'delete' => 'tarea.eliminar',
        'search' => 'tarea.buscar',
        'index' => 'tarea.index',
    );

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $entidad = 'Tarea';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Tarea';
        $tarea = null;
        $formData = array('tarea.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('tarea', 'formData', 'entidad', 'boton', 'listar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(),
            array(
                'descripcion' => 'required|max:50|string',
                'codigo' => [
                    'required',
                    Rule::unique('tarea'),
                    'string',
                    'max:20'
                ],
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request) {
            $tarea = new Tarea();
            $tarea->descripcion = $request->input('descripcion');
            $tarea->codigo = $request->input('codigo');
            $tarea->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param Tarea $tarea
     * @return void
     */
    public function show(Tarea $tarea)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return bool|string
     */
    public function edit(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'tarea');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $tarea = Tarea::find($id);
        $entidad = 'Tarea';
        $formData = array('tarea.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('tarea', 'formData', 'entidad', 'boton', 'listar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return string
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'tarea');
        if ($existe !== true) {
            return $existe;
        }
        $validacion = Validator::make($request->all(),
            array(
                'descripcion' => 'required|max:50|string',
                'codigo' => [
                    'required',
                    Rule::unique('tarea')->ignore($id),
                    'string',
                    'max:20'
                ],
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request, $id) {
            $tarea = Tarea::find($id);
            $tarea->descripcion = $request->input('descripcion');
            $tarea->codigo = $request->input('codigo');
            $tarea->save();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return bool|string
     */
    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'tarea');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $tarea = Tarea::find($id);
            $tarea->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @param Request $request
     * @return Response
     */
    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Tarea';
        $nombre = Libreria::getParam($request->input('nombre'));
        $resultado = Tarea::listar($nombre);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Código', 'numero' => '1');
        $cabecera[] = array('valor' => 'Descripción', 'numero' => '1');
        $cabecera[] = array('valor' => 'Editar', 'numero' => '1');
        $cabecera[] = array('valor' => 'Eliminar', 'numero' => '1');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }


    /**
     * Función para confirmar la eliminación de un registrlo
     * @param integer $id id del registro a intentar eliminar
     * @param string $listarLuego consultar si luego de eliminar se listará
     * @return bool|string
     */
    public function eliminar($id, $listarLuego)
    {

        $existe = Libreria::verificarExistencia($id, 'tarea');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Tarea::find($id);
        $entidad = 'Tarea';
        $formData = array('route' => array('tarea.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        $mensaje = null;
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar', 'mensaje'));
    }
}
