<?php

namespace App\Http\Controllers;

use App\Librerias\Libreria;
use App\Respuesta;
use App\Tarea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class RespuestaController extends Controller
{

    protected $folderview = 'app.respuesta';
    protected $tituloAdmin = 'Respuestas';
    protected $tituloRegistrar = 'Registrar Respuesta';
    protected $tituloModificar = 'Modificar Respuesta';
    protected $tituloEliminar = 'Eliminar Respuesta';
    protected $rutas = array('create' => 'respuesta.create',
        'edit' => 'respuesta.edit',
        'delete' => 'respuesta.eliminar',
        'search' => 'respuesta.buscar',
        'index' => 'respuesta.index',
    );

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entidad = 'Respuesta';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Respuesta';
        $respuesta = null;
        $formData = array('respuesta.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        $cboTarea = ['' => 'Seleccione'] + Tarea::pluck('descripcion', 'id')->all();
        $cboAccion = ['' => 'Seleccione'] + ['siguiente' => 'Siguiente'] + ['anterior' => 'Anterior'] + ['seleccionar' => 'Seleccionar'] + ['rechazar' => 'Rechazado'];
        return view($this->folderview . '.mant')->with(compact('respuesta', 'formData', 'entidad', 'boton', 'listar', 'cboTarea', 'cboAccion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(),
            array(
                'descripcion' => 'required|max:45|string',
                'accion' => 'required|in:siguiente,anterior,seleccionar,rechazar',
                'tarea_id' => 'nullable|integer|exists:tarea,id',
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request) {
            $respuesta = new Respuesta();
            $respuesta->descripcion = $request->input('descripcion');
            $respuesta->accion = $request->input('accion');
            $respuesta->tarea_id = Libreria::getParam($request->input('tarea_id'));
            $respuesta->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Respuesta $respuesta
     * @return void
     */
    public function show(Respuesta $respuesta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return bool|string
     */
    public function edit(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'respuesta');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $respuesta = Respuesta::find($id);
        $entidad = 'Respuesta';
        $formData = array('respuesta.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        $cboAccion = ['' => 'Seleccione'] + ['siguiente' => 'Siguiente'] + ['anterior' => 'Anterior'] + ['seleccionar' => 'Seleccionar'] + ['rechazar' => 'Rechazado'];
        $cboTarea = ['' => 'Seleccione'] + Tarea::pluck('descripcion', 'id')->all();
        return view($this->folderview . '.mant')->with(compact('respuesta', 'formData', 'entidad', 'boton', 'listar', 'cboTarea', 'cboAccion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return string
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'respuesta');
        if ($existe !== true) {
            return $existe;
        }
        $validacion = Validator::make($request->all(),
            array(
                'descripcion' => 'required|max:45|string',
                'accion' => 'required|in:siguiente,anterior,seleccionar,rechazar',
                'tarea_id' => 'nullable|integer|exists:tarea,id',
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request, $id) {
            $respuesta = Respuesta::find($id);
            $respuesta->descripcion = $request->input('descripcion');
            $respuesta->accion = $request->input('accion');
            $respuesta->tarea_id = Libreria::getParam($request->input('tarea_id'));
            $respuesta->save();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return bool|string
     */
    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'respuesta');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $respuesta = Respuesta::find($id);
            $respuesta->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Respuesta';
        $nombre = Libreria::getParam($request->input('nombre'));
        $resultado = Respuesta::listar($nombre);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Tarea', 'numero' => '1');
        $cabecera[] = array('valor' => 'Descripción', 'numero' => '1');
        $cabecera[] = array('valor' => 'Acción', 'numero' => '1');
        $cabecera[] = array('valor' => 'Editar', 'numero' => '1');
        $cabecera[] = array('valor' => 'Eliminar', 'numero' => '1');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }


    /**
     * Función para confirmar la eliminación de un registrlo
     * @param integer $id id del registro a intentar eliminar
     * @param string $listarLuego consultar si luego de eliminar se listará
     * @return bool|string
     */
    public function eliminar($id, $listarLuego)
    {

        $existe = Libreria::verificarExistencia($id, 'respuesta');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Respuesta::find($id);
        $entidad = 'Respuesta';
        $formData = array('route' => array('respuesta.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        $mensaje = null;
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar', 'mensaje'));
    }
}
