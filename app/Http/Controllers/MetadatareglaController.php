<?php

namespace App\Http\Controllers;

use App\Librerias\Libreria;
use App\Metadataregla;
use App\Regla;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class MetadatareglaController extends Controller
{

    protected $folderview = 'app.metadataregla';
    protected $tituloAdmin = 'Reglas de metadata';
    protected $tituloRegistrar = 'Registrar Reglas de metadata';
    protected $tituloModificar = 'Modificar Reglas de metadata';
    protected $tituloEliminar = 'Eliminar Reglas de metadata';
    protected $rutas = array('create' => 'metadataregla.create',
        'edit' => 'metadataregla.edit',
        'delete' => 'metadataregla.eliminar',
        'search' => 'metadataregla.buscar',
        'index' => 'metadataregla.index',
    );

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $metadata_id = Libreria::getParam($request->input('metadata_id'));
        $entidad = 'Metadataregla';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta', 'metadata_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $metadata_id = Libreria::getParam($request->input('metadata_id'));
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Metadataregla';
        $metadataregla = null;
        $cboRegla = ['' => 'Seleccione'] + Regla::pluck('descripcion', 'id')->all();
        $formData = array('metadataregla.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('metadataregla', 'formData', 'entidad', 'boton', 'listar', 'metadata_id', 'cboRegla'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(),
            array(
                'valor' => 'nullable|string|max:120',
                'metadata_id' => 'required|integer|exists:metadata,id',
                'regla_id' => 'required|integer|exists:regla,id',
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request) {
            $metadataregla = new Metadataregla();
            $metadataregla->valor = Libreria::getParam($request->input('valor'));
            $metadataregla->metadata_id = $request->input('metadata_id');
            $metadataregla->regla_id = $request->input('regla_id');
            $metadataregla->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Metadataregla $metadataregla
     * @return void
     */
    public function show(Metadataregla $metadataregla)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return bool|string
     */
    public function edit(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'metadata_regla');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $metadataregla = Metadataregla::find($id);
        $metadata_id = $metadataregla->metadata_id;
        $cboRegla = ['' => 'Seleccione'] + Regla::pluck('descripcion', 'id')->all();
        $entidad = 'Metadataregla';
        $formData = array('metadataregla.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('metadataregla', 'formData', 'entidad', 'boton', 'listar', 'metadata_id', 'cboRegla'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return string
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'metadata_regla');
        if ($existe !== true) {
            return $existe;
        }
        $validacion = Validator::make($request->all(),
            array(
                'valor' => 'nullable|string|max:120',
                'metadata_id' => 'required|integer|exists:metadata,id',
                'regla_id' => 'required|integer|exists:regla,id',
            )
        );
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request, $id) {
            $metadataregla = Metadataregla::find($id);
            $metadataregla->valor = Libreria::getParam($request->input('valor'));
            $metadataregla->metadata_id = $request->input('metadata_id');
            $metadataregla->regla_id = $request->input('regla_id');
            $metadataregla->save();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return bool|string
     */
    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'metadata_regla');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $metadataregla = Metadataregla::find($id);
            $metadataregla->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Metadataregla';
        $metadata_id = Libreria::getParam($request->input('metadata_id'));
        $resultado = Metadataregla::listar($metadata_id);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => 'Regla', 'numero' => '1');
        $cabecera[] = array('valor' => 'Valor', 'numero' => '1');
        $cabecera[] = array('valor' => 'Editar', 'numero' => '1');
        $cabecera[] = array('valor' => 'Eliminar', 'numero' => '1');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }


    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return bool|string
     */
    public function eliminar($id, $listarLuego)
    {

        $existe = Libreria::verificarExistencia($id, 'metadata_regla');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Metadataregla::find($id);
        $entidad = 'Metadataregla';
        $formData = array('route' => array('metadataregla.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        $mensaje = null;
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar', 'mensaje'));
    }
}
