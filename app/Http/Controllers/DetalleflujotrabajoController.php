<?php

namespace App\Http\Controllers;

use App\Detalleflujotrabajo;
use App\Detalleflujotrabajotipousuario;
use App\Librerias\Libreria;
use App\Tarea;
use App\Tipousuario;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class DetalleflujotrabajoController extends Controller
{

    protected $folderview = 'app.detalleflujotrabajo';
    protected $tituloAdmin = 'Detalle flujo trabajos';
    protected $tituloRegistrar = 'Registrar Detalle flujo trabajo';
    protected $tituloModificar = 'Modificar Detalle flujo trabajo';
    protected $tituloEliminar = 'Eliminar Detalle flujo trabajo';
    protected $rutas = array('create' => 'detalleflujotrabajo.create',
        'edit' => 'detalleflujotrabajo.edit',
        'delete' => 'detalleflujotrabajo.eliminar',
        'search' => 'detalleflujotrabajo.buscar',
        'index' => 'detalleflujotrabajo.index',
    );

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $flujotrabajo_id = $request->input('flujotrabajo_id');
        $entidad = 'Detalleflujotrabajo';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta', 'flujotrabajo_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $flujotrabajo_id = Libreria::getParam($request->input('flujotrabajo_id'), 'NO');
        $entidad = 'Detalleflujotrabajo';
        $detalleflujotrabajo = null;
        $formData = array('detalleflujotrabajo.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        $cboTarea = ['' => 'Seleccione'] + Tarea::where('editable', 1)->pluck('descripcion', 'id')->all();
        $cboTipousuario = Tipousuario::join('empresa', 'tipousuario.empresa_id', 'empresa.id')
                                    ->whereNull('tipousuario.deleted_at')
                                    ->whereNull('empresa.deleted_at')
                                    ->select(DB::raw('CONCAT(empresa.razonsocial, "-", tipousuario.nombre) as nombre'), 'tipousuario.id')->pluck('nombre', 'id')->all();
        return view($this->folderview . '.mant')->with(compact('detalleflujotrabajo', 'formData', 'entidad', 'boton', 'listar', 'cboTarea', 'flujotrabajo_id', 'cboTipousuario'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(),
            array(
                'tarea_id' => 'required|integer|exists:tarea,id',
                'tipousuario_id' => 'required|array',
                'flujotrabajo_id' => 'required|integer|exists:flujotrabajo,id',
                'editararchivos' => 'nullable',
                'secuencia' => [
                    'required',
                    'integer',
                    'min:1',
                    Rule::unique('detalle_flujotrabajo')->where(function ($query) use ($request) {
                        return $query->where('secuencia', $request->input('secuencia'))
                            ->where('flujotrabajo_id', $request->input('flujotrabajo_id'))
                            ->whereNull('deleted_at');
                    }),
                ],
            )
        );
        $tipousuarios = Libreria::getParamarray($request->input('tipousuario_id'), array());
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request, $tipousuarios) {
            $detalleflujotrabajo = new Detalleflujotrabajo();
            $detalleflujotrabajo->secuencia = $request->input('secuencia');
            $detalleflujotrabajo->tarea_id = $request->input('tarea_id');
            $detalleflujotrabajo->flujotrabajo_id = $request->input('flujotrabajo_id');
            if (!is_null(Libreria::getParam($request->input('editararchivos')))) {
                $detalleflujotrabajo->editararchivos = true;
            } else {
                $detalleflujotrabajo->editararchivos = false;
            }
            $detalleflujotrabajo->save();
            for ($i = 0; $i < count($tipousuarios); $i++) {
                $detalleflujotrabajotipousuario = new Detalleflujotrabajotipousuario();
                $detalleflujotrabajotipousuario->detalleflujotrabajo_id = $detalleflujotrabajo->id;
                $detalleflujotrabajotipousuario->tipousuario_id = $tipousuarios[$i];
                $detalleflujotrabajotipousuario->save();
            }
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param Detalleflujotrabajo $detalleflujotrabajo
     * @return void
     */
    public function show(Detalleflujotrabajo $detalleflujotrabajo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return bool|string
     */
    public function edit(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'detalle_flujotrabajo');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $detalleflujotrabajo = Detalleflujotrabajo::find($id);
        $entidad = 'Detalleflujotrabajo';
        $formData = array('detalleflujotrabajo.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        $cboTipousuario = Tipousuario::join('empresa', 'tipousuario.empresa_id', 'empresa.id')
                                    ->whereNull('tipousuario.deleted_at')
                                    ->whereNull('empresa.deleted_at')
                                    ->select(DB::raw('CONCAT(empresa.razonsocial, "-", tipousuario.nombre) as nombre'), 'tipousuario.id')->pluck('nombre', 'id')->all();
        $cboTarea = ['' => 'Seleccione'] + Tarea::where('editable', 1)->pluck('descripcion', 'id')->all();
        $flujotrabajo_id = $detalleflujotrabajo->flujotrabajo_id;
        return view($this->folderview . '.mant')->with(compact('detalleflujotrabajo', 'formData', 'entidad', 'boton', 'listar', 'cboFlujotrabajo', 'cboTarea', 'flujotrabajo_id', 'cboTipousuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return string
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'detalle_flujotrabajo');
        if ($existe !== true) {
            return $existe;
        }
        $validacion = Validator::make($request->all(),
            array(
                'secuencia' => [
                    'required',
                    'integer',
                    'min:1',
                    Rule::unique('detalle_flujotrabajo')->where(function ($query) use ($request, $id) {
                        return $query->where('secuencia', $request->input('secuencia'))
                            ->where('flujotrabajo_id', $request->input('flujotrabajo_id'))
                            ->whereNull('deleted_at')
                            ->whereNotIn('id', [$id]);
                    }),
                ],
                'editararchivos' => 'nullable',
                'tipousuario_id' => 'required|array',
                'tarea_id' => 'required|integer|exists:tarea,id',
                'flujotrabajo_id' => 'required|integer|exists:flujotrabajo,id',
            )
        );
        $tipousuarios = Libreria::getParamarray($request->input('tipousuario_id'), array());
        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }
        $error = DB::transaction(function () use ($request, $id, $tipousuarios) {
            $detalleflujotrabajo = Detalleflujotrabajo::find($id);
            $detalleflujotrabajo->secuencia = $request->input('secuencia');
            $detalleflujotrabajo->tarea_id = $request->input('tarea_id');
            $detalleflujotrabajo->flujotrabajo_id = $request->input('flujotrabajo_id');
            if (!is_null(Libreria::getParam($request->input('editararchivos')))) {
                $detalleflujotrabajo->editararchivos = true;
            } else {
                $detalleflujotrabajo->editararchivos = false;
            }
            $detalleflujotrabajo->save();
            // eliminamos las asignaciones anteriores
            Detalleflujotrabajotipousuario::where('detalleflujotrabajo_id', $detalleflujotrabajo->id)->delete();
            // registramos las nuevas asignaciones
            for ($i = 0; $i < count($tipousuarios); $i++) {
                $detalleflujotrabajotipousuario = new Detalleflujotrabajotipousuario();
                $detalleflujotrabajotipousuario->detalleflujotrabajo_id = $detalleflujotrabajo->id;
                $detalleflujotrabajotipousuario->tipousuario_id = $tipousuarios[$i];
                $detalleflujotrabajotipousuario->save();
            }
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return bool|string
     */
    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'detalle_flujotrabajo');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $detalleflujotrabajo = Detalleflujotrabajo::find($id);
            $detalleflujotrabajo->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @param Request $request
     * @return Response
     */
    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $flujotrabajo_id = Libreria::getParam($request->input('flujotrabajo_id'));
        $entidad = 'Detalleflujotrabajo';
        $nombre = Libreria::getParam($request->input('nombre'));
        $resultado = Detalleflujotrabajo::listar($nombre, $flujotrabajo_id);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => 'Secuencia', 'numero' => '1');
        $cabecera[] = array('valor' => 'Tarea', 'numero' => '1');
        $cabecera[] = array('valor' => '¿Editar archivos?', 'numero' => '1');
        $cabecera[] = array('valor' => 'Tipos de usuario', 'numero' => '1');
        $cabecera[] = array('valor' => 'Respuestas', 'numero' => '1');
        $cabecera[] = array('valor' => 'Editar', 'numero' => '1');
        $cabecera[] = array('valor' => 'Eliminar', 'numero' => '1');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }

    /**
     * Función para confirmar la eliminación de un registrlo
     * @param integer $id id del registro a intentar eliminar
     * @param string $listarLuego consultar si luego de eliminar se listará
     * @return bool|string
     */
    public function eliminar($id, $listarLuego)
    {
        $existe = Libreria::verificarExistencia($id, 'detalle_flujotrabajo');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Detalleflujotrabajo::find($id);
        $entidad = 'Detalleflujotrabajo';
        $formData = array('route' => array('detalleflujotrabajo.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        $mensaje = null;
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar', 'mensaje'));
    }
}
