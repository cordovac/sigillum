<?php

namespace App\Http\Middleware;

use App\Librerias\Libreria;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param $tipo
     * @param array $permisos
     * @return mixed
     */
    public function handle($request, Closure $next, $tipo, ...$permisos)
    {
        $permisosnecesarios = array();
        foreach ($permisos as $permiso) {
            $permisosnecesarios[] = $permiso;
        }
        // si es superusuario saltamos validación de permisos
        if (Auth::check()) {
            if (Auth::user()->superusuario) {
                return $next($request);
            }
        }
        $tipousuario_id = Auth::user()->unicotipousuario->tipousuario->id;
        for ($i = 0; $i < count($permisos); $i++) {
            $nombrepermiso = $permisos[$i];
            $permiso = DB::table('permiso')->where('codigo', $nombrepermiso)->first();
            if ($permiso) {
                $tienepermiso = DB::table('permiso_tipousuario')->where('permiso_id', $permiso->id)->where("tipousuario_id", $tipousuario_id)->first();
                if ($tienepermiso) {
                    return $next($request);
                } else {
                    return redirect()->route('home.noautorizado', array($tipo));
                }
            } else {
                return redirect()->route('home.noautorizado', array($tipo));
            }
        }
    }
}
