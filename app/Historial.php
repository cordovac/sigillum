<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Historial extends Model
{
    use SoftDeletes;

    protected $table = 'historial';
    protected $dates = ['deleted_at', 'fechaoperacion', 'created_at', 'updated_at'];

    /**
     * @return BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\User', 'usuario_id');
    }

    public function usuarioemisor()
    {
        return $this->belongsTo('App\User', 'usuarioemisor_id');
    }

    /**
     * @return BelongsTo
     */
    public function respuesta()
    {
        return $this->belongsTo('App\Respuesta', 'respuesta_id');
    }

    /**
     * @return BelongsTo
     */
    public function documento()
    {
        return $this->belongsTo('App\Documento', 'documento_id');
    }

    /**
     * @return BelongsTo
     */
    public function detalleflujotrabajo()
    {
        return $this->belongsTo('App\Detalleflujotrabajo', 'detalleflujotrabajo_id');
    }

    /**
     * @return BelongsTo
     */
    public function padre()
    {
        return $this->belongsTo('App\Historial', 'historial_id');
    }

    public function archivos()
    {
        return $this->hasMany('App\Archivo', 'historial_id')->orderBy('archivo.accion', 'ASC');
    }

    /**
     * @param $query
     * @param $documento_id
     * @return mixed
     */
    public function scopeultimopaso($query, $documento_id)
    {
        return $query->join('documento', 'historial.documento_id', 'documento.id')
            ->where('historial.documento_id', $documento_id)
            ->whereNull('historial.fechaoperacion')
            ->whereNull('historial.respuesta_id')
            ->whereNull('historial.usuario_id')
            ->select('historial.*')
            ->first();
    }
}
