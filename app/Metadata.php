<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Metadata extends Model
{
    use SoftDeletes;
    protected $table = 'metadata';
    protected $dates = ['deleted_at'];

    /**
     * @param $query
     * @param $nombre
     * @param $tipodocumento_id
     * @return mixed
     */
    public function scopelistar($query, $nombre, $tipodocumento_id)
    {
        return $query->where(function ($subquery) use ($nombre, $tipodocumento_id) {
            if (!is_null($nombre)) {
                $subquery->where('nombre', 'LIKE', '%' . $nombre . '%');
            }
            if (!is_null($tipodocumento_id)) {
                $subquery->where('tipodocumento_id', $tipodocumento_id);
            }
        })->orderBy('nombre', 'ASC');
    }

    /**
     * @return BelongsTo
     */
    public function tipodocumento()
    {
        return $this->belongsTo('App\Tipodocumento', 'tipodocumento_id');
    }

    /**
     * @return HasMany
     */
    public function metadatareglas()
    {
        return $this->hasMany('App\Metadataregla', 'metadata_id');
    }

    /**
     * @return BelongsToMany
     */
    public function reglas()
    {
        return $this->belongsToMany('App\Regla', 'metadata_regla', 'metadata_id', 'regla_id');

    }
}
