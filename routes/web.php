<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/main', 'HomeController@main')->name('home.main');
    Route::get('/noautorizado/{tipo}', 'HomeController@noautorizado')->name('home.noautorizado');
    Route::get('emisor', 'HomeController@emisor')->name('home.emisor');
    Route::get('receptor', 'HomeController@receptor')->name('home.receptor');
    Route::post('categoriamenu/buscar', 'CategoriamenuController@buscar')->name('categoriamenu.buscar');
    Route::get('categoriamenu/eliminar/{id}/{listarluego}', 'CategoriamenuController@eliminar')->name('categoriamenu.eliminar');
    Route::resource('categoriamenu', 'CategoriamenuController', array('except' => array('show')));

    Route::post('opcionmenu/buscar', 'OpcionmenuController@buscar')->name('opcionmenu.buscar');
    Route::get('opcionmenu/eliminar/{id}/{listarluego}', 'OpcionmenuController@eliminar')->name('opcionmenu.eliminar');
    Route::resource('opcionmenu', 'OpcionmenuController', array('except' => array('show')));

    Route::post('tipousuario/buscar', 'TipousuarioController@buscar')->name('tipousuario.buscar');
    Route::get('tipousuario/obtenerpermisos/{listar}/{id}', 'TipousuarioController@obtenerpermisos')->name('tipousuario.obtenerpermisos');
    Route::post('tipousuario/guardarpermisos/{id}/{permiso_id}/{accion}', 'TipousuarioController@guardarpermisos')->name('tipousuario.guardarpermisos');
    Route::get('tipousuario/eliminar/{id}/{listarluego}', 'TipousuarioController@eliminar')->name('tipousuario.eliminar');
    Route::resource('tipousuario', 'TipousuarioController', array('except' => array('show')));

    Route::resource('usuario', 'UsuarioController');
    Route::post('usuario/buscar', 'UsuarioController@buscar')->name('usuario.buscar');
    Route::get('usuario/eliminar/{id}/{listarluego}', 'UsuarioController@eliminar')->name('usuario.eliminar');
    Route::get('usuario/autocompletarrepresentante/{querysearch}', 'UsuarioController@autocompletarrepresentante')->name('usuario.autocompletarrepresentante');
    Route::get('usuario/activarusuario/{id}/{listarluego}', 'UsuarioController@activarusuario')->name('usuario.activarusuario');
    Route::post('usuario/activarcuenta/{id}', 'UsuarioController@activarcuenta')->name('usuario.activarcuenta');
    Route::get('usuario/editclave/{id}', 'UsuarioController@editclave')->name('usuario.editclave');
    Route::put('usuario/updateclave/{id}', 'UsuarioController@updateclave')->name('usuario.updateclave');
    Route::get('usuario/editdatos/{id}', 'UsuarioController@editdatos')->name('usuario.editdatos');
    Route::put('usuario/updatedatos/{id}', 'UsuarioController@updatedatos')->name('usuario.updatedatos');

    Route::post('permiso/buscar', 'PermisoController@buscar')->name('permiso.buscar');
    Route::get('permiso/eliminar/{id}/{listarluego}', 'PermisoController@eliminar')->name('permiso.eliminar');
    Route::resource('permiso', 'PermisoController', array('except' => array('show')));

    Route::post('empresa/buscar', 'EmpresaController@buscar')->name('empresa.buscar');
    Route::get('empresa/eliminar/{id}/{listarluego}', 'EmpresaController@eliminar')->name('empresa.eliminar');
    Route::resource('empresa', 'EmpresaController', array('except' => array('show')));

    Route::post('representante/buscar', 'RepresentanteController@buscar')->name('representante.buscar');
    Route::get('representante/eliminar/{id}/{listarluego}', 'RepresentanteController@eliminar')->name('representante.eliminar');
    Route::resource('representante', 'RepresentanteController', array('except' => array('show')));

    Route::post('tipodocumento/buscar', 'TipodocumentoController@buscar')->name('tipodocumento.buscar');
    Route::get('tipodocumento/eliminar/{id}/{listarluego}', 'TipodocumentoController@eliminar')->name('tipodocumento.eliminar');
    Route::resource('tipodocumento', 'TipodocumentoController', array('except' => array('show')));

    Route::post('metadata/buscar', 'MetadataController@buscar')->name('metadata.buscar');
    Route::get('metadata/eliminar/{id}/{listarluego}', 'MetadataController@eliminar')->name('metadata.eliminar');
    Route::resource('metadata', 'MetadataController', array('except' => array('show')));

    Route::post('flujotrabajo/buscar', 'FlujotrabajoController@buscar')->name('flujotrabajo.buscar');
    Route::get('flujotrabajo/eliminar/{id}/{listarluego}', 'FlujotrabajoController@eliminar')->name('flujotrabajo.eliminar');
    Route::get('flujotrabajo/verdocumentos/{flujotrabajo_id}', 'FlujotrabajoController@verdocumentos')->name('flujotrabajo.documentos');
    Route::resource('flujotrabajo', 'FlujotrabajoController', array('except' => array('show')));

    Route::post('documento/buscar', 'DocumentoController@buscar')->name('documento.buscar');
    Route::post('documento/guardar', 'DocumentoController@guardar')->name('documento.guardar');
    Route::get('documento/eliminar/{id}/{listarluego}', 'DocumentoController@eliminar')->name('documento.eliminar');
    Route::get('documento/emitir', 'DocumentoController@emitir')->name('documento.emitir');
    Route::get('documento/generarmetadata/{tipodocumento_id}/{documento_id?}', 'DocumentoController@generarmetadata')->name('documento.generarmetadata');
    Route::resource('documento', 'DocumentoController');

    Route::post('datos/buscar', 'DatosController@buscar')->name('datos.buscar');
    Route::get('datos/eliminar/{id}/{listarluego}', 'DatosController@eliminar')->name('datos.eliminar');
    Route::resource('datos', 'DatosController', array('except' => array('show')));

    Route::post('tarea/buscar', 'TareaController@buscar')->name('tarea.buscar');
    Route::get('tarea/eliminar/{id}/{listarluego}', 'TareaController@eliminar')->name('tarea.eliminar');
    Route::resource('tarea', 'TareaController', array('except' => array('show')));

    Route::post('bitacora/buscar', 'BitacoraController@buscar')->name('bitacora.buscar');
    Route::get('bitacora/eliminar/{id}/{listarluego}', 'BitacoraController@eliminar')->name('bitacora.eliminar');
    Route::resource('bitacora', 'BitacoraController', array('except' => array('show')));

    Route::post('respuesta/buscar', 'RespuestaController@buscar')->name('respuesta.buscar');
    Route::get('respuesta/eliminar/{id}/{listarluego}', 'RespuestaController@eliminar')->name('respuesta.eliminar');
    Route::resource('respuesta', 'RespuestaController', array('except' => array('show')));

    Route::post('detalleflujotrabajo/buscar', 'DetalleflujotrabajoController@buscar')->name('detalleflujotrabajo.buscar');
    Route::get('detalleflujotrabajo/eliminar/{id}/{listarluego}', 'DetalleflujotrabajoController@eliminar')->name('detalleflujotrabajo.eliminar');
    Route::resource('detalleflujotrabajo', 'DetalleflujotrabajoController', array('except' => array('show')));

    Route::post('seguimiento/buscar', 'SeguimientoController@buscar')->name('seguimiento.buscar');
    Route::get('seguimiento/eliminar/{id}/{listarluego}', 'SeguimientoController@eliminar')->name('seguimiento.eliminar');
    Route::get('seguimiento/buscartareas/{flujotrabajo_id}', 'SeguimientoController@buscartareas')->name('seguimiento.buscartareas');
    Route::get('seguimiento/documentosportarea/{detalleflujotrabajo_id}', 'SeguimientoController@documentosportarea')->name('seguimiento.documentosportarea');
    Route::resource('seguimiento', 'SeguimientoController', array('except' => array('show')));

    Route::post('seguimiento/buscaremisor', 'SeguimientoController@buscaremisor')->name('seguimiento.buscaremisor');
    Route::get('seguimiento/buscartareasemisor/{flujotrabajo_id}', 'SeguimientoController@buscartareasemisor')->name('seguimiento.buscartareasemisor');
    Route::get('seguimiento/documentosportareaemisor/{detalleflujotrabajo_id}', 'SeguimientoController@documentosportareaemisor')->name('seguimiento.documentosportareaemisor');

    Route::post('regla/buscar', 'ReglaController@buscar')->name('regla.buscar');
    Route::get('regla/eliminar/{id}/{listarluego}', 'ReglaController@eliminar')->name('regla.eliminar');
    Route::resource('regla', 'ReglaController', array('except' => array('show')));

    Route::post('historial/buscar', 'HistorialController@buscar')->name('historial.buscar');
    Route::get('historial/eliminar/{id}/{listarluego}', 'HistorialController@eliminar')->name('historial.eliminar');
    Route::resource('historial', 'HistorialController', array('except' => array('show')));
    Route::get('historial/documento/{documento}', 'HistorialController@historialdocumento')->name('historial.historialdocumento');
    Route::get('historial/verarchivos', 'HistorialController@verarchivos')->name('historial.verarchivos');

    Route::post('metadataregla/buscar', 'MetadatareglaController@buscar')->name('metadataregla.buscar');
    Route::get('metadataregla/eliminar/{id}/{listarluego}', 'MetadatareglaController@eliminar')->name('metadataregla.eliminar');
    Route::resource('metadataregla', 'MetadatareglaController', array('except' => array('show')));

    Route::post('respuestadetalleflujo/buscar', 'RespuestadetalleflujoController@buscar')->name('respuestadetalleflujo.buscar');
    Route::get('respuestadetalleflujo/eliminar/{id}/{listarluego}', 'RespuestadetalleflujoController@eliminar')->name('respuestadetalleflujo.eliminar');
    Route::resource('respuestadetalleflujo', 'RespuestadetalleflujoController', array('except' => array('show')));

    Route::get('archivo/mostrararchivo/{idarchivo}', 'ArchivoController@mostrararchivo')->name('archivo.mostrararchivo');


    Route::get('reporte/documento/{documento_id}', 'ReporteController@documentopdf')->name('reporte.documentopdf');
});

Route::get('mail/send', 'MailController@send');
